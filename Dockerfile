# Build stage
FROM jelastic/maven:3.9.5-openjdk-21 as build

ARG APP_PROFILE=local_docker

WORKDIR /code

COPY pom.xml /code/
COPY src /code/src

# An uber JAR file is also known as fat JAR, i.e., a JAR file with dependencies.
RUN mvn clean package -DskipTests -Dquarkus.package.type=uber-jar -Dquarkus.profile=${APP_PROFILE}

# Run stage
FROM eclipse-temurin:21-jre-alpine

ARG APP_PROFILE=local_docker
ENV APP_PROFILE=${APP_PROFILE}

WORKDIR /app

RUN addgroup --system javauser && adduser -S -s /usr/sbin/nologin -G javauser javauser
COPY --from=build --chown=javauser:javauser /code/target/*-runner.jar /app/app.jar
COPY --from=build --chown=javauser:javauser /code/src/main/resources/application-${APP_PROFILE}.properties /app/application-${APP_PROFILE}.properties

USER javauser
CMD java -Dquarkus.profile=${APP_PROFILE} -jar app.jar