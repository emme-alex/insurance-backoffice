# Back office of an insurance company

## Involved technologies

- Java JDK 21
- Quarkus 3
- Hibernate 6
- PostgreSQL
- Docker
- Keycloak
- JUnit

## Build & Run

### Dockerized version (with Database and OIDC server)

```shell
cd docker
docker compose up -d
```

### Local Build & Run

```shell
./mvnw clean install quarkus:dev -Dquarkus.profile=local
```

### Tests

```shell
./mvnw -Pdev-test
```

## API docs

- http://localhost:8085/openapi/swagger-ui/
- For request samples see [wiki/API_DOCS.md](wiki/API_DOCS.md)

## Keycloak

- **Console**: http://localhost:8666/admin/
- **Access Token URL**: `http://localhost:8666/realms/backoffice/realms/backoffice/protocol/openid-connect/token`
- Client OAuth 2.0 conf example:

![](src/main/resources/backoffice_oauth2_client_conf_sample.png)