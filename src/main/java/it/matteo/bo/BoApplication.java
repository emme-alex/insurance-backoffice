package it.matteo.bo;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;

@ApplicationPath("")
@OpenAPIDefinition(
        info = @Info(
                title = "Backoffice API",
                version = "1.0.0",
                contact = @Contact(
                        name = "Backoffice API Support",
                        email = "backoffice@example.com"),
                license = @License(
                        name = "Apache 2.0",
                        url = "https://www.apache.org/licenses/LICENSE-2.0.html")),
        security = {
                @SecurityRequirement(name = "Keycloak", scopes = {"global"})
        }
)
public class BoApplication extends Application {
}
