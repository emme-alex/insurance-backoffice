package it.matteo.bo.client.document;

import it.matteo.bo.model.primary.DownloadDocumentRequest;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;


@RegisterRestClient(configKey = "primary-document")
public interface DocumentClient {

    @POST
    @Path("/download")
    Response download(DownloadDocumentRequest certificateRequestDto);

    @POST
    @Path("/downloadLink")
    Response downloadLink(DownloadDocumentRequest certificateRequestDto);

}

