package it.matteo.bo.client.product;

import it.matteo.bo.exception.ExternalServiceExceptionMapper;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.reactive.RestHeader;


@Path("/recommendations")
@RegisterProvider(ExternalServiceExceptionMapper.class)
@RegisterRestClient(configKey = "product")
public interface ProductRecommendationsClient {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    Response list(@RestHeader("Authorization") String token);

}
