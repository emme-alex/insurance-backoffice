package it.matteo.bo.constants;

import lombok.Getter;

@Getter
public enum CouponTypeEnum {

    SINGLE("single"),
    MULTIPLE("multiple");

    private final String value;

    CouponTypeEnum(final String value) {
        this.value = value;
    }

}
