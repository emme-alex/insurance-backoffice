package it.matteo.bo.constants;

import lombok.Getter;

@Getter
public enum RelatedWarrantyRulesErrorsEnum {

    ERR_ADD_WARRANTY("missing required dependency"),
    ERR_DEL_WARRANTY("found conflicting dependency"),
    ERR_POSS_ADD_WARRANTY("missing possible blocks of dependencies");

    private final String value;

    RelatedWarrantyRulesErrorsEnum(final String value) {
        this.value = value;
    }

}
