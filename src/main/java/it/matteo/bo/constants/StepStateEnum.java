package it.matteo.bo.constants;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public enum StepStateEnum {
    INSURANCE_INFO(0),
    ADDRESS(1),
    SURVEY(2),
    PAYMENT(3),
    CONFIRM(4),
    COMPLETE(5);

    private final Integer weight;

    StepStateEnum(final Integer weight) {
        this.weight = weight;
    }

    public static StepStateEnum getByValue(final Integer value) {
        if (value == null || value < 0) return INSURANCE_INFO;
        if (value >= 5) return COMPLETE;

        for (final StepStateEnum val : StepStateEnum.values()) {
            if (value.intValue() == val.getWeight().intValue()) return val;
        }

        return null;
    }

    public static StepStateEnum getByString(final String value) {
        if (StringUtils.isBlank(value)) return INSURANCE_INFO;

        return switch (value) {
            case "INSURANCE_INFO", "insurance_info" -> INSURANCE_INFO;
            case "ADDRESS", "address" -> ADDRESS;
            case "SURVEY", "survey" -> SURVEY;
            case "PAYMENT", "payment" -> PAYMENT;
            case "CONFIRM", "confirm" -> CONFIRM;
            case "COMPLETE", "complete" -> COMPLETE;
            default -> INSURANCE_INFO;
        };
    }
}
