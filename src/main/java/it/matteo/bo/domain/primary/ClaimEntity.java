package it.matteo.bo.domain.primary;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.type.SqlTypes;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@ToString
@Table(
        schema = "policy",
        name = "claims"
)
@Getter
@Setter
public class ClaimEntity {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "claim_type")
    private String claimType;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "message")
    private String message;

    @Column(name = "claim_number")
    private String claimNumber;

    @ManyToOne
    @JoinColumn(name = "policy_id", columnDefinition = "int4")
    @NotFound(action = NotFoundAction.IGNORE)
    private PolicyEntity policy;

    @CreationTimestamp
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @Column(name = "note")
    private String note;

    @Column(name = "status")
    private Integer status;

    @Column(name = "provider")
    private Integer provider;

    @Column(name = "provider_extra_status")
    private String providerExtraStatus;

    @Column(name = "provider_extra_description")
    private String providerExtraDescription;

    @Column(name = "value", columnDefinition = "numeric")
    private Integer value;

    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "data")
    private JsonNode data;

}
