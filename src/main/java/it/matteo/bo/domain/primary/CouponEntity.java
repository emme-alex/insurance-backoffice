package it.matteo.bo.domain.primary;

import it.matteo.bo.domain.validator.StringValuesPattern;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.UpdateTimestamp;

import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@ToString
@Table(
        schema = "promotion",
        name = "coupons",
        uniqueConstraints = @UniqueConstraint(columnNames = {"coupon_code"})
)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CouponEntity implements Serializable {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "coupon_code", nullable = false, updatable = false)
    private String couponCode;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Column(name = "active")
    private boolean active;

    @Column(name = "max_usage")
    private Integer maxUsage;

    @Column(name = "type", nullable = false, updatable = false)
    @StringValuesPattern(acceptedValues = {"single", "multiple"}, message = "Invalid coupon rule type")
    private String type;

    @ManyToOne
    @JoinColumn(name = "promotion_id", columnDefinition = "int4")
    @NotFound(action = NotFoundAction.IGNORE)
    private PromotionEntity promotion;

    @ManyToOne
    @JoinColumn(name = "batch_id", columnDefinition = "int4")
    @NotFound(action = NotFoundAction.IGNORE)
    private PromotionBatchEntity promotionBatch;

    @Column(name = "created_by", nullable = false, updatable = false)
    private String createdBy;

    @Column(name = "updated_by", nullable = false)
    private String updatedBy;

    @CreationTimestamp
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

}
