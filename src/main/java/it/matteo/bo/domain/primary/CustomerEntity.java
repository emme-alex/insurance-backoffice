package it.matteo.bo.domain.primary;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.type.SqlTypes;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(
        schema = "customer",
        name = "customers"
)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerEntity implements Serializable {

    @Id
    @Column(columnDefinition = "int4")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "customer_code", length = 50, updatable = false)
    private String customerCode;

    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "external_code", updatable = false)
    private JsonNode externalCode;

    @Column(name = "username", length = 50)
    private String username;

    @Column(name = "ndg")
    private String ndg;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "surname", length = 50)
    private String surname;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<UserAcceptancesEntity> userAcceptances;

    @Column(name = "date_of_birth", length = 50)
    private String dateOfBirth;

    @Column(name = "birth_city", length = 50)
    private String birthCity;

    @Column(name = "birth_country", length = 50)
    private String birthCountry;

    @Column(name = "birth_state", length = 50)
    private String birthState;

    @Column(name = "tax_code", length = 50, nullable = false)
    private String taxCode;

    @Column(name = "gender", length = 50)
    private String gender;

    @Column(name = "street", length = 50)
    private String street;

    @Column(name = "street_number", length = 50)
    private String streetNumber;

    @Column(name = "city", length = 50)
    private String city;

    @Column(name = "country", length = 50)
    private String country;

    @Column(name = "country_id", columnDefinition = "int4")
    private Long countryId;

    @Column(name = "zip_code", length = 50)
    private String zipCode;

    @Column(name = "state", length = 50)
    private String state;

    @Column(name = "state_id", columnDefinition = "int4")
    private Long stateId;

    @Column(name = "primary_mail", length = 50)
    private String primaryMail;

    @Column(name = "secondary_mail", length = 50)
    private String secondaryMail;

    @Column(name = "primary_phone", length = 50)
    private String primaryPhone;

    @Column(name = "secondary_phone", length = 50)
    private String secondaryPhone;

    @Column(name = "language", length = 50)
    private String language;

    @Column(name = "legal_form", length = 50)
    private String legalForm;

    @Column(name = "education")
    private String education;

    @Column(name = "salary")
    private String salary;

    @Column(name = "profession")
    private String profession;

    @CreationTimestamp
    @Column(name = "created_at", updatable = false)
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

}
