package it.matteo.bo.domain.primary;

import it.matteo.bo.domain.JpaUtils;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.UpdateTimestamp;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(
        schema = "\"order\"",
        name = "\"orders\""
)
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderEntity implements Serializable {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "order_code", unique = true)
    private String orderCode;

    @Column(name = "policy_code")
    private String policyCode;

    @ManyToOne
    @JoinColumn(name = "anag_state_id", nullable = false, columnDefinition = "int4")
    private OrderAnagStatesEntity anagState;

    @ManyToOne
    @JoinColumn(name = "packet_id", columnDefinition = "int4")
    @NotFound(action = NotFoundAction.IGNORE)
    private PacketEntity packet;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false, columnDefinition = "int4")
    private ProductEntity product;

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false, columnDefinition = "int4")
    @NotFound(action = NotFoundAction.IGNORE)
    private CustomerEntity customer;

    @Column(name = "broker_id", columnDefinition = "int4")
    private Long brokerId;

    @Column(name = "company_id", columnDefinition = "int4")
    private Long companyId;

    @Column(name = "insurance_premium", columnDefinition = JpaUtils.NUMERIC_DEF)
    private BigDecimal insurancePremium;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updatedAt;

    @OneToMany(mappedBy = "orders", cascade = CascadeType.PERSIST)
    @ToString.Exclude
    private List<OrderItemEntity> orderItem = new ArrayList<>();

    @Column(name = "payment_transaction_id")
    private Integer paymentTransactionId;

    @Column(name = "payment_token")
    private String paymentToken;

    @Column(name = "product_type")
    private String productType;

}
