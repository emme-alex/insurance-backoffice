package it.matteo.bo.domain.primary;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.time.LocalDateTime;

@Entity
@ToString
@Table(
        schema = "\"order\"",
        name = "order_item"
)
@Getter
@Setter
public class OrderItemEntity {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "price", nullable = false)
    private String price;

    @Column(name = "product_id", nullable = false, columnDefinition = "int4")
    private Long productId;

    @Column(name = "policy_number", nullable = false)
    private String policyNumber;

    @Column(name = "master_policy_number", nullable = false)
    private String masterPolicyNumber;

    @Column(name = "external_id", nullable = false)
    private String externalId;

    @Column(name = "state", nullable = false)
    private String state;

    @Column(name = "start_date", nullable = false)
    private LocalDateTime startDate;

    @Column(name = "expiration_date", nullable = false)
    private LocalDateTime expirationDate;

    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "insured_item", columnDefinition = "jsonb not null")
    private JsonNode insuredItem;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "quotation", columnDefinition = "jsonb not null")
    private JsonNode quotation;

    @ManyToOne
    @JoinColumn(name = "order_id", columnDefinition = "int4")
    private OrderEntity orders;

}
