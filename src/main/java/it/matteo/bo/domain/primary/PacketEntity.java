package it.matteo.bo.domain.primary;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@ToString
@Table(
        schema = "product",
        name = "packets",
        uniqueConstraints = @UniqueConstraint(columnNames = {"name", "broker_id"})
)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PacketEntity {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(name = "broker_id", updatable = false, columnDefinition = "int4")
    private Long brokerId;

    @ManyToOne
    @JoinColumn(name = "product_id", columnDefinition = "int4")
    @NotFound(action = NotFoundAction.IGNORE)
    private ProductEntity product;

    @OneToMany(mappedBy = "packet", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Exclude
    private Set<RelatedWarrantyEntity> warranties = new HashSet<>();

    @Column(nullable = false)
    private String sku;

    @Column(name = "packet_premium", nullable = false, columnDefinition = "numeric")
    private int packetPremium;

    @Column(name = "description")
    private String description;

    @Column(name = "duration")
    private String duration;

    @Column(name = "duration_type")
    private String duration_type;

    @Column(name = "fixed_end_date")
    private LocalDateTime fixedEndDate;

    @Column(name = "fixed_start_date")
    private LocalDateTime fixedStartDate;

    @Column(name = "plan_id")
    private String planId;

    @Column(name = "plan_name")
    private String planName;

}
