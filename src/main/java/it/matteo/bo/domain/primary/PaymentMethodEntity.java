package it.matteo.bo.domain.primary;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Entity
@Table(
        schema = "product",
        name = "payment_methods"
)
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentMethodEntity {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "payment_method", nullable = false)
    private String paymentMethod;

    @Column(name = "payment_method_type")
    private String paymentMethodType;

    @Column(name = "type")
    private String type;

    @Column(name = "active", nullable = false)
    private boolean active = true;

    @Column(name = "external_id", columnDefinition = "int4")
    private Long externalId;

    @OneToMany(mappedBy = "paymentMethod")
    @ToString.Exclude
    private List<ProductsPaymentMethodsEntity> productsPaymentMethods;

}
