package it.matteo.bo.domain.primary;

import com.fasterxml.jackson.databind.JsonNode;
import it.matteo.bo.domain.JpaUtils;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.type.SqlTypes;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@ToString
@Table(
        schema = "policy",
        name = "policies"
)
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PolicyEntity {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "policy_code", nullable = false)
    private String policyCode;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @ManyToOne
    @JoinColumn(name = "anag_state_id", nullable = false, columnDefinition = "int4")
    private PolicyAnagStatesEntity state;

    @ManyToOne
    @JoinColumn(name = "product_id", columnDefinition = "int4")
    private ProductEntity product;

    @ManyToOne
    @JoinColumn(name = "customer_id", columnDefinition = "int4")
    @NotFound(action = NotFoundAction.IGNORE)
    private CustomerEntity customer;

    @Column(name = "payment_id", columnDefinition = "int4")
    private Long paymentId;

    @ManyToOne
    @JoinColumn(name = "order_id", columnDefinition = "int4")
    @NotFound(action = NotFoundAction.IGNORE)
    private OrderEntity order;

    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "choosen_properties", columnDefinition = "jsonb not null")
    private JsonNode choosenProperties;

    @Column(name = "insurance_premium", columnDefinition = JpaUtils.NUMERIC_DEF)
    private BigDecimal insurancePremium;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "policy", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<PolicyWarrantyEntity> warranties = new HashSet<>();

    @CreationTimestamp
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @Column(name = "quantity", columnDefinition = "int4")
    private Long quantity;

    @Column(name = "insured_is_contractor")
    private Boolean insuredIsContractor;

    @Column(name = "certificate_file_name")
    private String certificateFileName;

    @Column(name = "certificate_link")
    private String certificateLink;

    @Column(name = "certificate_content_type")
    private String certificateContentType;

    @Column(name = "certificate_file_size", columnDefinition = "numeric")
    private Integer certificateFileSize;

    @UpdateTimestamp
    @Column(name = "certificate_updated_at")
    private LocalDateTime certificateUpdatedAt;

    @Column(name = "type")
    private String type;

    @Column(name = "withdrawal_request_date")
    private LocalDateTime withdrawalRequestDate;

    @Column(name = "renewed_at")
    private LocalDateTime renewedAt;

    @Column(name = "marked_as_renewable")
    private Boolean markedAsRenewable;

    @Column(name = "master_policy_number")
    private String masterPolicyNumber;

    @Column(name = "payment_frequency")
    private String paymentFrequency;

    @Column(name = "payment_trx")
    private String paymentTrx;

    @Column(name = "payment_token")
    private String paymentToken;

    @OneToMany(mappedBy = "policy", cascade = CascadeType.PERSIST)
    @ToString.Exclude
    private List<ClaimEntity> claims = new ArrayList<>();

    @Column(name = "canceled_at")
    private LocalDateTime canceledAt;

}
