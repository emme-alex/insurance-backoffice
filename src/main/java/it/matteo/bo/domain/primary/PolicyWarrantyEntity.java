package it.matteo.bo.domain.primary;

import it.matteo.bo.domain.JpaUtils;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@ToString
@Table(
        schema = "policy",
        name = "warranty_policies"
)
@Getter
@Setter
public class PolicyWarrantyEntity {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @ToString.Exclude
    @JoinColumn(name = "policy_id", nullable = false, updatable = false, columnDefinition = "int4")
    private PolicyEntity policy;

    @Column(name = "warranties_id", nullable = false, columnDefinition = "int4")
    private Long warrantyId;

    @Column(name = "insurance_premium", columnDefinition = JpaUtils.NUMERIC_DEF)
    private BigDecimal insurancePremium;

    @JoinColumn(nullable = false)
    private boolean mandatory;

    @Column(name = "ceiling", columnDefinition = JpaUtils.NUMERIC_DEF)
    private BigDecimal ceiling;

    @Column(name = "created_by", nullable = false, updatable = false)
    private String createdBy;

    @Column(name = "updated_by", nullable = false)
    private String updatedBy;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updatedAt;

}
