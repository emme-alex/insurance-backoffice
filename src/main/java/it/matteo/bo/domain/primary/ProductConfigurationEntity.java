package it.matteo.bo.domain.primary;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@ToString
@Table(
        schema = "product",
        name = "configuration"
)
@Getter
@Setter
public class ProductConfigurationEntity {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "product_id", columnDefinition = "int4")
    @NotFound(action = NotFoundAction.IGNORE)
    private ProductEntity product;

    @Column(name = "emission")
    private String emission;

    @Column(name = "emission_prefix")
    private String emissionPrefix;

    @Column(name = "certificate")
    private String certificate;

    @Column(name = "can_open_claim", nullable = false)
    private Boolean canOpenClaim = false;

    @Column(name = "claim_type")
    private String claimType;

    @Column(name = "claim_provider")
    private String claimProvider;

    @Column(name = "withdraw_type")
    private String withdrawType;

    @Column(name = "deactivate_type")
    private String deactivateType;

}
