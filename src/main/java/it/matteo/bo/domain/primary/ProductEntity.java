package it.matteo.bo.domain.primary;

import com.fasterxml.jackson.databind.JsonNode;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.OrderColumn;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.ListIndexBase;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.type.SqlTypes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@ToString
@Table(
        schema = "product",
        name = "products",
        uniqueConstraints = @UniqueConstraint(columnNames = {"product_code"})
)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProductEntity extends PanacheEntityBase {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_code", nullable = false)
    private String code;

    @Column(name = "product_description")
    private String productDescription;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "recurring")
    private boolean recurring;

    @Column(name = "duration")
    private Integer duration;

    @Column(name = "duration_type")
    private String duration_type;

    @Column(name = "external_id")
    private String externalId;

    @Column(name = "insurance_company")
    private String insuranceCompany;

    @Column(name = "insurance_company_logo")
    private String insuranceCompanyLogo;

    @Column(name = "insurance_premium", columnDefinition = "numeric")
    private int insurancePremium;

    @Column(nullable = false)
    private String business;

    @Column(name = "title_prod")
    private String titleProd;

    @Column(name = "short_description")
    private String shortDescription;

    @Column(name = "description")
    private String description;

    @Column(nullable = false)
    private String conditions;

    @Column(name = "information_package")
    private String informationPackage;

    @Column(name = "conditions_package")
    private String conditionsPackage;

    @Column(name = "display_price")
    private String displayPrice;

    @Column(name = "price", columnDefinition = "numeric")
    private Double price;

    @Column(name = "only_contractor", nullable = false)
    private boolean onlyContractor;

    @Column(name = "maximum_insurable", nullable = false, columnDefinition = "numeric")
    private int maximumInsurable;

    @Column(name = "can_open_claim", nullable = false)
    private boolean canOpenClaim;

    @Column(name = "holder_maximum_age", nullable = false, columnDefinition = "numeric")
    private int holderMaximumAge;

    @Column(name = "holder_minimum_age", nullable = false, columnDefinition = "numeric")
    private int holderMinimumAge;

    @Column(name = "show_in_dashboard", nullable = false)
    private boolean showInDashboard;

    @OneToOne
    @JoinColumn(name = "product_image_id", columnDefinition = "int4")
    private ProductImagesEntity image;

    @Column(name = "catalog_id")
    private Integer catalogId;

    @JdbcTypeCode(SqlTypes.JSON)
    @Column(columnDefinition = "jsonb not null")
    private JsonNode properties;

    @Column(name = "quotator_type")
    private String quotatorType;

    @Column(name = "show_addons_in_shopping_cart")
    private boolean showAddonsInShoppingCart;

    @Column(name = "thumbnail")
    private boolean thumbnail;

    @Column(name = "privacy_documentation_link")
    private String privacyDocumentationLink;

    @Column(name = "informative_set")
    private String informativeSet;

    @Column(name = "attachment_3_4")
    private String attachment34;

    @JdbcTypeCode(SqlTypes.JSON)
    private JsonNode extras;

    @Column(name = "plan_id")
    private String planId;

    @Column(name = "plan_name")
    private String planName;

    @Column(name = "product_type")
    private String productType;

    @JdbcTypeCode(SqlTypes.JSON)
    @Column(columnDefinition = "jsonb not null")
    private JsonNode legacy;

    @Column(name = "medium_tax_ratio", columnDefinition = "float8")
    private Double mediumTaxRatio;

    @Column(name = "ia_code")
    private String iaCode;

    @Column(name = "ia_net_commission", columnDefinition = "float8")
    private Double iaNetCommission;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Exclude
    private List<SplitEntity> splits = new ArrayList<>();

    @ListIndexBase(value = 1)
    @OrderColumn(name = "position")
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Exclude
    private List<SurveyQuestionEntity> questions = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "product_categories",
            schema = "product",
            joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id", columnDefinition = "int4"),
            inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id", columnDefinition = "int4")
    )
    @ToString.Exclude
    private Set<CategoryEntity> categories = new HashSet<>();

    @OneToMany(mappedBy = "product")
    @ToString.Exclude
    private List<PacketEntity> packets = new ArrayList<>();

    @OneToMany(mappedBy = "product")
    @ToString.Exclude
    private List<ProductsPaymentMethodsEntity> productsPaymentMethods;

    @OneToMany(mappedBy = "product")
    @ToString.Exclude
    private List<ProductConfigurationEntity> configurations = new ArrayList<>();

    @ManyToMany(mappedBy = "products")
    @ToString.Exclude
    @NotFound(action = NotFoundAction.IGNORE)
    private Set<UtmSourceEntity> utmSources = new HashSet<>();

}