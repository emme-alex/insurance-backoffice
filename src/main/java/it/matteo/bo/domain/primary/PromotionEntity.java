package it.matteo.bo.domain.primary;

import it.matteo.bo.domain.validator.StringValuesPattern;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@ToString
@Table(
        schema = "promotion",
        name = "promotions",
        uniqueConstraints = @UniqueConstraint(columnNames = {"promotion_code"})
)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PromotionEntity implements Serializable {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "promotion_code", nullable = false)
    private String promotionCode;

    @Column(name = "description")
    private String description;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Column(name = "active")
    private boolean active;

    @Column(name = "code", nullable = false, updatable = false)
    @StringValuesPattern(acceptedValues = {"internal", "provider"}, message = "Invalid promotion code")
    private String code;

    @ManyToOne
    @JoinColumn(name = "rule_id", columnDefinition = "int4")
    private PromotionRuleEntity rule;

    @OneToMany(mappedBy = "promotion")
    @ToString.Exclude
    private List<CouponEntity> coupons = new ArrayList<>();

    @OneToMany(mappedBy = "promotion")
    @ToString.Exclude
    private List<PromotionBatchEntity> promotionBatch = new ArrayList<>();

    @Column(name = "created_by", nullable = false, updatable = false)
    private String createdBy;

    @Column(name = "updated_by", nullable = false)
    private String updatedBy;

    @CreationTimestamp
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

}
