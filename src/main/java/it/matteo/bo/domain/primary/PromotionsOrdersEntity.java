package it.matteo.bo.domain.primary;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.UpdateTimestamp;

import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@ToString
@Table(
        schema = "promotion",
        name = "promotions_orders"
)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PromotionsOrdersEntity implements Serializable {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "order_code", nullable = false, referencedColumnName = "order_code")
    private OrderEntity order;

    @ManyToOne
    @JoinColumn(name = "promotion_code", nullable = false, referencedColumnName = "promotion_code")
    private PromotionEntity promotion;

    @ManyToOne
    @JoinColumn(name = "coupon_code", referencedColumnName = "coupon_code")
    private CouponEntity coupon;

    @ManyToOne
    @JoinColumn(name = "customer_code", referencedColumnName = "tax_code")
    @NotFound(action = NotFoundAction.IGNORE)
    private CustomerEntity customer;

    @CreationTimestamp
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @Column(name = "created_by", nullable = false, updatable = false)
    private String createdBy;

    @Column(name = "updated_by", nullable = false)
    private String updatedBy;

}
