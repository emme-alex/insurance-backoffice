package it.matteo.bo.domain.primary;

import com.fasterxml.jackson.databind.JsonNode;
import it.matteo.bo.domain.JpaUtils;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@ToString
@Table(
        schema = "product",
        name = "related_warranties",
        uniqueConstraints = @UniqueConstraint(columnNames = {"packet_id", "warranties_id"})
)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RelatedWarrantyEntity {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @ToString.Exclude
    @JoinColumn(name = "packet_id", nullable = false, updatable = false, columnDefinition = "int4")
    private PacketEntity packet;

    @ManyToOne
    @JoinColumn(name = "category_id", columnDefinition = "int4")
    private CategoryEntity category;

    @ManyToOne
    @JoinColumn(name = "warranties_id", nullable = false, updatable = false, columnDefinition = "int4")
    private WarrantyEntity warranty;

    @OneToOne
    @JoinColumn(name = "parent_id", columnDefinition = "int4")
    private RelatedWarrantyEntity parent;

    @JoinColumn(nullable = false)
    private boolean mandatory;

    @Column(name = "insurance_premium", columnDefinition = JpaUtils.NUMERIC_DEF)
    private BigDecimal insurancePremium;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(nullable = false)
    private boolean recurring;

    @JdbcTypeCode(SqlTypes.JSON)
    private JsonNode rule;

    @Column(name = "external_code")
    private String externalCode;

    @JdbcTypeCode(SqlTypes.JSON)
    private JsonNode ceilings;

    @JdbcTypeCode(SqlTypes.JSON)
    private JsonNode taxons;

}
