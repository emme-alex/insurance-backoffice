package it.matteo.bo.domain.primary;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@Table(
        schema = "product",
        name = "survey_answers"
)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SurveyAnswerEntity {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String value;

    @ManyToOne
    @ToString.Exclude
    @JoinColumn(name = "survey_questions_id", nullable = false, updatable = false, columnDefinition = "int4")
    private SurveyQuestionEntity question;

    @Column(nullable = false)
    private int position;

    @Column(name = "default_value", nullable = false)
    private boolean defaultValue;

    @Column(nullable = false)
    private String rule;

    @Column(name = "external_code")
    private String externalCode;

}
