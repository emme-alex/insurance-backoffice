package it.matteo.bo.domain.primary;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderColumn;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.ListIndexBase;

import java.util.ArrayList;
import java.util.List;

@Entity
@ToString
@Table(
        schema = "product",
        name = "survey_questions"
)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SurveyQuestionEntity {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String content;

    @ManyToOne
    @ToString.Exclude
    @JoinColumn(name = "product_id", nullable = false, updatable = false, columnDefinition = "int4")
    private ProductEntity product;

    @Column(nullable = false)
    private int position;

    @Column(name = "external_code")
    private String externalCode;

    @ListIndexBase(1)
    @OrderColumn(name = "position")
    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Exclude
    private List<SurveyAnswerEntity> answers = new ArrayList<>();

}
