package it.matteo.bo.domain.primary;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@ToString
@Table(
        schema = "product",
        name = "utm_sources",
        uniqueConstraints = @UniqueConstraint(columnNames = {"code"})
)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UtmSourceEntity extends PanacheEntityBase {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "created_by", nullable = false, updatable = false)
    private String createdBy;

    @Column(name = "updated_by", nullable = false)
    private String updatedBy;

    @CreationTimestamp
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @ManyToMany
    @JoinTable(
            name = "utm_sources_products",
            schema = "product",
            joinColumns = @JoinColumn(name = "utm_source_id", referencedColumnName = "id", columnDefinition = "int4"),
            inverseJoinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id", columnDefinition = "int4")
    )
    @ToString.Exclude
    private Set<ProductEntity> products = new HashSet<>();

    @OneToMany(mappedBy = "utmSource", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Exclude
    private Set<UtmSourceLocationEntity> locations = new HashSet<>();

    @OneToMany(mappedBy = "utmSource", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @ToString.Exclude
    private Set<GiftCardEntity> giftCards = new HashSet<>();

}