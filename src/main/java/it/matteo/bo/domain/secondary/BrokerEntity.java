package it.matteo.bo.domain.secondary;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Entity
@Table(
        schema = "`catalog`",
        name = "brokers"
)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BrokerEntity {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "identitycode", nullable = false)
    private String identitycode;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "broker")
    @ToString.Exclude
    private List<CompanyBrokerEntity> companiesBrokers;

}

