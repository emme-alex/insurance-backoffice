package it.matteo.bo.domain.secondary;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

@Entity
@Table(
        schema = "`catalog`",
        name = "catalog"
)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CatalogEntity {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "product_code", nullable = false)
    private String productCode;

    @Column(name = "insurance_company", nullable = false)
    private String company;

    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "asset", columnDefinition = "jsonb not null")
    private JsonNode asset;

    @Column(name = "revision", nullable = false)
    private String revision;

    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

}

