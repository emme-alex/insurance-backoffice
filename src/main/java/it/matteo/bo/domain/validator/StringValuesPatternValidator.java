package it.matteo.bo.domain.validator;

import io.micrometer.common.util.StringUtils;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.ArrayList;
import java.util.List;

public class StringValuesPatternValidator implements ConstraintValidator<StringValuesPattern, String> {

    private List<String> valueList;

    @Override
    public void initialize(final StringValuesPattern constraintAnnotation) {
        valueList = new ArrayList<>();
        for (final String val : constraintAnnotation.acceptedValues()) {
            valueList.add(StringUtils.isNotBlank(val) ? val.toLowerCase() : val);
        }
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return StringUtils.isNotBlank(value) && valueList.contains(value.toLowerCase());
    }

}