package it.matteo.bo.exception;

public record BoRestException(String message, String details) {
}
