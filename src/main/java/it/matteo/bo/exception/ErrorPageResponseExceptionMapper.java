package it.matteo.bo.exception;

import it.matteo.bo.model.ErrorBoundaryResponse;
import jakarta.persistence.PersistenceException;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class ErrorPageResponseExceptionMapper implements ExceptionMapper<Exception> {

    private static final Logger LOG = LoggerFactory.getLogger(ErrorPageResponseExceptionMapper.class);

    @Override
    public Response toResponse(final Exception exception) {
        if (exception instanceof RelatedWarrantyRulesException) {
            LOG.error("RelatedWarranty Rules validation error", exception);
            return Response
                    .status(422)
                    .entity(new ErrorBoundaryResponse<>(
                            ((RelatedWarrantyRulesException) exception).getRelatedWarrantyRulesExceptionTOs())
                    )
                    .build();
        }
        if (exception instanceof WebApplicationException) {
            LOG.error("WebApplicationException", exception);
            return Response
                    .fromResponse(((WebApplicationException) exception).getResponse())
                    .entity(new BoRestException(
                            ExceptionUtils.getMessage(exception),
                            ExceptionUtils.getRootCauseMessage(exception)))
                    .build();
        }
        if (exception instanceof NullPointerException) {
            LOG.error("Critical server error", exception);
            return Response
                    .serverError()
                    .entity(new BoRestException(
                            ExceptionUtils.getMessage(exception),
                            ExceptionUtils.getRootCauseMessage(exception)))
                    .build();
        }
        if (exception instanceof IllegalArgumentException || exception instanceof EntityNotFoundException) {
            LOG.error(exception.getMessage(), exception);
            return Response
                    .status(400)
                    .entity(new BoRestException(
                            ExceptionUtils.getMessage(exception),
                            ExceptionUtils.getRootCauseMessage(exception)))
                    .build();
        }
        if (exception instanceof DataValidationException) {
            LOG.error("Data validation error", exception);
            return Response
                    .status(422)
                    .entity(new BoRestException(
                            ExceptionUtils.getMessage(exception),
                            ExceptionUtils.getRootCauseMessage(exception)))
                    .build();
        }
        if (exception instanceof PersistenceException) {
            LOG.error("DB error", exception);
            return Response
                    .serverError()
                    .entity(new BoRestException(
                            ExceptionUtils.getMessage(exception),
                            ExceptionUtils.getRootCauseMessage(exception)))
                    .build();
        }
        if (exception instanceof ExternalServiceException) {
            LOG.error("Error with external rest service", exception);
            return Response
                    .serverError()
                    .entity(new BoRestException(
                            ExceptionUtils.getMessage(exception),
                            ExceptionUtils.getRootCauseMessage(exception)))
                    .build();
        }
        LOG.error("Generic error", exception);
        return Response
                .serverError()
                .entity(new BoRestException(
                        ExceptionUtils.getMessage(exception),
                        ExceptionUtils.getRootCauseMessage(exception)))
                .build();
    }

}
