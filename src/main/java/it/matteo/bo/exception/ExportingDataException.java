package it.matteo.bo.exception;

public class ExportingDataException extends RuntimeException {

    public ExportingDataException(final String message) {
        super(message);
    }

}
