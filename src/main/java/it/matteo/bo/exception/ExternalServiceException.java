package it.matteo.bo.exception;

public class ExternalServiceException extends RuntimeException {

    public ExternalServiceException(final String message) {
        super(message);
    }

}
