package it.matteo.bo.exception;

import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.ext.ResponseExceptionMapper;

public class ExternalServiceExceptionMapper implements ResponseExceptionMapper<ExternalServiceException> {

    @Override
    public ExternalServiceException toThrowable(final Response resp) {
        return new ExternalServiceException(resp.getStatus() + " - " + resp.readEntity(String.class));
    }

}
