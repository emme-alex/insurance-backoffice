package it.matteo.bo.exception;

import it.matteo.bo.model.RelatedWarrantyRulesExceptionTO;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class RelatedWarrantyRulesException extends RuntimeException {

    private final List<RelatedWarrantyRulesExceptionTO> relatedWarrantyRulesExceptionTOs = new ArrayList<>();

    public RelatedWarrantyRulesException(final List<RelatedWarrantyRulesExceptionTO> exs) {
        super(exs.stream()
                .map(ex -> ex.getErrorCode().name())
                .collect(Collectors.joining(", ")));
        this.relatedWarrantyRulesExceptionTOs.addAll(exs);
    }

}
