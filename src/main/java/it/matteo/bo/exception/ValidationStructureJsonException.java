package it.matteo.bo.exception;

public class ValidationStructureJsonException extends RuntimeException {

    public ValidationStructureJsonException(final String message) {
        super(message);
    }

    public ValidationStructureJsonException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
