package it.matteo.bo.mapper;


import it.matteo.bo.domain.secondary.BrokerEntity;
import it.matteo.bo.model.secondary.BrokerBoundaryRequest;
import it.matteo.bo.model.secondary.BrokerBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BrokerMapper extends
        GenericBoundaryMapper<BrokerBoundaryRequest, BrokerBoundaryResponse, BrokerEntity> {

    BrokerMapper INSTANCE = Mappers.getMapper(BrokerMapper.class);

}
