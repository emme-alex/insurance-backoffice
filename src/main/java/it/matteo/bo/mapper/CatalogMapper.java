package it.matteo.bo.mapper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import it.matteo.bo.domain.secondary.CatalogEntity;
import it.matteo.bo.model.secondary.CatalogBoundaryRequest;
import it.matteo.bo.model.secondary.CatalogBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CatalogMapper extends
        GenericBoundaryMapper<CatalogBoundaryRequest, CatalogBoundaryResponse, CatalogEntity> {

    CatalogMapper INSTANCE = Mappers.getMapper(CatalogMapper.class);

    default JsonNode toString(final String elem) {
        return JsonNodeFactory.instance.objectNode().putObject(elem);
    }

}
