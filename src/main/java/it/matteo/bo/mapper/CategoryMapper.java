package it.matteo.bo.mapper;

import it.matteo.bo.domain.primary.CategoryEntity;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.model.primary.CategoryBoundaryRequest;
import it.matteo.bo.model.primary.CategoryBoundaryResponse;
import it.matteo.bo.model.primary.ProductReferenceBoundaryRequest;
import it.matteo.bo.model.primary.ProductReferenceBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CategoryMapper extends
        GenericBoundaryMapper<CategoryBoundaryRequest, CategoryBoundaryResponse, CategoryEntity> {

    CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);

    ProductEntity requestToEntity(ProductReferenceBoundaryRequest request);

    ProductReferenceBoundaryResponse entityToResponse(ProductEntity entity);

}
