package it.matteo.bo.mapper;


import it.matteo.bo.domain.primary.ClaimEntity;
import it.matteo.bo.model.primary.ClaimBoundaryRequest;
import it.matteo.bo.model.primary.ClaimBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ClaimMapper extends
        GenericBoundaryMapper<ClaimBoundaryRequest, ClaimBoundaryResponse, ClaimEntity> {

    ClaimMapper INSTANCE = Mappers.getMapper(ClaimMapper.class);

    @Override
    ClaimEntity requestToEntity(ClaimBoundaryRequest request);

    @Override
    ClaimBoundaryRequest entityToRequest(ClaimEntity request);

}
