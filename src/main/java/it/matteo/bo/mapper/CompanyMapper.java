package it.matteo.bo.mapper;


import it.matteo.bo.domain.secondary.CompanyEntity;
import it.matteo.bo.model.secondary.CompanyBoundaryRequest;
import it.matteo.bo.model.secondary.CompanyBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CompanyMapper extends
        GenericBoundaryMapper<CompanyBoundaryRequest, CompanyBoundaryResponse, CompanyEntity> {

    CompanyMapper INSTANCE = Mappers.getMapper(CompanyMapper.class);

}
