package it.matteo.bo.mapper;


import it.matteo.bo.domain.primary.ConfigurationEntity;
import it.matteo.bo.model.primary.ConfigurationBoundaryRequest;
import it.matteo.bo.model.primary.ConfigurationBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ConfigurationMapper extends
        GenericBoundaryMapper<ConfigurationBoundaryRequest, ConfigurationBoundaryResponse, ConfigurationEntity> {

    ConfigurationMapper INSTANCE = Mappers.getMapper(ConfigurationMapper.class);

}
