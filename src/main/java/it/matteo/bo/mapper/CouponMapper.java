package it.matteo.bo.mapper;


import it.matteo.bo.domain.primary.CouponEntity;
import it.matteo.bo.model.primary.CouponBoundaryRequest;
import it.matteo.bo.model.primary.CouponBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CouponMapper extends
        GenericBoundaryMapper<CouponBoundaryRequest, CouponBoundaryResponse, CouponEntity> {

    CouponMapper INSTANCE = Mappers.getMapper(CouponMapper.class);

    @Mapping(target = "promotion.id", source = "promotionId")
    @Override
    CouponEntity requestToEntity(CouponBoundaryRequest request);

    @Mapping(target = "promotionId", source = "promotion.id")
    @Override
    CouponBoundaryRequest entityToRequest(CouponEntity request);

}
