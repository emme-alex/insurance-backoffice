package it.matteo.bo.mapper;


import it.matteo.bo.domain.primary.CustomerEntity;
import it.matteo.bo.model.primary.CustomerBoundaryRequest;
import it.matteo.bo.model.primary.CustomerBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CustomerMapper extends
        GenericBoundaryMapper<CustomerBoundaryRequest, CustomerBoundaryResponse, CustomerEntity> {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

}
