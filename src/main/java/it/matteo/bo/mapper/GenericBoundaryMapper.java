package it.matteo.bo.mapper;

import java.util.List;

public interface GenericBoundaryMapper<Request, Response, Entity> {

    Entity requestToEntity(Request request);

    Response entityToResponse(Entity entity);

    List<Response> entitiesToResponses(List<Entity> entity);

    Request entityToRequest(Entity entity);

}
