package it.matteo.bo.mapper;

import it.matteo.bo.domain.primary.GiftCardEntity;
import it.matteo.bo.model.primary.GiftCardBoundaryRequest;
import it.matteo.bo.model.primary.GiftCardBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface GiftCardMapper extends
        GenericBoundaryMapper<GiftCardBoundaryRequest, GiftCardBoundaryResponse, GiftCardEntity> {

    GiftCardMapper INSTANCE = Mappers.getMapper(GiftCardMapper.class);

    @Mapping(target = "utmSource.id", source = "utmSourceId")
    @Override
    GiftCardEntity requestToEntity(GiftCardBoundaryRequest request);

    @Mapping(target = "utmSourceId", source = "utmSource.id")
    @Override
    GiftCardBoundaryRequest entityToRequest(GiftCardEntity entity);

}

