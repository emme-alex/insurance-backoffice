package it.matteo.bo.mapper;

import it.matteo.bo.domain.primary.OrderEntity;
import it.matteo.bo.model.primary.OrderBoundaryRequest;
import it.matteo.bo.model.primary.OrderBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderMapper extends
        GenericBoundaryMapper<OrderBoundaryRequest, OrderBoundaryResponse, OrderEntity> {

    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    @Mapping(target = "anagState.id", source = "anagStateId")
    @Mapping(target = "product.id", source = "productId")
    @Mapping(target = "packet.id", source = "packetId")
    @Mapping(target = "customer.id", source = "customerId")
    @Override
    OrderEntity requestToEntity(OrderBoundaryRequest request);

}
