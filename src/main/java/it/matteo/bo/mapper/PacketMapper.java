package it.matteo.bo.mapper;

import it.matteo.bo.domain.primary.BranchEntity;
import it.matteo.bo.domain.primary.CategoryEntity;
import it.matteo.bo.domain.primary.PacketEntity;
import it.matteo.bo.domain.primary.RelatedWarrantyEntity;
import it.matteo.bo.model.primary.BranchBoundaryResponse;
import it.matteo.bo.model.primary.CategoryReferenceBoundaryRequest;
import it.matteo.bo.model.primary.CategoryReferenceBoundaryResponse;
import it.matteo.bo.model.primary.PacketBoundaryRequest;
import it.matteo.bo.model.primary.PacketBoundaryResponse;
import it.matteo.bo.model.primary.RelatedWarrantyBoundaryRequest;
import it.matteo.bo.model.primary.WarrantyBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface PacketMapper extends
        GenericBoundaryMapper<PacketBoundaryRequest, PacketBoundaryResponse, PacketEntity> {

    PacketMapper INSTANCE = Mappers.getMapper(PacketMapper.class);

    @Override
    @Mapping(target = "product.id", source = "productId")
    PacketEntity requestToEntity(PacketBoundaryRequest request);

    @Override
    @Mapping(target = "product.id", source = "product.id")
    PacketBoundaryResponse entityToResponse(PacketEntity entity);

    RelatedWarrantyEntity requestToEntity(RelatedWarrantyBoundaryRequest request);

    default WarrantyBoundaryResponse entityToResponse(final RelatedWarrantyEntity entity) {
        if (entity == null) {
            return null;
        }
        final WarrantyBoundaryResponse warrantyBoundaryResponse = new WarrantyBoundaryResponse();
        warrantyBoundaryResponse.setId(entity.getWarranty().getId());
        warrantyBoundaryResponse.setName(entity.getWarranty().getName());
        warrantyBoundaryResponse.setImages(entity.getWarranty().getImages());
        warrantyBoundaryResponse.setCeilings(entity.getWarranty().getCeilings());
        warrantyBoundaryResponse.setBranch(
                entityToResponse(entity.getWarranty().getBranch()));
        return warrantyBoundaryResponse;
    }

    BranchBoundaryResponse entityToResponse(BranchEntity entity);

    CategoryEntity requestToEntity(CategoryReferenceBoundaryRequest request);

    CategoryReferenceBoundaryResponse entityToResponse(CategoryEntity entity);

    List<PacketBoundaryResponse> entitiesToResponses(List<PacketEntity> entities);

}
