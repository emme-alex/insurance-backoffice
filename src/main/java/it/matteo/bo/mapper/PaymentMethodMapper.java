package it.matteo.bo.mapper;


import it.matteo.bo.domain.primary.PaymentMethodEntity;
import it.matteo.bo.model.primary.PaymentMethodBoundaryRequest;
import it.matteo.bo.model.primary.PaymentMethodBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PaymentMethodMapper extends
        GenericBoundaryMapper<PaymentMethodBoundaryRequest, PaymentMethodBoundaryResponse, PaymentMethodEntity> {

    PaymentMethodMapper INSTANCE = Mappers.getMapper(PaymentMethodMapper.class);

}
