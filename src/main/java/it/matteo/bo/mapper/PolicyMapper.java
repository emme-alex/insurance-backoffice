package it.matteo.bo.mapper;


import it.matteo.bo.domain.primary.PolicyEntity;
import it.matteo.bo.domain.primary.PolicyWarrantyEntity;
import it.matteo.bo.model.primary.PolicyBoundaryRequest;
import it.matteo.bo.model.primary.PolicyBoundaryResponse;
import it.matteo.bo.model.primary.PolicyWarrantyBoundaryRequest;
import it.matteo.bo.model.primary.PolicyWarrantyBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface PolicyMapper extends
        GenericBoundaryMapper<PolicyBoundaryRequest, PolicyBoundaryResponse, PolicyEntity> {

    PolicyMapper INSTANCE = Mappers.getMapper(PolicyMapper.class);

    @Override
    @Mapping(target = "product.id", source = "product.id")
    @Mapping(target = "paymentId", source = "payment.id")
    @Mapping(target = "customer.id", source = "customer.id")
    @Mapping(target = "order.id", source = "orderId")
    @Mapping(target = "paymentFrequency", source = "payment.paymentFrequency")
    @Mapping(target = "paymentTrx", source = "payment.paymentTrx")
    @Mapping(target = "paymentToken", source = "payment.paymentToken")
    @Mapping(target = "state.id", source = "stateId")
    PolicyEntity requestToEntity(PolicyBoundaryRequest request);

    @Override
    @Mapping(target = "product.id", source = "product.id")
    @Mapping(target = "payment.id", source = "paymentId")
    @Mapping(target = "customer.id", source = "customer.id")
    @Mapping(target = "order.id", source = "order.id")
    @Mapping(target = "payment.paymentFrequency", source = "paymentFrequency")
    @Mapping(target = "payment.paymentTrx", source = "paymentTrx")
    @Mapping(target = "payment.paymentToken", source = "paymentToken")
    PolicyBoundaryResponse entityToResponse(PolicyEntity entity);

    @Mapping(target = "warrantyId", source = "warranty.id")
    PolicyWarrantyEntity requestToEntity(PolicyWarrantyBoundaryRequest request);

    @Mapping(target = "warranty.id", source = "warrantyId")
    PolicyWarrantyBoundaryResponse entityToResponse(PolicyWarrantyEntity entity);

    List<PolicyBoundaryResponse> entitiesToResponses(List<PolicyEntity> entities);

}
