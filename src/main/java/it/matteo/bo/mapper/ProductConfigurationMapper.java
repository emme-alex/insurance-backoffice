package it.matteo.bo.mapper;

import it.matteo.bo.domain.primary.ProductConfigurationEntity;
import it.matteo.bo.model.primary.ProductConfigurationBoundaryRequest;
import it.matteo.bo.model.primary.ProductConfigurationBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductConfigurationMapper extends
        GenericBoundaryMapper<ProductConfigurationBoundaryRequest, ProductConfigurationBoundaryResponse, ProductConfigurationEntity> {

    ProductConfigurationMapper INSTANCE = Mappers.getMapper(ProductConfigurationMapper.class);

    @Mapping(target = "product.id", source = "productId")
    @Override
    ProductConfigurationEntity requestToEntity(ProductConfigurationBoundaryRequest request);

}
