package it.matteo.bo.mapper;

import it.matteo.bo.domain.primary.CategoryEntity;
import it.matteo.bo.domain.primary.PacketEntity;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.domain.primary.ProductImagesEntity;
import it.matteo.bo.domain.primary.SurveyAnswerEntity;
import it.matteo.bo.domain.primary.SurveyQuestionEntity;
import it.matteo.bo.domain.primary.UtmSourceEntity;
import it.matteo.bo.model.primary.CategoryReferenceBoundaryRequest;
import it.matteo.bo.model.primary.CategoryReferenceBoundaryResponse;
import it.matteo.bo.model.primary.PacketBoundaryRequest;
import it.matteo.bo.model.primary.PacketReferenceBoundaryResponse;
import it.matteo.bo.model.primary.ProductBoundaryRequest;
import it.matteo.bo.model.primary.ProductBoundaryResponse;
import it.matteo.bo.model.primary.ProductImagesBoundaryRequest;
import it.matteo.bo.model.primary.ProductImagesBoundaryResponse;
import it.matteo.bo.model.primary.SurveyAnswerBoundaryRequest;
import it.matteo.bo.model.primary.SurveyAnswerBoundaryResponse;
import it.matteo.bo.model.primary.SurveyQuestionBoundaryRequest;
import it.matteo.bo.model.primary.SurveyQuestionBoundaryResponse;
import it.matteo.bo.model.primary.UtmSourceBoundaryRequest;
import it.matteo.bo.model.primary.UtmSourceBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ProductMapper extends
        GenericBoundaryMapper<ProductBoundaryRequest, ProductBoundaryResponse, ProductEntity> {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

//    @Override
//    @Mapping(target = "catalog.id", source = "catalog.id")
//    ProductBoundaryResponse entityToResponse(ProductEntity entity);

    List<ProductBoundaryResponse> entityToResponse(List<ProductEntity> entity);

    @Mapping(target = "product.id", source = "productId")
    SurveyQuestionEntity requestToEntity(SurveyQuestionBoundaryRequest request);

    SurveyQuestionBoundaryResponse entityToResponse(SurveyQuestionEntity entity);

    SurveyAnswerEntity requestToEntity(SurveyAnswerBoundaryRequest request);

    SurveyAnswerBoundaryResponse entityToResponse(SurveyAnswerEntity entity);

    CategoryEntity requestToEntity(CategoryReferenceBoundaryRequest request);

    CategoryReferenceBoundaryResponse entityToResponse(CategoryEntity entity);

    ProductImagesEntity requestToEntity(ProductImagesBoundaryRequest request);

    ProductImagesBoundaryResponse entityToResponse(ProductImagesEntity entity);

//    CatalogEntity requestToEntity(CatalogBoundaryRequest request);
//
//    CatalogBoundaryResponse entityToResponse(CatalogEntity entity);

    PacketEntity requestToEntity(PacketBoundaryRequest request);

    PacketReferenceBoundaryResponse entityToResponse(PacketEntity entity);

    UtmSourceEntity requestToEntity(UtmSourceBoundaryRequest request);

    UtmSourceBoundaryResponse entityToResponse(UtmSourceEntity entity);

}
