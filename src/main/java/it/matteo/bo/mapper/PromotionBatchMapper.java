package it.matteo.bo.mapper;

import it.matteo.bo.domain.primary.PromotionBatchEntity;
import it.matteo.bo.model.primary.PromotionBatchBoundaryRequest;
import it.matteo.bo.model.primary.PromotionBatchBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PromotionBatchMapper extends
        GenericBoundaryMapper<PromotionBatchBoundaryRequest, PromotionBatchBoundaryResponse, PromotionBatchEntity> {

    PromotionBatchMapper INSTANCE = Mappers.getMapper(PromotionBatchMapper.class);

    @Mapping(target = "promotion.id", source = "promotionId")
    @Override
    PromotionBatchEntity requestToEntity(PromotionBatchBoundaryRequest request);

    @Mapping(target = "promotionId", source = "promotion.id")
    @Override
    PromotionBatchBoundaryRequest entityToRequest(PromotionBatchEntity request);

}
