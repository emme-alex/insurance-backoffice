package it.matteo.bo.mapper;


import it.matteo.bo.domain.primary.PromotionEntity;
import it.matteo.bo.model.primary.PromotionBoundaryRequest;
import it.matteo.bo.model.primary.PromotionBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PromotionMapper extends
        GenericBoundaryMapper<PromotionBoundaryRequest, PromotionBoundaryResponse, PromotionEntity> {

    PromotionMapper INSTANCE = Mappers.getMapper(PromotionMapper.class);

    @Mapping(target = "rule.id", source = "ruleId")
    @Override
    PromotionEntity requestToEntity(PromotionBoundaryRequest request);

    @Mapping(target = "ruleId", source = "rule.id")
    @Override
    PromotionBoundaryRequest entityToRequest(PromotionEntity request);

}
