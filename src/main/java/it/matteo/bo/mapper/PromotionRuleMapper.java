package it.matteo.bo.mapper;


import it.matteo.bo.domain.primary.PromotionRuleEntity;
import it.matteo.bo.model.primary.PromotionRuleBoundaryRequest;
import it.matteo.bo.model.primary.PromotionRuleBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PromotionRuleMapper extends
        GenericBoundaryMapper<PromotionRuleBoundaryRequest, PromotionRuleBoundaryResponse, PromotionRuleEntity> {

    PromotionRuleMapper INSTANCE = Mappers.getMapper(PromotionRuleMapper.class);

}
