package it.matteo.bo.mapper;

import it.matteo.bo.domain.primary.PromotionsOrdersEntity;
import it.matteo.bo.model.primary.PromotionsOrdersBoundaryRequest;
import it.matteo.bo.model.primary.PromotionsOrdersBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PromotionsOrdersMapper extends
        GenericBoundaryMapper<PromotionsOrdersBoundaryRequest, PromotionsOrdersBoundaryResponse, PromotionsOrdersEntity> {

    PromotionsOrdersMapper INSTANCE = Mappers.getMapper(PromotionsOrdersMapper.class);

    @Mapping(target = "coupon.couponCode", source = "couponCode")
    @Mapping(target = "promotion.promotionCode", source = "promotionCode")
    @Mapping(target = "order.orderCode", source = "orderCode")
    @Mapping(target = "customer.taxCode", source = "customerCode")
    @Override
    PromotionsOrdersEntity requestToEntity(PromotionsOrdersBoundaryRequest request);

    @Mapping(target = "couponCode", source = "coupon.couponCode")
    @Mapping(target = "promotionCode", source = "promotion.promotionCode")
    @Mapping(target = "orderCode", source = "order.orderCode")
    @Mapping(target = "customerCode", source = "customer.taxCode")
    @Override
    PromotionsOrdersBoundaryRequest entityToRequest(PromotionsOrdersEntity entity);

    @Mapping(target = "couponCode", source = "coupon.couponCode")
    @Mapping(target = "promotionCode", source = "promotion.promotionCode")
    @Mapping(target = "orderCode", source = "order.orderCode")
    @Mapping(target = "customerCode", source = "customer.taxCode")
    @Override
    PromotionsOrdersBoundaryResponse entityToResponse(PromotionsOrdersEntity entity);

}
