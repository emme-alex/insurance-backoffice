package it.matteo.bo.mapper;

import it.matteo.bo.domain.primary.RelatedWarrantyEntity;
import it.matteo.bo.model.primary.RelatedWarrantyBoundaryRequest;
import it.matteo.bo.model.primary.RelatedWarrantyBoundaryResponse;
import it.matteo.bo.model.primary.RelatedWarrantyReferenceBoundaryRequest;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface RelatedWarrantyMapper extends
        GenericBoundaryMapper<RelatedWarrantyBoundaryRequest, RelatedWarrantyBoundaryResponse, RelatedWarrantyEntity> {

    RelatedWarrantyMapper INSTANCE = Mappers.getMapper(RelatedWarrantyMapper.class);

    @Override
    RelatedWarrantyEntity requestToEntity(RelatedWarrantyBoundaryRequest request);

    @Override
    RelatedWarrantyBoundaryRequest entityToRequest(RelatedWarrantyEntity entity);

    @Override
    RelatedWarrantyBoundaryResponse entityToResponse(RelatedWarrantyEntity entity);

    default List<RelatedWarrantyEntity> requestToEntities(final List<RelatedWarrantyReferenceBoundaryRequest> request) {
        if (request == null) {
            return null;
        }
        final List<RelatedWarrantyEntity> relatedWarranties = new ArrayList<>();
        request.forEach(relatedWarrantyReferenceBoundaryRequest -> {
            final RelatedWarrantyEntity relatedWarranty = new RelatedWarrantyEntity();
            relatedWarranty.setId(relatedWarrantyReferenceBoundaryRequest.getId());
            relatedWarranty.setExternalCode(relatedWarrantyReferenceBoundaryRequest.getExternalCode());
            relatedWarranties.add(relatedWarranty);
        });
        return relatedWarranties;
    }

}
