package it.matteo.bo.mapper;

import it.matteo.bo.domain.primary.SurveyQuestionEntity;
import it.matteo.bo.model.primary.SurveyQuestionBoundaryRequest;
import it.matteo.bo.model.primary.SurveyQuestionBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SurveyQuestionMapper extends
        GenericBoundaryMapper<SurveyQuestionBoundaryRequest, SurveyQuestionBoundaryResponse, SurveyQuestionEntity> {

    SurveyQuestionMapper INSTANCE = Mappers.getMapper(SurveyQuestionMapper.class);

    @Mapping(target = "product.id", source = "productId")
    SurveyQuestionEntity requestToEntity(SurveyQuestionBoundaryRequest request);

}
