package it.matteo.bo.mapper;

import it.matteo.bo.domain.primary.UtmSourceLocationEntity;
import it.matteo.bo.model.primary.UtmSourceLocationBoundaryRequest;
import it.matteo.bo.model.primary.UtmSourceLocationBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UtmSourceLocationMapper extends
        GenericBoundaryMapper<UtmSourceLocationBoundaryRequest, UtmSourceLocationBoundaryResponse, UtmSourceLocationEntity> {

    UtmSourceLocationMapper INSTANCE = Mappers.getMapper(UtmSourceLocationMapper.class);

    @Mapping(target = "utmSource.id", source = "utmSourceId")
    @Override
    UtmSourceLocationEntity requestToEntity(UtmSourceLocationBoundaryRequest request);

    @Mapping(target = "utmSourceId", source = "utmSource.id")
    @Override
    UtmSourceLocationBoundaryRequest entityToRequest(UtmSourceLocationEntity request);

}
