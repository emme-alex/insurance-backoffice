package it.matteo.bo.mapper;

import it.matteo.bo.domain.primary.UtmSourceEntity;
import it.matteo.bo.domain.primary.UtmSourceLocationEntity;
import it.matteo.bo.model.primary.UtmSourceBoundaryRequest;
import it.matteo.bo.model.primary.UtmSourceBoundaryResponse;
import it.matteo.bo.model.primary.UtmSourceLocationReferenceBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UtmSourceMapper extends
        GenericBoundaryMapper<UtmSourceBoundaryRequest, UtmSourceBoundaryResponse, UtmSourceEntity> {

    UtmSourceMapper INSTANCE = Mappers.getMapper(UtmSourceMapper.class);

    UtmSourceLocationEntity requestToEntity(UtmSourceLocationReferenceBoundaryResponse request);

    UtmSourceLocationReferenceBoundaryResponse entityToRequest(UtmSourceLocationEntity request);

}
