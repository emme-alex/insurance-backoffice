package it.matteo.bo.mapper;

import it.matteo.bo.domain.primary.BranchEntity;
import it.matteo.bo.domain.primary.WarrantyEntity;
import it.matteo.bo.model.primary.BranchReferenceBoundaryRequest;
import it.matteo.bo.model.primary.WarrantyBoundaryRequest;
import it.matteo.bo.model.primary.WarrantyBoundaryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface WarrantyMapper extends
        GenericBoundaryMapper<WarrantyBoundaryRequest, WarrantyBoundaryResponse, WarrantyEntity> {

    WarrantyMapper INSTANCE = Mappers.getMapper(WarrantyMapper.class);

    @Mapping(target = "branch.id", source = "branchId")
    @Override
    WarrantyEntity requestToEntity(WarrantyBoundaryRequest request);

    @Mapping(target = "branchId", source = "branch.id")
    @Override
    WarrantyBoundaryRequest entityToRequest(WarrantyEntity entity);

    @Mapping(target = "branch.id", source = "branch.id")
    @Override
    WarrantyBoundaryResponse entityToResponse(WarrantyEntity entity);

    BranchEntity requestToEntity(BranchReferenceBoundaryRequest request);

    BranchReferenceBoundaryRequest entityToResponse(BranchEntity request);

}
