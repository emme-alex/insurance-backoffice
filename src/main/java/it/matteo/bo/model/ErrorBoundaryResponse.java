package it.matteo.bo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Related Warranty Rules Boundary Exception response")
@Getter
@Setter
public class ErrorBoundaryResponse<Response> {

    @Schema(description = "Error")
    private Response error;

}