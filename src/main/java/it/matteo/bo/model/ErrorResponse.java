package it.matteo.bo.model;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@AllArgsConstructor
@RegisterForReflection
@Schema(description = "Error response")
@Getter
@Setter
public class ErrorResponse {

    @Schema(description = "Error")
    private String error;

    @Schema(description = "Information")
    private String errorInfo;

}
