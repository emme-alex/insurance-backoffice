package it.matteo.bo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import it.matteo.bo.constants.RelatedWarrantyRulesErrorsEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class RelatedWarrantyRulesExceptionTO implements Serializable {

    @Schema
    private RelatedWarrantyRulesErrorsEnum errorCode;

    @Schema
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<RelatedWarrantyRulesExceptionWarrantyTO> warranties;

    @Schema
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<List<RelatedWarrantyRulesExceptionWarrantyTO>> possibleWarranties;

}