package it.matteo.bo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class RelatedWarrantyRulesExceptionWarrantyTO implements Serializable {

    @Schema(description = "Related Warranty Id")
    private Long id;

    @Schema(description = "Anag Warranty Internal Code")
    private String internalCode;

    @Schema(description = "Anag Warranty Name")
    private String name;

    @Schema(description = "Related Warranty Id that is the reason of the error")
    private Long cause;

}