package it.matteo.bo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@Data
@AllArgsConstructor
public class RelatedWarrantyRulesTO implements Serializable {

    @Schema(description = "Related Warranty Id")
    private Long id;

    @Schema(description = "Related Warranty External Code")
    private String externalCode;

    @Schema(description = "Anag Warranty Internal Code")
    private String warrantyInternalCode;

    @Schema(description = "Anag Warranty Name")
    private String warrantyName;

    @Schema(description = "Related Warranty Packet")
    private Long packetId;

    @Builder.Default
    @Schema(description = "Dependencies")
    private List<Long> dependencies = new ArrayList<>();

    @Builder.Default
    @Schema(description = "Possible Dependencies")
    private List<List<Long>> possibleDependencies = new ArrayList<>();

    @Builder.Default
    @Schema(description = "Conflicts")
    private List<Long> conflicts = new ArrayList<>();

    @Schema(description = "Has conflicts or dependencies")
    private boolean hasConstraints;

}
