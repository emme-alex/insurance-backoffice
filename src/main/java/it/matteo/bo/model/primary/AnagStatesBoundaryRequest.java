package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "Anag States data")
@Getter
@Setter
public class AnagStatesBoundaryRequest {

    @Schema(description = "State")
    private String state;

    @Schema(description = "Created By")
    private String createdBy;

    @Schema(description = "Updated by")
    private String updatedBy;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

}
