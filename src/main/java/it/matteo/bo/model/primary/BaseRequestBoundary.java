package it.matteo.bo.model.primary;

import jakarta.validation.Valid;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
@Schema(description = "Base request")
public class BaseRequestBoundary<Request> {

    @Valid
    @Schema(description = "Data")
    private Request data;

}
