package it.matteo.bo.model.primary;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Base response")
@Getter
@Setter
public class BaseResponseBoundary<Response> {

    @Schema(description = "Data")
    private Response data;

}
