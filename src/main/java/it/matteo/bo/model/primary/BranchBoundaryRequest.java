package it.matteo.bo.model.primary;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
@Schema(description = "Branch data")
public class BranchBoundaryRequest {

    @NotBlank
    @Schema(description = "Name")
    private String name;

    @Schema(description = "External code")
    private String externalCode;

    @Schema(description = "Section")
    private String section;

}
