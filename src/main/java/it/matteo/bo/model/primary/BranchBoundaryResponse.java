package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Branch entity")
@Getter
@Setter
public class BranchBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Name")
    private String name;

    @Schema(description = "External code")
    private String externalCode;

    @Schema(description = "Section")
    private String section;

}
