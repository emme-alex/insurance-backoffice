package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.ArrayList;
import java.util.List;

@Schema(description = "Category data")
@Getter
@Setter
public class CategoryBoundaryRequest {

    @NotBlank
    @Schema(description = "Name")
    private String name;

    @NotBlank
    @Schema(description = "External code")
    private String externalCode;

    @NotNull
    @Schema(description = "Asset", example = "{}")
    private JsonNode asset;

    @Schema(description = "Products")
    private List<ProductReferenceBoundaryRequest> products = new ArrayList<>();

}
