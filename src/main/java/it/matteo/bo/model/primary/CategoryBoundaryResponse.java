package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.List;

@Schema(description = "Category entity")
@Getter
@Setter
public class CategoryBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Name")
    private String name;

    @Schema(description = "External code")
    private String externalCode;

    @Schema(description = "Asset", example = "{}")
    private JsonNode asset;

    @Schema(description = "Products", example = "{}")
    private List<ProductReferenceBoundaryResponse> products;

}
