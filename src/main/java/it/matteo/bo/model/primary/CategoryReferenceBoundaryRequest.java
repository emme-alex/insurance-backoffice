package it.matteo.bo.model.primary;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Category reference data")
@Getter
@Setter
public class CategoryReferenceBoundaryRequest {

    @NotNull
    @Schema(description = "ID")
    private Long id;

}
