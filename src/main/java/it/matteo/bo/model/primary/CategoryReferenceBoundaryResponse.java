package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Category reference entity")
@Getter
@Setter
public class CategoryReferenceBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Name")
    private String name;

    @Schema(description = "External code")
    private String externalCode;

    @Schema(description = "Asset", example = "{}")
    private JsonNode asset;

}
