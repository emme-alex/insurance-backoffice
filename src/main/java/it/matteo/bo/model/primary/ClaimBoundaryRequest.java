package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Schema(description = "Claim data")
@Getter
@Setter
public class ClaimBoundaryRequest {

    @Schema(description = "Claim Type")
    private String claimType;

    @Schema(description = "Date")
    private LocalDate date;

    @Schema(description = "Message")
    private String message;

    @Schema(description = "Claim Number")
    private String claimNumber;

    @Schema(description = "created_at")
    private LocalDateTime createdAt;

    @Schema(description = "updated_at")
    private LocalDateTime updatedAt;

    @Schema(description = "Notes")
    private String note;

    @Schema(description = "Status")
    private Integer status;

    @Schema(description = "Provider")
    private Integer provider;

    @Schema(description = "Provider extra status")
    private String providerExtraStatus;

    @Schema(description = "Provider extra description")
    private String providerExtraDescription;

    @Schema(description = "Value")
    private Integer value;

    @Schema(description = "data", example = "{}")
    private JsonNode data;

    @Schema(description = "Policy")
    private PolicyBoundaryRequest policy;

}
