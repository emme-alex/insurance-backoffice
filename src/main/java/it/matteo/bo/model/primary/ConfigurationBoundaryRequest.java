package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "Configuration data")
@Getter
@Setter
public class ConfigurationBoundaryRequest {

    @NotBlank
    @Schema(description = "Configuration", example = "{}")
    private JsonNode configuration;

    @NotBlank
    @Schema(description = "Created By")
    private String createdBy;

    @NotBlank
    @Schema(description = "Updated By")
    private String updatedBy;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

}
