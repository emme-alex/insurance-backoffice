package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "Configuration entity")
@Getter
@Setter
public class ConfigurationBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Configuration")
    private JsonNode configuration;

    @Schema(description = "Created By")
    private String createdBy;

    @Schema(description = "Updated By")
    private String updatedBy;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

}
