package it.matteo.bo.model.primary;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "Coupon data")
@Getter
@Setter
public class CouponBoundaryRequest {

    @NotNull
    @Schema(description = "Promotion Id")
    private Long promotionId;

    @Schema(description = "Coupon Code")
    private String couponCode;

    @Schema(description = "Start Date")
    private LocalDateTime startDate;

    @Schema(description = "End Date")
    private LocalDateTime endDate;

    @Schema(description = "Active")
    private boolean active;

    @Schema(description = "Max Usage")
    private int maxUsage;

    @Schema(description = "Type")
    private String type = "single";

}
