package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "Coupon entity")
@Getter
@Setter
public class CouponBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Coupon Code")
    private String couponCode;

    @Schema(description = "Start Date")
    private LocalDateTime startDate;

    @Schema(description = "End Date")
    private LocalDateTime endDate;

    @Schema(description = "Active")
    private boolean active;

    @Schema(description = "Max Usage")
    private int maxUsage;

    @Schema(description = "Type")
    private String type;

    @Schema(description = "Promotion")
    private PromotionBoundaryResponse promotion;

    @Schema(description = "Created By")
    private String createdBy;

    @Schema(description = "Updated by")
    private String updatedBy;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

}
