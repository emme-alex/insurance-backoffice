package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "Customer entity")
@Setter
@Getter
public class CustomerBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Customer code")
    private String customerCode;

    @Schema(description = "User Acceptances")
    private List<UserAcceptancesBoundaryResponse> userAcceptances;

    private JsonNode externalCode;

    private String username;

    private String ndg;

    private String name;

    private String surname;

    private String dateOfBirth;

    private String birthCity;

    private String birthCountry;

    private String birthState;

    private String taxCode;

    private String gender;

    private String street;

    private String streetNumber;

    private String city;

    private String country;

    private Long countryId;

    private String zipCode;

    private String state;

    private Long stateId;

    private String primaryMail;

    private String secondaryMail;

    private String primaryPhone;

    private String secondaryPhone;

    private String language;

    private String legalForm;

    private String education;

    private String salary;

    private String profession;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

}
