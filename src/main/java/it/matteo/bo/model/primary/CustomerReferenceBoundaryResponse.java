package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Customer reference entity")
@Getter
@Setter
public class CustomerReferenceBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "customer_code")
    private String customerCode;

    @Schema(description = "username")
    private String username;

    @Schema(description = "external_code", example = "{}")
    private JsonNode externalCode;

    @Schema(description = "name")
    private String name;

    @Schema(description = "Surname")
    private String surname;

    @Schema(description = "Tax code")
    private String taxCode;

    @Schema(description = "Primary phone")
    private String primaryPhone;

}
