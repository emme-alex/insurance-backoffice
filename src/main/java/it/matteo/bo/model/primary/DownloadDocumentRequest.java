package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Download Document Request Data")

@Getter
@Setter
public class DownloadDocumentRequest {

    @Schema(description = "link")
    private String link;

}

