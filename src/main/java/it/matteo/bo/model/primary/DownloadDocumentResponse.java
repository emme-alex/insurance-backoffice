package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "State entity")
@Getter
@Setter
public class DownloadDocumentResponse {

    @Schema(description = "FILE")
    private String file;

    @Schema(description = "file name")
    private String fileName;

}
