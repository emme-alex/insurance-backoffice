package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Flags entity")
@Getter
@Setter
public class FlagsBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "description")
    private String description;

    @Schema(description = "tag")
    private String tag;

    @Schema(description = "kind")
    private String kind;

    @Schema(description = "Position")
    private Long position;

    @Schema(description = "Mandatory")
    private boolean mandatory;

}
