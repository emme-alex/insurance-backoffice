package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Schema(description = "Gift Card data")
@Getter
@Setter
public class GiftCardBoundaryRequest {

    @Schema(description = "Unique Card Name")
    private String uniqueCardName;

    @Schema(description = "Card Name")
    private String cardName;

    @Schema(description = "Card Value")
    private BigDecimal cardValue;

    @Schema(description = "Start Date")
    private LocalDateTime startDate;

    @Schema(description = "End Date")
    private LocalDateTime endDate;

    @Schema(description = "Utm Source Id")
    private Integer utmSourceId;

}

