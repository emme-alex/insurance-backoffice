package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Schema(description = "Gift Card entity")
@Getter
@Setter
public class GiftCardBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Unique Card Name")
    private String uniqueCardName;

    @Schema(description = "Card Name")
    private String cardName;

    @Schema(description = "Card Value")
    private BigDecimal cardValue;

    @Schema(description = "Start Date")
    private LocalDateTime startDate;

    @Schema(description = "End Date")
    private LocalDateTime endDate;

    @Schema(description = "Utm Source")
    private UtmSourceBoundaryResponse utmSource;

    @Schema(description = "Created By")
    private String createdBy;

    @Schema(description = "Updated By")
    private String updatedBy;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

}

