package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import it.matteo.bo.domain.primary.OrderItemEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Schema(description = "Order data")
@Getter
@Setter
public class OrderBoundaryRequest {

    @Valid
    @NotNull
    @Schema(description = "Anag States ID")
    private Long anagStateId;

    @Schema(description = "Customer ID")
    private Long customerId;

    @Schema(description = "Order code")
    private String orderCode;

    @Schema(description = "Policy code")
    private String policyCode;

    @Schema(description = "Packet ID")
    private Long packetId;

    @Schema(description = "Product ID")
    private Long productId;

    @Schema(description = "Asset of goods", example = "{}")
    private JsonNode asset;

    @Schema(description = "Broker ID")
    private Long brokerId;

    @Schema(description = "Company ID")
    private Long companyId;

    @Schema(description = "Insured item", example = "{}")
    private JsonNode insuredItem;

    @Schema(description = "Insurance premium")
    private double insurancePremium;

    @Schema(description = "Created by")
    private String createdBy;

    @Schema(description = "Updated by")
    private String updatedBy;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

    @Schema(description = "Payment transaction id")
    private Integer paymentTransactionId;

    @Schema(description = "Payment token")
    private String paymentToken;

    @Schema(description = "Product Type")
    private String productType;

    @Schema(description = "Discount")
    private String discount;

    private PacketBoundaryRequest packet;

    private List<OrderItemEntity> orderItem = new ArrayList<>();

}
