package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "Order History data")
@Getter
@Setter
public class OrderHistoryBoundaryRequest {

    @Schema(description = "Created By")
    private String createdBy;

    @Schema(description = "Updated By")
    private String updatedBy;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

    @Schema(name = "Order Code")
    private String orderCode;

    @Schema(name = "Step State")
    private String stepState;

    @Schema(name = "Order State")
    private String orderState;

}
