package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import it.matteo.bo.domain.primary.OrderEntity;
import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "Order item entity")
@Getter
@Setter
public class OrderItemBoundaryResponse {

    @Id
    @Column(columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Schema(description = "Price")
    private String price;

    @Schema(description = "Product id")
    private Long productId;

    @Schema(description = "Policy number")
    private String policyNumber;

    @Schema(description = "Master policy number")
    private String masterPolicyNumber;

    @Schema(description = "External id")
    private String externalId;

    @Schema(description = "State")
    private String state;

    @Schema(description = "Start date")
    private LocalDateTime startDate;

    @Schema(description = "Expiration date")
    private LocalDateTime expirationDate;

    @Schema(description = "Insured Item")
    private JsonNode insuredItem;

    @Schema(description = "Quantity")
    private Integer quantity;

    @Schema(description = "Order")
    private OrderEntity order;

}
