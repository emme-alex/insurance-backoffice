package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Getter
@Setter
public class OrderItemReferenceBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Price")
    private String price;

    @Schema(description = "Product id")
    private Long productId;

    @Schema(description = "Policy number")
    private String policyNumber;

    @Schema(description = "Master policy number")
    private String masterPolicyNumber;

    @Schema(description = "External id")
    private String externalId;

    @Schema(description = "State")
    private String state;

    @Schema(description = "Start date")
    private LocalDateTime startDate;

    @Schema(description = "Expiration date")
    private LocalDateTime expirationDate;

    @Schema(description = "Insured item")
    private JsonNode insuredItem;

    @Schema(description = "Quantity")
    private Integer quantity;

}
