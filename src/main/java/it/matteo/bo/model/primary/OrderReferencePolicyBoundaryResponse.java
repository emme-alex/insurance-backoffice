package it.matteo.bo.model.primary;

import it.matteo.bo.constants.StepStateEnum;
import it.matteo.bo.domain.primary.OrderHistoryEntity;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "Order reference entity for policy response")
@Getter
@Setter
public class OrderReferencePolicyBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Order code")
    private String orderCode;

    @Schema(description = "Policy code")
    private String policyCode;

    @Schema(description = "Anag State")
    private AnagStatesBoundaryResponse anagState;

    @Schema(description = "Broker ID")
    private Long brokerId;

    @Schema(description = "Company ID")
    private Long companyId;

    @Schema(description = "Insurance premium")
    private double insurancePremium;

    @Schema(description = "Created by")
    private String createdBy;

    @Schema(description = "Updated by")
    private String updatedBy;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

    @Schema(description = "Packet for order")
    private PacketReferenceBoundaryResponse packet;

    @Schema(description = "Order history")
    private OrderHistoryEntity orderHistory;

    @Schema(description = "Step state")
    private StepStateEnum[] stepState = StepStateEnum.values();

    @Schema(description = "Order item")
    private List<OrderItemReferenceBoundaryResponse> orderItem;

    @Schema(description = "Payment Transaction Id")
    private Integer paymentTransactionId;

    @Schema(description = "Payment token")
    private String paymentToken;

    @Schema(description = "Product Type")
    private String productType;

}
