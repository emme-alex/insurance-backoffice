package it.matteo.bo.model.primary;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "Packet data")
@Getter
@Setter
public class PacketBoundaryRequest {

    @NotBlank
    @Schema(description = "Name")
    private String name;

    @Schema(description = "Broker id")
    private String brokerId;

    @Schema(description = "Product id")
    private String productId;

    @Schema(description = "Warranties")
    private List<RelatedWarrantyBoundaryRequest> warranties;

    @Schema(description = "Description")
    private String description;

    @NotBlank
    @Schema(description = "Sku")
    private String sku;

    @Schema(description = "External code")
    private String externalCode;

    @Schema(description = "Duration")
    private String duration;

    @Schema(description = "Duration type")
    private String duration_type;

    @Schema(description = "Fixed end date")
    private LocalDateTime fixedEndDate;

    @Schema(description = "Fixed start date")
    private LocalDateTime fixedStartDate;

    @Schema(description = "Plan id")
    private String planId;

    @Schema(description = "Plan name")
    private String planName;

}
