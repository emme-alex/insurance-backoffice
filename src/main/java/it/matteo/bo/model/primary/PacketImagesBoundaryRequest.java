package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import it.matteo.bo.domain.primary.PacketEntity;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
public class PacketImagesBoundaryRequest {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Image")
    private JsonNode image;

    @Schema(description = "Packet")
    private PacketEntity packet;

}
