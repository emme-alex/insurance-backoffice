package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
public class PacketImagesBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Image")
    private JsonNode image;

}
