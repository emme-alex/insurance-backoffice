package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "Packet reference entity for Product")
@Getter
@Setter
public class PacketReferenceForProductBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    private String sku;

    @Schema(description = "Name")
    private String name;

    @Schema(description = "External code")
    private String externalCode;

    @Schema(description = "Asset", example = "{}")
    private JsonNode asset;

    @Schema(description = "Broker ID")
    private Long brokerId;

    @Schema(description = "duration")
    private String duration;

    @Schema(description = "Duration type")
    private String duration_type;

    @Schema(description = "Fixed end date")
    private LocalDateTime fixedEndDate;

    @Schema(description = "Fixed start date")
    private LocalDateTime fixedStartDate;

    @Schema(description = "Plan id")
    private String planId;

    @Schema(description = "Plan name")
    private String planName;

    @Schema(description = "warranties")
    private List<RelatedWarrantyReferenceBoundaryResponse> warranties;

}
