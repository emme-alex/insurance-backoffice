package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Payment method data")
@Getter
@Setter
public class PaymentMethodBoundaryRequest {

    @Schema(description = "Payment method")
    private String paymentMethod;

    @Schema(description = "Payment method type")
    private String paymentMethodType;

    @Schema(description = "Type")
    private String type;

    @Schema(description = "Active")
    private Boolean active;

    @Schema(description = "External id")
    private Long externalId;

}
