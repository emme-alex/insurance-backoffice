package it.matteo.bo.model.primary;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
public class PaymentMethodBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Payment method")
    private String paymentMethod;

    @NotBlank
    @Schema(description = "Active")
    private Boolean active;

    @Schema(description = "Payment method type")
    private String paymentMethodType;

    @Schema(description = "Type")
    private String type;

    @Schema(description = "External id")
    private Long externalId;

}
