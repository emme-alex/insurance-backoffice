package it.matteo.bo.model.primary;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Payment policy reference data")
@Getter
@Setter
public class PaymentReferenceBoundaryRequest {

    @NotNull
    @Schema(description = "ID")
    private Long id;

    private String paymentFrequency;

    private String paymentTrx;

    private String paymentToken;

}
