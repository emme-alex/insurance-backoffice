package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.List;

@Schema(description = "Payment policy reference entity")
@Getter
@Setter
public class PaymentReferenceBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    private String paymentFrequency;

    private String paymentTrx;

    private String paymentToken;

    private List<?> paymentSources;

}
