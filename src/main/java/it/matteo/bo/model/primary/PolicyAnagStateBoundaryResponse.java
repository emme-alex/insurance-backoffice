package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "State entity")
@Getter
@Setter
public class PolicyAnagStateBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "State")
    private String state;

}
