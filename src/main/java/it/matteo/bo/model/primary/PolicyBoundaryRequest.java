package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "Policy data")
@Getter
@Setter
public class PolicyBoundaryRequest {

    @NotBlank
    @Schema(description = "Policy code")
    private String policyCode;

    @NotBlank
    @Schema(description = "Order ID")
    private String orderId;

    @Valid
    @NotNull
    @Schema(description = "State ID")
    private Long stateId;

    @Schema(description = "Start date")
    private LocalDateTime startDate;

    @Schema(description = "End date")
    private LocalDateTime endDate;

    @Schema(description = "Product")
    private ProductReferenceBoundaryRequest product;

    @Schema(description = "Payment")
    private PaymentReferenceBoundaryRequest payment;

    @Schema(description = "Customer")
    private CustomerReferenceBoundaryRequest customer;

    @Schema(description = "Choosen properties", example = "{}")
    private JsonNode choosenProperties;

    @Schema(description = "Insurance premium")
    private BigDecimal insurancePremium;

    @Schema(description = "Warranties")
    private List<Long> warrantiesID;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

    @Schema(description = "quantity")
    private Long quantity;

    @Schema(description = "insured_is_contractor")
    private Boolean insuredIsContractor;

    @Schema(description = "certificate_file_name")
    private String certificateFileName;

    @Schema(description = "certificate_link")
    private String certificateLink;

    @Schema(description = "certificate_type")
    private String certificateContentType;

    @Schema(description = "certificate_file_size")
    private String certificateFileSize;

    @Schema(description = "certificate_updated_at")
    private LocalDateTime certificateUpdatedAt;

    @Schema(description = "type")
    private String type;

    @Schema(description = "withdrawal_request_date")
    private LocalDateTime withdrawalRequestDate;

    @Schema(description = "renewed_at")
    private LocalDateTime renewedAt;

    @Schema(description = "marked_as_renewable")
    private Boolean markedAsRenewable;

    @Schema(description = "master_policy_number")
    private String masterPolicyNumber;

    @Schema(description = "payment_frequency")
    private String paymentFrequency;

    @Schema(description = "payment_trx")
    private String paymentTrx;

    @Schema(description = "payment_token")
    private String paymentToken;

}
