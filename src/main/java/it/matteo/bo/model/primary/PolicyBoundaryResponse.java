package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "Policy entity")
@Getter
@Setter
public class PolicyBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Policy code")
    private String policyCode;

    @Schema(description = "External code")
    private String externalCode;

    @Schema(description = "Start date")
    private LocalDateTime startDate;

    @Schema(description = "End date")
    private LocalDateTime endDate;

    @Schema(description = "Order")
    private OrderReferencePolicyBoundaryResponse order;

    @Schema(description = "State")
    private PolicyAnagStateBoundaryResponse state;

    @Schema(description = "Product")
    private ProductReferenceBoundaryResponse product;

    @Schema(description = "Payment")
    private PaymentReferenceBoundaryResponse payment;

    @Schema(description = "Customer")
    private CustomerReferenceBoundaryResponse customer;

    @Schema(description = "Choosen properties", example = "{}")
    private JsonNode choosenProperties;

    @Schema(description = "Insurance premium")
    private BigDecimal insurancePremium;

    @Schema(description = "Warranties")
    private List<PolicyWarrantyBoundaryResponse> warranties;

    @Schema(description = "AdditionalPolicyInfo")
    private JsonNode AdditionalPolicyInfo;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

    @Schema(description = "quantity")
    private Long quantity;

    @Schema(description = "insured_is_contractor")
    private Boolean insuredIsContractor;

    @Schema(description = "certificate_file_name")
    private String certificateFileName;

    @Schema(description = "certificate_link")
    private String certificateLink;

    @Schema(description = "certificate_type")
    private String certificateContentType;

    @Schema(description = "certificate_file_size")
    private String certificateFileSize;

    @Schema(description = "certificate_updated_at")
    private LocalDateTime certificateUpdatedAt;

    @Schema(description = "type")
    private String type;

    @Schema(description = "withdrawal_request_date")
    private LocalDateTime withdrawalRequestDate;

    @Schema(description = "renewed_at")
    private LocalDateTime renewedAt;

    @Schema(description = "marked_as_renewable")
    private Boolean markedAsRenewable;

    @Schema(description = "master_policy_number")
    private String masterPolicyNumber;

}
