package it.matteo.bo.model.primary;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.math.BigDecimal;

@Schema(description = "Policy warranty data")
@Getter
@Setter
public class PolicyWarrantyBoundaryRequest {

    @Valid
    @NotNull
    @Schema(description = "Warranty")
    private WarrantyReferenceBoundaryRequest warranty;

    @Schema(description = "Insurance premium")
    private BigDecimal insurancePremium;

    @Schema(description = "Whether this warranty is mandatory")
    private boolean mandatory;

    @Schema(description = "Ceiling")
    private BigDecimal ceiling;

}
