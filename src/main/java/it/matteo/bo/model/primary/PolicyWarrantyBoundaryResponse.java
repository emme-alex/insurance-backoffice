package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.math.BigDecimal;

@Schema(description = "Policy warranty entity")
@Getter
@Setter
public class PolicyWarrantyBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Warranty")
    private WarrantyReferenceBoundaryResponse warranty;

    @Schema(description = "Insurance premium")
    private BigDecimal insurancePremium;

    @Schema(description = "Whether this warranty is mandatory")
    private boolean mandatory;

    @Schema(description = "Ceiling")
    private BigDecimal ceiling;

}
