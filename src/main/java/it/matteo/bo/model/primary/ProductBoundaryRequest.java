package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.ArrayList;
import java.util.List;

@Schema(description = "Product data")
@Getter
@Setter
public class ProductBoundaryRequest {

    @Schema(description = "Description")
    private String description;

    @Schema(description = "Product description")
    private String productDescription;

    @Schema(description = "Short Description")
    private String shortDescription;

    @Schema(description = "Price")
    private Double price;

    @Schema(description = "Display Price")
    private String displayPrice;

    @Schema(description = "Informative Set")
    private String informativeSet;

    @Schema(description = "UtmSources")
    private List<UtmSourceBoundaryRequest> utmSources = new ArrayList<>();

}
