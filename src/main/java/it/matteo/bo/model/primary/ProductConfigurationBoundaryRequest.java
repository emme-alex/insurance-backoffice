package it.matteo.bo.model.primary;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Configuration data")
@Getter
@Setter
public class ProductConfigurationBoundaryRequest {

    @Schema(description = "Emission")
    private String emission;

    @Schema(description = "Emission prefix")
    private String emissionPrefix;

    @Schema(description = "Certificate")
    private String certificate;

    @NotNull
    @Schema(description = "Whether this configuration can open a claim")
    private Boolean canOpenClaim;

    @Schema(description = "Claim type")
    private String claimType;

    @Schema(description = "Claim provider")
    private String claimProvider;

    @Schema(description = "Withdraw type")
    private String withdrawType;

    @Schema(description = "Deactivate type")
    private String deactivateType;

    @Schema(description = "Product ID")
    private Long productId;

}
