package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Configuration entity")
@Getter
@Setter
public class ProductConfigurationBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Emission")
    private String emission;

    @Schema(description = "Emission prefix")
    private String emissionPrefix;

    @Schema(description = "Certificate")
    private String certificate;

    @Schema(description = "Whether this configuration can open a claim")
    private Boolean canOpenClaim;

    @Schema(description = "Claim type")
    private String claimType;

    @Schema(description = "Claim provider")
    private String claimProvider;

    @Schema(description = "Withdraw type")
    private String withdrawType;

    @Schema(description = "Deactivate type")
    private String deactivateType;

    @Schema(description = "Product", example = "{}")
    private ProductSimpleReferenceBoundaryResponse product;

}
