package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Product images reference data")
@Getter
@Setter
public class ProductImagesBoundaryRequest {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Image", example = "{}")
    private JsonNode images;

}
