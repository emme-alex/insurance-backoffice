package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Product images entity")
@Getter
@Setter
public class ProductImagesBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Images", example = "{}")
    private JsonNode images;

}
