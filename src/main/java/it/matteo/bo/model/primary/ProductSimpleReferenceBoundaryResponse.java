package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDate;

@Schema(description = "Product simple reference entity")
@Getter
@Setter
public class ProductSimpleReferenceBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Code")
    private String code;

    @Schema(description = "Description")
    private String productDescription;

    @Schema(description = "Start date")
    private LocalDate startDate;

    @Schema(description = "End date")
    private LocalDate endDate;

    @Schema(description = "Whether this product is recurring")
    private boolean recurring;

    @Schema(description = "duration")
    private Integer duration;

    @Schema(description = "Duration type")
    private String duration_type;

    @Schema(description = "External ID")
    private String externalId;

    @Schema(description = "Insurance premium")
    private String insurancePremium;

    @Schema(description = "Insurance company")
    private String insuranceCompany;

    @Schema(description = "Insurance Company Logo")
    private String insuranceCompanyLogo;

    @Schema(description = "Business")
    private String business;

    @Schema(description = "Title prod")
    private String titleProd;

    @Schema(description = "Short description")
    private String shortDescription;

    @Schema(description = "Description")
    private String description;

    @Schema(description = "Conditions")
    private String conditions;

    @Schema(description = "Information package")
    private String informationPackage;

    @Schema(description = "Conditions package")
    private String conditionsPackage;

    @Schema(description = "Display price")
    private String displayPrice;

    @Schema(description = "Price")
    private Integer price;

    @Schema(description = "Only contractor")
    private boolean onlyContractor;

    @Schema(description = "Maximum insurable")
    private Integer maximumInsurable;

    @Schema(description = "Can open claim")
    private boolean canOpenClaim;

    @Schema(description = "Holder maximum age")
    private Integer holderMaximumAge;

    @Schema(description = "Holder minimum age")
    private Integer holderMinimumAge;

    @Schema(description = "Show in dashboard")
    private boolean showInDashboard;

    @Schema(description = "Catalog Id")
    private Integer catalogId;

    @Schema(description = "Properties", example = "{}")
    private JsonNode properties;

    @Schema(description = "Quotator Type")
    private String quotatorType;

    @Schema(description = "Show addons in shop")
    private boolean showAddonsInShoppingCart;

    @Schema(description = "Thumbnail")
    private boolean thumbnail;

    @Schema(description = "Privacy documentation link")
    private String privacyDocumentationLink;

    @Schema(description = "Informative set")
    private String informativeSet;

    @Schema(description = "Attachment 3 4")
    private String attachment34;

    @Schema(description = "extras", example = "{}")
    private JsonNode extras;

    @Schema(description = "Plan id")
    private String planId;

    @Schema(description = "Plan name")
    private String planName;

    @Schema(description = "Product type")
    private String productType;

    @Schema(description = "Legacy", example = "{}")
    private JsonNode legacy;

}
