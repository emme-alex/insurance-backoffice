package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
public class ProductsPaymentMethodBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Payment Method")
    private PaymentMethodBoundaryResponse paymentMethod;

}
