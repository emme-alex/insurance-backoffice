package it.matteo.bo.model.primary;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "Promotion Batch data")
@Getter
@Setter
public class PromotionBatchBoundaryRequest {

    @NotNull
    @Schema(description = "Promotion Id")
    private Long promotionId;

    @Schema(description = "Base code")
    private String baseCode;

    @Schema(description = "Description")
    private String description;

    @NotNull
    @Schema(description = "Number of codes")
    private int numberOfCodes;

    @Schema(description = "Start Date")
    private LocalDateTime startDate;

    @Schema(description = "End Date")
    private LocalDateTime endDate;

}
