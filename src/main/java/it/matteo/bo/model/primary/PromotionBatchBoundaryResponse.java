package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "Promotion Batch entity")
@Getter
@Setter
public class PromotionBatchBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Base code")
    private String baseCode;

    @Schema(description = "Description")
    private String description;

    @Schema(description = "Number of codes")
    private int numberOfCodes;

    @Schema(description = "Promotion")
    private PromotionBoundaryResponse promotion;

    @Schema(description = "Generated coupons")
    private List<CouponReferenceBoundaryResponse> coupons;

    @Schema(description = "Created By")
    private String createdBy;

    @Schema(description = "Updated by")
    private String updatedBy;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

}
