package it.matteo.bo.model.primary;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "Promotion data")
@Getter
@Setter
public class PromotionBoundaryRequest {

    @Schema(description = "Rule Id")
    private Long ruleId;

    @NotBlank
    @Schema(description = "Promotion Code")
    private String promotionCode;

    @Schema(description = "Description")
    private String description;

    @Schema(description = "Start Date")
    private LocalDateTime startDate;

    @Schema(description = "End Date")
    private LocalDateTime endDate;

    @Schema(description = "Whether is active or not")
    private boolean active;

    @Schema(description = "Code")
    private String code;

}
