package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "Promotion entity")
@Getter
@Setter
public class PromotionBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Rule")
    private PromotionRuleBoundaryResponse rule;

    @Schema(description = "Promotion Code")
    private String promotionCode;

    @Schema(description = "Description")
    private String description;

    @Schema(description = "Start Date")
    private LocalDateTime startDate;

    @Schema(description = "End Date")
    private LocalDateTime endDate;

    @Schema(description = "Whether is active or not")
    private boolean active;

    @Schema(description = "Code")
    private String code;

    @Schema(description = "Created By")
    private String createdBy;

    @Schema(description = "Updated by")
    private String updatedBy;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

}
