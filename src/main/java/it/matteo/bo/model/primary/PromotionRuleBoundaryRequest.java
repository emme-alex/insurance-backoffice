package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Promotion rules data")
@Getter
@Setter
public class PromotionRuleBoundaryRequest {

    @NotBlank
    @Schema(description = "Name")
    private String name;

    @NotNull
    @Schema(description = "Rule", example = "{}")
    private JsonNode rule;

}
