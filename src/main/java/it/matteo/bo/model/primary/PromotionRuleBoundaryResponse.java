package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "Promotion rules entity")
@Getter
@Setter
public class PromotionRuleBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Name")
    private String name;

    @Schema(description = "Rule", example = "{}")
    private JsonNode rule;

    @Schema(description = "Created By")
    private String createdBy;

    @Schema(description = "Updated by")
    private String updatedBy;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

}
