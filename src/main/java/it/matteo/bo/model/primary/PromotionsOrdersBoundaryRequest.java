package it.matteo.bo.model.primary;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "Promotions orders data")
@Getter
@Setter
public class PromotionsOrdersBoundaryRequest {

    @Valid
    @NotNull
    @Schema(description = "Promotion code")
    private String promotionCode;

    @Valid
    @NotNull
    @Schema(description = "Order code")
    private String orderCode;

    @Valid
    @Schema(description = "Coupon code")
    private String couponCode;

    @Valid
    @Schema(description = "Customer code")
    private String customerCode;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

}
