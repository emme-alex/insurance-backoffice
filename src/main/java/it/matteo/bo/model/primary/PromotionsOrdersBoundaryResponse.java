package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "Promotions orders entity")
@Getter
@Setter
public class PromotionsOrdersBoundaryResponse {

    @Schema(description = "Promotion code")
    private String promotionCode;

    @Schema(description = "Order code")
    private String orderCode;

    @Schema(description = "Coupon code")
    private String couponCode;

    @Schema(description = "Customer code")
    private String customerCode;

    @Schema(description = "Created By")
    private String createdBy;

    @Schema(description = "Updated by")
    private String updatedBy;

    @Schema(description = "Created at")
    private LocalDateTime createdAt;

    @Schema(description = "Updated at")
    private LocalDateTime updatedAt;

}
