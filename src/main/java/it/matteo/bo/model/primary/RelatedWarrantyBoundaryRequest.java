package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.math.BigDecimal;
import java.time.LocalDate;

@Schema(description = "Related warranty data")
@Getter
@Setter
public class RelatedWarrantyBoundaryRequest {

    @Schema(description = "Warranty")
    private WarrantyReferenceBoundaryRequest warranty;

    @Schema(description = "Parent")
    private RelatedWarrantyReferenceBoundaryRequest parent;

    @Schema(description = "Category")
    private CategoryReferenceBoundaryRequest category;

    @Schema(description = "Whether this warranty is mandatory or not")
    private boolean mandatory;

    @Schema(description = "Insurance premium")
    private BigDecimal insurancePremium;

    @Schema(description = "Start date")
    private LocalDate startDate;

    @Schema(description = "End date")
    private LocalDate endDate;

    @Schema(description = "Whether this warranty is recurring or not")
    private boolean recurring;

    @Schema(description = "Rule")
    private JsonNode rule;

    @Schema(description = "External Code")
    private String externalCode;

    @Schema(description = "Ceiling")
    private JsonNode ceilings;

    @Schema(description = "Taxons")
    private JsonNode taxons;

}
