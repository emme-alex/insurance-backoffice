package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.math.BigDecimal;
import java.time.LocalDate;

@Schema(description = "Related warranty entity")
@Getter
@Setter
public class RelatedWarrantyBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Warranty")
    private WarrantyBoundaryResponse warranty;

    @Schema(description = "Parent")
    private RelatedWarrantyReferenceBoundaryResponse parent;

    @Schema(description = "Category")
    private CategoryReferenceBoundaryResponse category;

    @Schema(description = "Packet")
    private PacketReferenceBoundaryResponse packet;

    @Schema(description = "Whether this warranty is mandatory or not")
    private Boolean mandatory;

    @Schema(description = "Insurance premium")
    private BigDecimal insurancePremium;

    @Schema(description = "Start date")
    private LocalDate startDate;

    @Schema(description = "End date")
    private LocalDate endDate;

    @Schema(description = "Whether this warranty is recurring or not")
    private boolean recurring;

    @Schema(description = "Rule")
    private JsonNode rule;

    @Schema(description = "External Code")
    private String externalCode;

    @Schema(description = "Ceiling")
    private JsonNode ceilings;

    @Schema(description = "Taxons")
    private JsonNode taxons;

}
