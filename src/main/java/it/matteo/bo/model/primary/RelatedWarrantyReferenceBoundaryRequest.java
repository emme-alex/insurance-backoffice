package it.matteo.bo.model.primary;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Related warranty reference data")
@Getter
@Setter
public class RelatedWarrantyReferenceBoundaryRequest {

    @NotNull
    @Schema(description = "ID")
    private Long id;

    @Schema(description = "External Code")
    private String externalCode;

}
