package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.List;

@Schema(description = "Packet RelatedWarranty constraints data")
@Getter
@Setter
public class RelatedWarrantyValidationBoundaryRequest {

    @Schema(description = "Packet")
    private Long packetId;

    @Schema(description = "Optional Id of the just removed relatedWarranty")
    private Long justRemovedId;

    @Schema(description = "RelatedWarranty list")
    private List<RelatedWarrantyReferenceBoundaryRequest> warranties;

}
