package it.matteo.bo.model.primary;

import it.matteo.bo.model.RelatedWarrantyRulesTO;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.ArrayList;
import java.util.List;

@Schema(description = "Packet RelatedWarranty constraints entity")
@Getter
@Setter
public class RelatedWarrantyValidationBoundaryResponse {

    @Schema(description = "Whether that set of related warranties is valid or not based on the rules")
    private boolean valid;

    @Schema(description = "Rules")
    private List<RelatedWarrantyRulesTO> rules = new ArrayList<>();

}
