package it.matteo.bo.model.primary;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
public class SplitBoundaryRequest {

    @Valid
    @NotNull
    @Schema(description = "Product")
    private ProductReferenceBoundaryRequest product;

    @Schema(description = "Name")
    private String name;

    @Schema(description = "Value")
    private String value;

    @Schema(description = "sku")
    private String sku;

}
