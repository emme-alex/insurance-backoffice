package it.matteo.bo.model.primary;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Survey acceptable answer data")
@Getter
@Setter
public class SurveyAnswerBoundaryRequest {

    @NotBlank
    @Schema(description = "Value")
    private String value;

    @Schema(description = "Whether this is the default value")
    private boolean defaultValue;

    @Schema(description = "Position")
    private int position;

    @Schema(description = "External code")
    private String externalCode;

    @Schema(description = "Rule")
    private String rule;

}
