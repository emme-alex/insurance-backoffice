package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
@Schema(description = "Survey acceptable answer entity")
public class SurveyAnswerBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Value")
    private String value;

    @Schema(description = "Whether this is the default value")
    private boolean defaultValue;

    @Schema(description = "Position")
    private int position;

    @Schema(description = "External code")
    private String externalCode;

    @Schema(description = "Rule")
    private String rule;

}
