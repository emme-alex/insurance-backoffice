package it.matteo.bo.model.primary;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.List;

@Schema(description = "Survey question data")
@Getter
@Setter
public class SurveyQuestionBoundaryRequest {

    @NotBlank
    @Schema(description = "Content")
    private String content;

    @Schema(description = "Position")
    private int position;

    @Schema(description = "External code")
    private String externalCode;

    @Valid
    @NotNull
    @Schema(description = "Acceptable answers")
    private List<SurveyAnswerBoundaryRequest> answers;

    @Schema(description = "Product ID")
    private Long productId;

}
