package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.List;

@Schema(description = "Product-Question entity")
@Getter
@Setter
public class SurveyQuestionBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Content")
    private String content;

    @Schema(description = "Position")
    private int position;

    @Schema(description = "External code")
    private String externalCode;

    @Schema(description = "Acceptable answers")
    private List<SurveyAnswerBoundaryResponse> answers;

}
