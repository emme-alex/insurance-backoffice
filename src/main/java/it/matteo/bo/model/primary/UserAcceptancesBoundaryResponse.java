package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "User acceptances entity")
@Getter
@Setter
public class UserAcceptancesBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    private FlagsBoundaryResponse flag;

    private boolean value;

    private LocalDateTime created_at;

    private LocalDateTime updated_at;

    private LocalDateTime deleted_at;

}
