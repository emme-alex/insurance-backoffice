package it.matteo.bo.model.primary;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.List;

@Schema(description = "UtmSource data")
@Getter
@Setter
public class UtmSourceBoundaryRequest {

    @Schema(description = "Code")
    private String code;

    @NotBlank
    @Schema(description = "Name")
    private String name;

    @Schema(description = "Locations")
    private List<UtmSourceLocationReferenceBoundaryRequest> locations;

    @Schema(description = "Products")
    private List<ProductReferenceBoundaryRequest> products;

}
