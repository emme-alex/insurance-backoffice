package it.matteo.bo.model.primary;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "UtmSource Location data")
@Getter
@Setter
public class UtmSourceLocationBoundaryRequest {

    @Schema(description = "Code")
    private String code;

    @NotBlank
    @Schema(description = "Name")
    private String name;

    @Schema(description = "UtmSource ID")
    private Long utmSourceId;

}
