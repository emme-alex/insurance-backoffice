package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "UtmSource Location entity")
@Getter
@Setter
public class UtmSourceLocationBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Code")
    private String code;

    @Schema(description = "Name")
    private String name;

    @Schema(description = "UtmSource")
    private UtmSourceBoundaryResponse utmSource;

}
