package it.matteo.bo.model.primary;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;


@Schema(description = "UtmSource Location reference data")
@Getter
@Setter
public class UtmSourceLocationReferenceBoundaryRequest {

    @NotNull
    @Schema(description = "ID")
    private Long id;

}
