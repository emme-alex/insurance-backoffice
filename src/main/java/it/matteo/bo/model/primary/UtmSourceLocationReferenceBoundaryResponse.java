package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "UtmSource Location reference entity")
@Getter
@Setter
public class UtmSourceLocationReferenceBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Code")
    private String code;

    @Schema(description = "Name")
    private String name;

}
