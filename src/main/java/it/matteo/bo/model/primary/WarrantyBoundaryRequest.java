package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Warranty data")
@Getter
@Setter
public class WarrantyBoundaryRequest {

    @NotBlank
    @Schema(description = "Name")
    private String name;

    @NotNull
    @Schema(description = "Ceilings", example = "{}")
    private JsonNode ceilings;

    @NotNull
    @Schema(description = "Images", example = "{}")
    private JsonNode images;

    @Schema(description = "Internal Code")
    private String internalCode;

    @Schema(description = "Branch Id")
    private String branchId;

}
