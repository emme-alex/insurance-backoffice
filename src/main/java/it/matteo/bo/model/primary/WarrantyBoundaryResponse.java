package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Warranty entity")
@Getter
@Setter
public class WarrantyBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Name")
    private String name;

    @Schema(description = "Ceilings", example = "{}")
    private JsonNode ceilings;

    @Schema(description = "Images", example = "{}")
    private JsonNode images;

    @Schema(description = "Internal Code")
    private String internalCode;

    @Schema(description = "Branch")
    private BranchBoundaryResponse branch;

}
