package it.matteo.bo.model.primary;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Warranty reference data")
@Getter
@Setter
public class WarrantyReferenceBoundaryRequest {

    @NotNull
    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Name")
    private String name;

    @Schema(description = "Ceilings", example = "{}")
    private JsonNode ceilings;

    @Schema(description = "Images", example = "{}")
    private JsonNode images;

    @Schema(description = "Internal Code")
    private String internalCode;

    @Schema(description = "Branch Id")
    private Long branchId;

}
