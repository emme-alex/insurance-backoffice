package it.matteo.bo.model.primary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Warranty reference entity")
@Getter
@Setter
public class WarrantyReferenceBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

}
