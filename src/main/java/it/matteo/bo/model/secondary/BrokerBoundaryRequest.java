package it.matteo.bo.model.secondary;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Broker data")
@Getter
@Setter
public class BrokerBoundaryRequest {

    @NotBlank
    @Schema(description = "Name")
    private String name;

    @NotBlank
    @Schema(description = "Identity code")
    private String identitycode;

    @Schema(description = "Description")
    private String description;

}
