package it.matteo.bo.model.secondary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Broker entity")
@Getter
@Setter
public class BrokerBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Name")
    private String name;

    @Schema(description = "Identity code")
    private String identitycode;

    @Schema(description = "Description")
    private String description;

}
