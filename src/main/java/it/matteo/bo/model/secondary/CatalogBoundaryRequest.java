package it.matteo.bo.model.secondary;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Catalog data")
@Getter
@Setter
public class CatalogBoundaryRequest {

    @NotBlank
    @Schema(name = "product_code", description = "Product code")
    private String productCode;

    @Schema(description = "Type")
    private String type;

    @NotBlank
    @Schema(description = "Insurance company")
    private String company;

    @NotNull
    @Schema(description = "Asset", example = "{}")
    private JsonNode asset;

    @NotNull
    @Schema(description = "Revision")
    private String revision;

    @NotNull
    @Schema(description = "Enabled")
    private Boolean enabled;

}
