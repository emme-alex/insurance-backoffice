package it.matteo.bo.model.secondary;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Category entity")
@Getter
@Setter
public class CatalogBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Type")
    private String type;

    @Schema(name = "product_code", description = "Product code")
    private String productCode;

    @Schema(description = "Insurance company")
    private String company;

    @Schema(description = "Asset", example = "{}")
    private JsonNode asset;

    @Schema(description = "Revision")
    private String revision;

    @Schema(description = "Enabled")
    private Boolean enabled;

}
