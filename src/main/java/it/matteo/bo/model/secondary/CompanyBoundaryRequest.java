package it.matteo.bo.model.secondary;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Company data")
@Getter
@Setter
public class CompanyBoundaryRequest {

    @NotBlank
    @Schema(description = "Name")
    private String name;

    @Schema(description = "description")
    private String description;

}
