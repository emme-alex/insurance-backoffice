package it.matteo.bo.model.secondary;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description = "Company entity")
@Getter
@Setter
public class CompanyBoundaryResponse {

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "Name")
    private String name;

    @Schema(description = "description")
    private String description;

}
