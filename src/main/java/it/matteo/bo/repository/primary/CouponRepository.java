package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.CouponEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class CouponRepository implements PanacheRepository<CouponEntity> {

    public Optional<CouponEntity> findByCode(final String couponCode, final boolean insensitive) {
        return insensitive
                ? find("lower(coupon_code)", couponCode.toLowerCase()).singleResultOptional()
                : find("coupon_code", couponCode).singleResultOptional();
    }

    public List<CouponEntity> findMultiple() {
        return find("type", "multiple").list();
    }

}
