package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.CustomerEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class CustomerRepository implements PanacheRepository<CustomerEntity> {

    public Optional<CustomerEntity> findByTaxCode(final String tax_code) {
        return find("tax_code", tax_code).singleResultOptional();
    }

}
