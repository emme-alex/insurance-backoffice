package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.GiftCardEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class GiftCardRepository implements PanacheRepository<GiftCardEntity> {

    public List<GiftCardEntity> listByUtmSourceId(final int utmSourceId) {
        return list("utmSourceId", utmSourceId);
    }

}

