package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.OrderAnagStatesEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class OrderAnagStatesRepository implements PanacheRepository<OrderAnagStatesEntity> {

    public Optional<OrderAnagStatesEntity> findCancelState() {
        return find("select a from OrderAnagStatesEntity a where a.state like concat('%',?1,'%')",
                "Failed")
                .singleResultOptional();
    }

}
