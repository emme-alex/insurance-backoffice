package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.OrderEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class OrderRepository implements PanacheRepository<OrderEntity> {

    public Optional<OrderEntity> findByCode(final String orderCode) {
        return find("order_code", orderCode).singleResultOptional();
    }

    public List<OrderEntity> findByPolicyCode(final String policyCode) {
        return list("policy_code", policyCode);
    }

    public List<OrderEntity> findByCustomerId(final Long customerId) {
        return list("customer_id", customerId);
    }

}
