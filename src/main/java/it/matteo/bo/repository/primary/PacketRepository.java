package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.PacketEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class PacketRepository implements PanacheRepository<PacketEntity> {

    public List<PacketEntity> findByPacketIds(final List<Long> id) {
        return list("select p from PacketEntity p where p.id in (?1)", id);
    }

    public List<PacketEntity> findByProductId(final Long productId) {
        return list("product_id", productId);
    }

}
