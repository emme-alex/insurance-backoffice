package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.PaymentMethodEntity;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PaymentMethodRepository
        implements PanacheRepository<PaymentMethodEntity> {

}
