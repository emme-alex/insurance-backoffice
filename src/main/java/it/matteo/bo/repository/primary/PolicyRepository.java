package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.PolicyEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class PolicyRepository implements PanacheRepository<PolicyEntity> {

    public List<PolicyEntity> findPoliciesByOrderId(final Long orderId) {
        return list("order_id", orderId);
    }

    public Optional<PolicyEntity> findPolicyByCodeOrMasterNumber(final String code) {
        // only "Active" policies (anag_state_id = 2)
        return find("(policy_code = ?1 OR master_policy_number = ?1) AND anag_state_id = 2", code)
                .singleResultOptional();
    }

    public List<PolicyEntity> findByCustomerId(final Long customerId) {
        return list("customer_id", customerId);
    }

}
