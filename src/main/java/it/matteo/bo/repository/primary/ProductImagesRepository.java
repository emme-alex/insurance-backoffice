package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.ProductImagesEntity;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ProductImagesRepository implements
        PanacheRepository<ProductImagesEntity> {
}
