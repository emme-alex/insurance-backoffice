package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.ProductEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class ProductRepository implements PanacheRepository<ProductEntity> {

    public static final String FIND_ACTIVE_PRODUCTS_QUERY =
            "SELECT p FROM ProductEntity p " +
                    "WHERE current_date > p.startDate " +
                    "AND (p.endDate IS NULL OR current_date < p.endDate) " +
                    "AND p.catalogId IS NOT NULL ";

    public List<ProductEntity> findByProductIDs(final List<Long> ids) {
        return list("SELECT p FROM ProductEntity p WHERE p.id IN (?1)", ids);
    }

}
