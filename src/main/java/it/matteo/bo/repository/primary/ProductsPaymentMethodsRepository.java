package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.ProductsPaymentMethodsEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class ProductsPaymentMethodsRepository implements PanacheRepository<ProductsPaymentMethodsEntity> {

    public List<ProductsPaymentMethodsEntity> findByProductId(final long productId) {
        return list("product_id", productId);
    }

}
