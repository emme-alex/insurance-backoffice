package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.PromotionBatchEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class PromotionBatchRepository implements PanacheRepository<PromotionBatchEntity> {

    public List<PromotionBatchEntity> listByPromotionId(final long promotionId) {
        return list("promotion_id", promotionId);
    }

}
