package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.PromotionEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class PromotionRepository implements PanacheRepository<PromotionEntity> {

    public Optional<PromotionEntity> findByCode(final String promotionCode) {
        return find("promotion_code", promotionCode).singleResultOptional();
    }

}
