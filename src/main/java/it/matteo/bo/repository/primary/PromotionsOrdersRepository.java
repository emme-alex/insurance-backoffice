package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.PromotionsOrdersEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class PromotionsOrdersRepository implements PanacheRepository<PromotionsOrdersEntity> {

    public List<PromotionsOrdersEntity> findByOrderCode(final String orderCode) {
        return find("order_code", orderCode).list();
    }

    public List<PromotionsOrdersEntity> findByPromotionCode(final String promotionCode) {
        return find("promotion_code", promotionCode).list();
    }

    public List<PromotionsOrdersEntity> findByCouponCode(final String couponCode, final boolean insensitive) {
        return insensitive
                ? find("lower(coupon_code)", couponCode.toLowerCase()).list()
                : find("coupon_code", couponCode).list();
    }

    public List<PromotionsOrdersEntity> findByCustomerCode(final String customerCode) {
        return find("customer_code", customerCode).list();
    }

    public long countByCouponCode(final String couponCode, final boolean insensitive) {
        return insensitive
                ? count("lower(coupon_code)", couponCode.toLowerCase())
                : count("coupon_code", couponCode);
    }

}
