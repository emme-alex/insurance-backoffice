package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.RelatedWarrantyEntity;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class RelatedWarrantyRepository implements PanacheRepository<RelatedWarrantyEntity> {

    public RelatedWarrantyEntity safeFindById(final Long id) {
        return findByIdOptional(id).orElse(new RelatedWarrantyEntity());
    }

}
