package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.UtmSourceLocationEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class UtmSourceLocationRepository implements PanacheRepository<UtmSourceLocationEntity> {

    public Optional<UtmSourceLocationEntity> findByCode(final String utmSourceLocationCode) {
        return find("code", utmSourceLocationCode).singleResultOptional();
    }

}
