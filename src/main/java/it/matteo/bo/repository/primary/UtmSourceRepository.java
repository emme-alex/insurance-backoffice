package it.matteo.bo.repository.primary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.primary.UtmSourceEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class UtmSourceRepository implements PanacheRepository<UtmSourceEntity> {

    public Optional<UtmSourceEntity> findByCode(final String utmSourceCode) {
        return find("code", utmSourceCode).singleResultOptional();
    }

}
