package it.matteo.bo.repository.secondary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.secondary.BrokerEntity;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class BrokerRepository implements
        PanacheRepository<BrokerEntity> {
}
