package it.matteo.bo.repository.secondary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.secondary.CatalogEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class CatalogRepository implements PanacheRepository<CatalogEntity> {

    public Optional<CatalogEntity> findByProductCode(final String productCode) {
        return Optional.of(find("product_code", productCode).firstResult());
    }

}
