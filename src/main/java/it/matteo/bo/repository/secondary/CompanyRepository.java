package it.matteo.bo.repository.secondary;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import it.matteo.bo.domain.secondary.CompanyEntity;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CompanyRepository implements
        PanacheRepository<CompanyEntity> {
}
