package it.matteo.bo.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import io.micrometer.common.util.StringUtils;
import it.matteo.bo.utils.UtilsBO;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class BaseResource {

    protected Map<String, Object> getFilters(final String filters) {
        final Map<String, Object> filtersObj = new HashMap<>();
        try {
            if (StringUtils.isNotBlank(filters)) {
                filtersObj.putAll(UtilsBO.OBJECT_MAPPER.readValue(
                        filters,
                        new TypeReference<Map<String, Object>>() {
                        }));
            }
        } catch (final JsonProcessingException ex) {
            log.warn("While processing 'filter' query parameter", ex);
        }
        return filtersObj;
    }

}
