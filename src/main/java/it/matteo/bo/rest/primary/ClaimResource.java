package it.matteo.bo.rest.primary;


import it.matteo.bo.domain.primary.ClaimEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.ClaimMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.ClaimBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.ClaimService;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.RestPath;

import java.util.List;

@Path("/api/v1/claims")
public class ClaimResource extends BaseResource {

    @Inject
    ClaimService service;

    @GET
    @Path("{id}")
    @Operation(summary = "Reads an claim entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<ClaimBoundaryResponse> read(
            @Parameter(description = "Claim ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final ClaimEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity claim by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                ClaimMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Operation(summary = "Lists policy entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<ClaimBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                ClaimMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

}
