package it.matteo.bo.rest.primary;

import com.fasterxml.jackson.databind.JsonNode;
import it.matteo.bo.domain.primary.ConfigurationEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.ConfigurationMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.ConfigurationBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.ConfigurationService;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.RestPath;

import java.util.List;

@Path("/api/v1/configurations")
public class ConfigurationResource extends BaseResource {

    @Inject
    ConfigurationService service;

    @GET
    @Path("{id}")
    @Operation(summary = "Reads a configuration entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<ConfigurationBoundaryResponse> read(
            @Parameter(description = "Configuration ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final ConfigurationEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity configuration by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                ConfigurationMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("confNode/{nodeName}")
    @Operation(summary = "Reads the specific JSON node of a configuration entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<JsonNode> readConfJsonNode(
            @Parameter(description = "Configuration JSON node name") @RestPath("nodeName") final String nodeName,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                service.readConfJsonNode(nodeName)
        );
    }

    @GET
    @Operation(summary = "Lists configuration entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<ConfigurationBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                ConfigurationMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

}

