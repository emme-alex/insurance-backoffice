package it.matteo.bo.rest.primary;


import it.matteo.bo.domain.primary.CouponEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.CouponMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.CouponBoundaryRequest;
import it.matteo.bo.model.primary.CouponBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.CouponService;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse;

import java.util.List;

@Path("/api/v1/coupons")
public class CouponResource extends BaseResource {

    @Inject
    CouponService service;

    @POST
    @Path("/single")
    @ResponseStatus(RestResponse.StatusCode.CREATED)
    @Operation(summary = "Creates a new coupon returning the created entity")
    @APIResponse(responseCode = "201",
            description = "Entity created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ResponseSchema.class)))
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<CouponBoundaryResponse> createSingle(
            @Valid final BaseRequestBoundary<CouponBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final CouponEntity entity = service.createSingle(
                CouponMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                CouponMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("{id}")
    @Operation(summary = "Reads an coupon entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<CouponBoundaryResponse> read(
            @Parameter(description = "Coupon ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final CouponEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity coupon by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                CouponMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byCode/{coupon_code}")
    @Operation(summary = "Reads an coupon entity by coupon code")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<CouponBoundaryResponse> readByCode(
            @Parameter(description = "Coupon code") @RestPath("coupon_code") final String couponCode,
            @Parameter(description = "Case strategy used to search for the coupon_code")
            @QueryParam("insensitive") final boolean insensitive,
            @HeaderParam("Authorization") final String token) {

        final CouponEntity entity = service.readByCode(couponCode, insensitive)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity coupon by code=%s not found", couponCode)));
        return new BaseResponseBoundary<>(
                CouponMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @PUT
    @Path("{id}")
    @Operation(summary = "Updates a coupon returning the resulting entity")
    @APIResponse(responseCode = "200", description = "Entity updated")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<CouponBoundaryResponse> update(
            @Parameter(description = "Coupon ID") @RestPath("id") final long id,
            @Valid final BaseRequestBoundary<CouponBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final CouponEntity entity = service.update(
                id,
                CouponMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                CouponMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/multiple/csv")
    @Operation(summary = "Generate a CSV of coupon entities of type 'multiple'")
    @APIResponse(responseCode = "200", description = "CSV generation of coupons of type 'multiple'")
    @APIResponse(description = "Error",
            content = @Content(mediaType = "text/csv",
                    schema = @Schema(implementation = ErrorResponse.class)))
    public Response exportCsvMultiple(
            @HeaderParam("Authorization") final String token) {

        return Response
                .ok(service.exportCsvMultiple(), MediaType.APPLICATION_OCTET_STREAM)
                .header("content-disposition",
                        "attachment; filename = coupons_multiple.csv").
                build();
    }

    @GET
    @Operation(summary = "Lists coupon entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<CouponBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                CouponMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

    static class ResponseSchema extends BaseResponseBoundary<CouponBoundaryResponse> {

        public ResponseSchema(final CouponBoundaryResponse data) {
            super(data);
        }
    }

}
