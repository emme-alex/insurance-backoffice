package it.matteo.bo.rest.primary;


import it.matteo.bo.domain.primary.CustomerEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.CustomerMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.CustomerBoundaryRequest;
import it.matteo.bo.model.primary.CustomerBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.CustomerService;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.RestPath;

import java.util.List;

@Path("/api/v1/customers")
@Slf4j
public class CustomerResource extends BaseResource {

    @Inject
    CustomerService service;

    @GET
    @Path("{id}")
    @Operation(summary = "Reads an order entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<CustomerBoundaryResponse> read(
            @HeaderParam("Authorization") final String token,
            @Parameter(description = "Customer by id") @RestPath("id") final Long id) {

        final CustomerEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity customer by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                CustomerMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @PUT
    @Path("/{id}")
    @Operation(summary = "Updates a customer returning the resulting entity")
    @APIResponse(responseCode = "200", description = "Entity updated")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<CustomerBoundaryResponse> update(
            @HeaderParam("Authorization") final String token,
            @Parameter(description = "Costumer ID") @RestPath("id") final long id,
            @Valid final BaseRequestBoundary<CustomerBoundaryRequest> req) {

        final CustomerEntity entity = service.update(
                id,
                CustomerMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                CustomerMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Operation(summary = "Lists customer entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<CustomerBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                CustomerMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))
                )
        );
    }

}
