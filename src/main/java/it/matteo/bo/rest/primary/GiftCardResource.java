package it.matteo.bo.rest.primary;


import it.matteo.bo.domain.primary.GiftCardEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.GiftCardMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.GiftCardBoundaryRequest;
import it.matteo.bo.model.primary.GiftCardBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.GiftCardService;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse;

import java.util.List;

@Path("/api/v1/giftcards")
public class GiftCardResource extends BaseResource {

    @Inject
    GiftCardService service;

    @POST
    @ResponseStatus(RestResponse.StatusCode.CREATED)
    @Operation(summary = "Creates a new gift card returning the created entity")
    @APIResponse(responseCode = "201",
            description = "Entity created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ResponseSchema.class)))
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<GiftCardBoundaryResponse> create(
            @Valid final BaseRequestBoundary<GiftCardBoundaryRequest> req) {

        final GiftCardEntity entity = service.create(
                GiftCardMapper.INSTANCE.requestToEntity(req.getData()));

        return new BaseResponseBoundary<>(
                GiftCardMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("{id}")
    @Operation(summary = "Reads a gift card entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<GiftCardBoundaryResponse> read(
            @Parameter(description = "Gift Card ID") @RestPath("id") final long id) {

        final GiftCardEntity entity = service.read(id).orElseThrow(() ->
                new EntityNotFoundException(String.format(
                        "Entity gift card by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                GiftCardMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @PUT
    @Path("{id}")
    @Operation(summary = "Updates a gift card returning the resulting entity")
    @APIResponse(responseCode = "200", description = "Entity updated")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<GiftCardBoundaryResponse> update(
            @Parameter(description = "Gift Card ID") @RestPath("id") final long id,
            @Valid final BaseRequestBoundary<GiftCardBoundaryRequest> req) {

        final GiftCardEntity entity = service.update(
                id,
                GiftCardMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                GiftCardMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Operation(summary = "Lists gift card entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<GiftCardBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                GiftCardMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

    static class ResponseSchema extends BaseResponseBoundary<GiftCardBoundaryResponse> {

        public ResponseSchema(final GiftCardBoundaryResponse data) {
            super(data);
        }
    }

}
