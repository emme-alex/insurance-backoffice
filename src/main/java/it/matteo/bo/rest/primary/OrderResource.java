package it.matteo.bo.rest.primary;


import it.matteo.bo.domain.primary.OrderEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.OrderMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.OrderBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.OrderService;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.RestPath;

import java.util.List;

@Path("/api/v1/orders")
public class OrderResource extends BaseResource {

    @Inject
    OrderService service;

    @GET
    @Path("{id}")
    @Operation(summary = "Reads an order entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<OrderBoundaryResponse> read(
            @Parameter(description = "Order ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final OrderEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity order by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                OrderMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byCode/{order_code}")
    @Operation(summary = "Reads an order entity by order code")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<OrderBoundaryResponse> readByCode(
            @Parameter(description = "Order code") @RestPath("order_code") final String orderCode,
            @HeaderParam("Authorization") final String token) {

        final OrderEntity entity = service.readOrderByCode(orderCode)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity order by code=%s not found", orderCode)));
        return new BaseResponseBoundary<>(
                OrderMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byPolicyId/{policy_id}")
    @Operation(summary = "Reads an order entity by policy id")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<OrderBoundaryResponse>> readByPolicyId(
            @Parameter(description = "Policy id") @RestPath("policy_id") final Long policyId,
            @HeaderParam("Authorization") final String token) {

        final OrderEntity entity = service.readOrderByPolicyId(policyId)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity order by policy id=%d not found", policyId)));
        return new BaseResponseBoundary<>(
                OrderMapper.INSTANCE.entitiesToResponses(
                        // It was requested from front-end to return a list
                        // instead of a single entity (as it should be)
                        List.of(entity)
                )
        );
    }

    @GET
    @Path("/byPolicyCode/{policy_code}")
    @Operation(summary = "Reads an order entity by policy code")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<OrderBoundaryResponse>> readByPolicyCode(
            @Parameter(description = "Policy code") @RestPath("policy_code") final String policyCode,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                OrderMapper.INSTANCE.entitiesToResponses(
                        service.readByPolicyCode(policyCode))
        );
    }

    @GET
    @Path("/byCustomerId/{customer_id}")
    @Operation(summary = "Reads an order entity by customer id")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<OrderBoundaryResponse>> readByCustomerId(
            @Parameter(description = "Customer id") @RestPath("customer_id") final Long customerId,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                OrderMapper.INSTANCE.entitiesToResponses(
                        service.readOrderByCustomerId(customerId)
                )
        );
    }

    @PUT
    @Path("/cancel")
    @Operation(summary = "Updates the status of an order to 'failed'. Returning the resulting entity")
    @APIResponse(responseCode = "200", description = "Entity updated")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<OrderBoundaryResponse> cancel(
            final @HeaderParam("Authorization") String token,
            final @Parameter(description = "Order ID") @QueryParam("order_id") long orderId) {

        final OrderEntity entity = service.cancel(orderId);
        return new BaseResponseBoundary<>(
                OrderMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Operation(summary = "Lists order entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<OrderBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                OrderMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

}
