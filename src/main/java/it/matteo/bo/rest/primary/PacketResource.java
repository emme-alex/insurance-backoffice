package it.matteo.bo.rest.primary;


import it.matteo.bo.domain.primary.PacketEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.PacketMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.PacketBoundaryRequest;
import it.matteo.bo.model.primary.PacketBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.PacketService;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Path("/api/v1/packets")
public class PacketResource extends BaseResource {

    @Inject
    PacketService service;

    @POST
    @ResponseStatus(RestResponse.StatusCode.CREATED)
    @Operation(summary = "Creates a new packet returning the created entity")
    @APIResponse(responseCode = "201", description = "Entity created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ResponseSchema.class)))
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PacketBoundaryResponse> create(
            @Valid final BaseRequestBoundary<PacketBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final PacketEntity entity = service.create(
                PacketMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                PacketMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("{id}")
    @Operation(summary = "Reads a packet entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PacketBoundaryResponse> read(
            @Parameter(description = "Packet ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final PacketEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity packet by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                PacketMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byProductId/{id}")
    @Operation(summary = "Reads a packet entity by product id")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PacketBoundaryResponse>> readByProductId(
            @Parameter(description = "PRODUCT ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final List<PacketEntity> entities = service.readByProductId(id);
        return new BaseResponseBoundary<>(
                PacketMapper.INSTANCE.entitiesToResponses(entities)
        );
    }

    @PUT
    @Path("{id}")
    @Operation(summary = "Updates a packet returning the resulting entity")
    @APIResponse(responseCode = "200", description = "Entity updated")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PacketBoundaryResponse> update(
            @Parameter(description = "Packet ID") @RestPath("id") final long id,
            @Valid final BaseRequestBoundary<PacketBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final PacketEntity entity = service.update(
                id,
                PacketMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                PacketMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byIds/{ids}")
    @Operation(summary = "Reads one or more packet entities by ids")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PacketBoundaryResponse>> readByIds(
            @Parameter(description = "Packet IDs") @PathParam("ids") final String ids,
            @HeaderParam("Authorization") final String token) {

        // MODIFICA NECESSARIA IN QUANTO IL SEPARATORE CON LE VIRGOLE IN URL DA FASTIDIO
        final List<Long> idsLong = Arrays.stream(ids.split("-"))
                .map(Long::valueOf)
                .collect(Collectors.toList());

        final List<PacketEntity> entities = service.readByIDs(idsLong);
        return new BaseResponseBoundary<>(
                PacketMapper.INSTANCE.entitiesToResponses(entities)
        );

    }

    @GET
    @Operation(summary = "Lists packet entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PacketBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                PacketMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

    static class ResponseSchema extends BaseResponseBoundary<PacketBoundaryResponse> {

        public ResponseSchema(final PacketBoundaryResponse data) {
            super(data);
        }
    }

}
