package it.matteo.bo.rest.primary;


import it.matteo.bo.domain.primary.PaymentMethodEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.PaymentMethodMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.PaymentMethodBoundaryRequest;
import it.matteo.bo.model.primary.PaymentMethodBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.PaymentMethodService;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse;

import java.util.List;

@Path("/api/v1/payment_methods")
public class PaymentMethodResource extends BaseResource {

    @Inject
    PaymentMethodService service;

    @POST
    @ResponseStatus(RestResponse.StatusCode.CREATED)
    @Operation(summary = "Creates a new payment method returning the created entity")
    @APIResponse(responseCode = "201",
            description = "Entity created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ResponseSchema.class)))
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PaymentMethodBoundaryResponse> create(
            @Valid final BaseRequestBoundary<PaymentMethodBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final PaymentMethodEntity entity = service.create(
                PaymentMethodMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                PaymentMethodMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("{id}")
    @Operation(summary = "Reads a payment method entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PaymentMethodBoundaryResponse> read(
            @Parameter(description = "Payment method ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final PaymentMethodEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity payment method by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                PaymentMethodMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byProductId/{product_id}")
    @Operation(summary = "Reads a payment method entities by product id")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PaymentMethodBoundaryResponse>> readByProductId(
            @Parameter(description = "Product ID") @RestPath("product_id") final long productId,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                PaymentMethodMapper.INSTANCE.entitiesToResponses(
                        service.readByProductId(productId)
                )
        );
    }

    @PUT
    @Path("{id}")
    @Operation(summary = "Updates a payment method returning the resulting entity")
    @APIResponse(responseCode = "200", description = "Entity updated")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PaymentMethodBoundaryResponse> update(
            @Parameter(description = "Payment method ID") @RestPath("id") final long id,
            @Valid final BaseRequestBoundary<PaymentMethodBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final PaymentMethodEntity entity = service.update(
                id,
                PaymentMethodMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                PaymentMethodMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Operation(summary = "Lists payment method entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PaymentMethodBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                PaymentMethodMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

    static class ResponseSchema extends BaseResponseBoundary<PaymentMethodBoundaryResponse> {

        public ResponseSchema(final PaymentMethodBoundaryResponse data) {
            super(data);
        }
    }

}

