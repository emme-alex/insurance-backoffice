package it.matteo.bo.rest.primary;


import io.quarkus.security.Authenticated;
import it.matteo.bo.domain.primary.PolicyEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.PolicyMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.DownloadDocumentResponse;
import it.matteo.bo.model.primary.PolicyBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.PolicyService;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.RestPath;

import java.util.List;

@Path("/api/v1/policies")
public class PolicyResource extends BaseResource {

    @Inject
    PolicyService service;

    @GET
    @Path("{id}")
    @Operation(summary = "Reads an policy entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PolicyBoundaryResponse> read(
            @Parameter(description = "Policy ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final PolicyEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity policy by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                PolicyMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byOrderId/{order_id}")
    @Operation(summary = "Reads policy entities by order id")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PolicyBoundaryResponse>> readPoliciesById(
            @Parameter(description = "Order id") @RestPath("order_id") final Long orderId,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                PolicyMapper.INSTANCE.entitiesToResponses(
                        service.readPoliciesByOrderId(orderId)
                )
        );
    }

    @GET
    @Path("/byPolicyCode/{policy_code}")
    @Operation(summary = "Reads policy entities by policy code or master policy number")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PolicyBoundaryResponse> readPolicyByCode(
            @Parameter(description = "Policy code") @RestPath("policy_code") final String code,
            @HeaderParam("Authorization") final String token) {

        final PolicyEntity entity = service.readPolicyByCodeOrMasterNumber(code)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity policy by code=%s not found", code)));
        return new BaseResponseBoundary<>(
                PolicyMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byCustomerId/{customer_id}")
    @Operation(summary = "Reads policy entities by customer id")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PolicyBoundaryResponse>> readByCustomerId(
            @Parameter(description = "Customer id") @RestPath("customer_id") final Long customerId,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                PolicyMapper.INSTANCE.entitiesToResponses(
                        service.readPoliciesByCustomerId(customerId)
                )
        );
    }

    @GET
    @Path("/downloadCertificate/{code}")
    @Operation(summary = "Reads a customerID entity")
    @Authenticated
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<DownloadDocumentResponse> downloadDoc(
            @HeaderParam("Authorization") final String token,
            @Parameter(description = "Customer ID") @RestPath("code") final String code) {

        try {
            return new BaseResponseBoundary<>(
                    service.downloadDocument(code)
            );
        } catch (final Exception ignored) {
            return new BaseResponseBoundary<>();
        }
    }

    @GET
    @Operation(summary = "Lists policy entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PolicyBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                PolicyMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

}
