package it.matteo.bo.rest.primary;


import it.matteo.bo.domain.primary.ProductConfigurationEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.ProductConfigurationMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.ProductConfigurationBoundaryRequest;
import it.matteo.bo.model.primary.ProductConfigurationBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.ProductConfigurationService;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse;

import java.util.List;

@Path("/api/v1/productConfigurations")
public class ProductConfigurationResource extends BaseResource {

    @Inject
    ProductConfigurationService service;

    @POST
    @ResponseStatus(RestResponse.StatusCode.CREATED)
    @Operation(summary = "Creates a new product configuration returning the created entity")
    @APIResponse(responseCode = "201",
            description = "Entity created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ResponseSchema.class)))
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<ProductConfigurationBoundaryResponse> create(
            @Valid final BaseRequestBoundary<ProductConfigurationBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final ProductConfigurationEntity entity = service.create(
                ProductConfigurationMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                ProductConfigurationMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("{id}")
    @Operation(summary = "Reads a product configuration entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<ProductConfigurationBoundaryResponse> read(
            @Parameter(description = "Product configuration ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final ProductConfigurationEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity product configuration by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                ProductConfigurationMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @PUT
    @Path("{id}")
    @Operation(summary = "Updates a product configuration returning the resulting entity")
    @APIResponse(responseCode = "200", description = "Entity updated")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<ProductConfigurationBoundaryResponse> update(
            @Parameter(description = "Product configuration ID") @RestPath("id") final long id,
            @Valid final BaseRequestBoundary<ProductConfigurationBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final ProductConfigurationEntity entity = service.update(
                id,
                ProductConfigurationMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                ProductConfigurationMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Operation(summary = "Lists product configuration entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<ProductConfigurationBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                ProductConfigurationMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

    static class ResponseSchema extends BaseResponseBoundary<ProductConfigurationBoundaryResponse> {

        public ResponseSchema(final ProductConfigurationBoundaryResponse data) {
            super(data);
        }
    }

}
