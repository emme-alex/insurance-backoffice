package it.matteo.bo.rest.primary;


import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.ProductMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.ProductBoundaryRequest;
import it.matteo.bo.model.primary.ProductBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.ProductService;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.RestPath;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Path("/api/v1/products")
public class ProductResource extends BaseResource {

    @Inject
    ProductService service;

    @GET
    @Path("{id}")
    @Operation(summary = "Reads a product entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<ProductBoundaryResponse> read(
            @Parameter(description = "Product ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final ProductEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity product by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                ProductMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byCode/{product_code}")
    @Operation(summary = "Reads a product entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<ProductBoundaryResponse> readByProductCode(
            @Parameter(description = "Product Code") @RestPath("product_code") final String productCode,
            @HeaderParam("Authorization") final String token) {

        final ProductEntity entity = service.readByCode(productCode)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity product by code=%s not found", productCode)));
        return new BaseResponseBoundary<>(
                ProductMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @PUT
    @Path("/{id}")
    @Operation(summary = "Updates a product returning the resulting entity")
    @APIResponse(responseCode = "200", description = "Entity updated")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<ProductBoundaryResponse> update(
            @Parameter(description = "Product ID")
            @RestPath("id") final long id,
            @Valid final BaseRequestBoundary<ProductBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final ProductEntity entity = service.update(
                id,
                ProductMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                ProductMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byIds/{ids}")
    @Operation(summary = "Reads one or more product entities by ids")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<ProductBoundaryResponse>> readByids(
            @Parameter(description = "Products codes") @RestPath("ids") final String ids,
            @HeaderParam("Authorization") final String token) {

        // MODIFICA NECESSARIA IN QUANTO IL SEPARATORE CON LE VIRGOLE IN URL DA FASTIDIO
        final List<Long> idsLong = Arrays.stream(ids.split("-"))
                .map(Long::valueOf)
                .collect(Collectors.toList());

        final List<ProductEntity> entity = service.readProductsByIDs(idsLong);
        return new BaseResponseBoundary<>(
                ProductMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Operation(summary = "Lists product entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<ProductBoundaryResponse>> listActive(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                ProductMapper.INSTANCE.entitiesToResponses(
                        service.listActiveProducts(perPage, page, order, sort, getFilters(filter))));
    }

    @GET
    @Path("/all")
    @Operation(summary = "Lists product entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<ProductBoundaryResponse>> listAll(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                ProductMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

    @GET
    @Path("/recommended")
    @Operation(summary = "Lists product entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<ProductBoundaryResponse>> findRecommendedProducts(
            @HeaderParam("Authorization") final String token) {

        return service.listRecommended();
    }

}

