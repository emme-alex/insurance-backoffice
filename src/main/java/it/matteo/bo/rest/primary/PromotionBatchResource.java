package it.matteo.bo.rest.primary;


import it.matteo.bo.domain.primary.PromotionBatchEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.PromotionBatchMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.PromotionBatchBoundaryRequest;
import it.matteo.bo.model.primary.PromotionBatchBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.PromotionBatchService;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse;

import java.util.List;

@Path(("/api/v1/promotionBatches"))
public class PromotionBatchResource extends BaseResource {

    @Inject
    PromotionBatchService service;

    @POST
    @ResponseStatus(RestResponse.StatusCode.CREATED)
    @Operation(summary = "Creates a new promotion batch returning the created entity")
    @APIResponse(responseCode = "201",
            description = "Entity created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ResponseSchema.class)))
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PromotionBatchBoundaryResponse> create(
            @Valid final BaseRequestBoundary<PromotionBatchBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final PromotionBatchEntity entity = service.create(
                PromotionBatchMapper.INSTANCE.requestToEntity(req.getData()),
                req.getData().getStartDate(),
                req.getData().getEndDate());
        return new BaseResponseBoundary<>(
                PromotionBatchMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("{id}")
    @Operation(summary = "Reads an promotion batch entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PromotionBatchBoundaryResponse> read(
            @Parameter(description = "Promotion Batch ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final PromotionBatchEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity promotion batch by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                PromotionBatchMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byPromotionId/{promotion_id}")
    @Operation(summary = "Reads a promotion batch entity by promotion id")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PromotionBatchBoundaryResponse>> readByPromotionId(
            @Parameter(description = "promotion_id") @RestPath("promotion_id") final Long promotionId,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                PromotionBatchMapper.INSTANCE.entitiesToResponses(
                        service.readByPromotionId(promotionId))
        );
    }

    @GET
    @Path("/byPromotionCode/{promotion_code}")
    @Operation(summary = "Reads a promotion batch entity by promotion code")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PromotionBatchBoundaryResponse>> readByPromotionCode(
            @Parameter(description = "promotion_code") @RestPath("promotion_code") final String promotionCode,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                PromotionBatchMapper.INSTANCE.entitiesToResponses(
                        service.readByPromotionCode(promotionCode))
        );
    }

    @GET
    @Operation(summary = "Lists promotion batch entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PromotionBatchBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                PromotionBatchMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

    static class ResponseSchema extends BaseResponseBoundary<PromotionBatchBoundaryResponse> {

        public ResponseSchema(final PromotionBatchBoundaryResponse data) {
            super(data);
        }
    }

}
