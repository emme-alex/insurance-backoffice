package it.matteo.bo.rest.primary;

import it.matteo.bo.domain.primary.PromotionEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.PromotionMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.PromotionBoundaryRequest;
import it.matteo.bo.model.primary.PromotionBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.PromotionService;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse;

import java.util.List;

@Path("/api/v1/promotions")
public class PromotionResource extends BaseResource {

    @Inject
    PromotionService service;

    @POST
    @ResponseStatus(RestResponse.StatusCode.CREATED)
    @Operation(summary = "Creates a new promotion returning the created entity")
    @APIResponse(responseCode = "201",
            description = "Entity created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ResponseSchema.class)))
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PromotionBoundaryResponse> create(
            @Valid final BaseRequestBoundary<PromotionBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final PromotionEntity entity = service.create(
                PromotionMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                PromotionMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("{id}")
    @Operation(summary = "Reads an promotion entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PromotionBoundaryResponse> read(
            @Parameter(description = "Promotion ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final PromotionEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity promotion by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                PromotionMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byCode/{promotion_code}")
    @Operation(summary = "Reads an promotion entity by promotion code")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PromotionBoundaryResponse> readByCode(
            @Parameter(description = "Promotion code") @RestPath("promotion_code") final String promotionCode,
            @HeaderParam("Authorization") final String token) {

        final PromotionEntity entity = service.readByCode(promotionCode)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity promotion by code=%s not found", promotionCode)));
        return new BaseResponseBoundary<>(
                PromotionMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @PUT
    @Path("{id}")
    @Operation(summary = "Updates a promotion returning the resulting entity")
    @APIResponse(responseCode = "200", description = "Entity updated")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PromotionBoundaryResponse> update(
            @Parameter(description = "Promotion ID") @RestPath("id") final long id,
            @Valid final BaseRequestBoundary<PromotionBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final PromotionEntity entity = service.update(
                id,
                PromotionMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                PromotionMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Operation(summary = "Lists promotion entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PromotionBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                PromotionMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

    static class ResponseSchema extends BaseResponseBoundary<PromotionBoundaryResponse> {

        public ResponseSchema(final PromotionBoundaryResponse data) {
            super(data);
        }
    }

}
