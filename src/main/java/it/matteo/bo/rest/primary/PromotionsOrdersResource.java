package it.matteo.bo.rest.primary;

import it.matteo.bo.domain.primary.PromotionsOrdersEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.PromotionsOrdersMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.PromotionsOrdersBoundaryRequest;
import it.matteo.bo.model.primary.PromotionsOrdersBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.PromotionsOrdersService;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse;

import java.util.List;

@Path("/api/v1/promotionsOrders")
public class PromotionsOrdersResource extends BaseResource {

    @Inject
    PromotionsOrdersService service;

    @POST
    @ResponseStatus(RestResponse.StatusCode.CREATED)
    @Operation(summary = "Creates a new promotionsOrders returning the created entity")
    @APIResponse(responseCode = "201",
            description = "Entity created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ResponseSchema.class)))
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PromotionsOrdersBoundaryResponse> create(
            @Valid final BaseRequestBoundary<PromotionsOrdersBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final PromotionsOrdersEntity entity = service.create(
                PromotionsOrdersMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                PromotionsOrdersMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("{id}")
    @Operation(summary = "Reads a promotionsOrders entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PromotionsOrdersBoundaryResponse> read(
            @Parameter(description = "PromotionOrder ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final PromotionsOrdersEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity promotion orders by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                PromotionsOrdersMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byOrderCode/{order_code}")
    @Operation(summary = "Reads a promotionsOrders entity by order code")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PromotionsOrdersBoundaryResponse>> readByOrderCode(
            @Parameter(description = "order_code") @RestPath("order_code") final String orderCode,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                PromotionsOrdersMapper.INSTANCE.entitiesToResponses(
                        service.readByOrderCode(orderCode))
        );
    }

    @GET
    @Path("/byPromotionCode/{promotion_code}")
    @Operation(summary = "Reads a promotionsOrders entity by promotion code")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PromotionsOrdersBoundaryResponse>> readByPromotionCode(
            @Parameter(description = "promotion_code") @RestPath("promotion_code") final String promotionCode,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                PromotionsOrdersMapper.INSTANCE.entitiesToResponses(
                        service.readByPromotionCode(promotionCode))
        );
    }

    @GET
    @Path("/byCouponCode/{coupon_code}")
    @Operation(summary = "Reads a promotionsOrders entity by coupon code")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PromotionsOrdersBoundaryResponse>> readByCouponCode(
            @Parameter(description = "coupon_code") @RestPath("coupon_code") final String couponCode,
            @Parameter(description = "Case strategy used to search for the coupon_code")
            @QueryParam("insensitive") final boolean insensitive,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                PromotionsOrdersMapper.INSTANCE.entitiesToResponses(
                        service.readByCouponCode(couponCode, insensitive))
        );
    }

    @GET
    @Path("/byCustomerCode/{customer_code}")
    @Operation(summary = "Reads a promotionsOrders entity by customer code")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PromotionsOrdersBoundaryResponse>> readByCustomerCode(
            @Parameter(description = "customer_code") @RestPath("customer_code") final String customerCode,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                PromotionsOrdersMapper.INSTANCE.entitiesToResponses(
                        service.readByCustomerCode(customerCode))
        );
    }

    @GET
    @Path("/validate/{coupon_code}")
    @Operation(summary = "Checks whether the number of 'promotionOrders' entities having that 'coupon_code' is less than " +
            "or equal to the 'maxUsage' value of the coupon")
    @APIResponse(responseCode = "200", description = "Entity check")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<Boolean> validateByCoupon(
            @Parameter(description = "coupon_code") @RestPath("coupon_code") final String couponCode,
            @Parameter(description = "Case strategy used to search for the coupon_code")
            @QueryParam("insensitive") final boolean insensitive,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                service.validateByCoupon(couponCode, insensitive)
        );
    }

    @PUT
    @Path("{id}")
    @Operation(summary = "Updates a promotionsOrders returning the resulting entity")
    @APIResponse(responseCode = "200", description = "Entity updated")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<PromotionsOrdersBoundaryResponse> update(
            @Parameter(description = "PromotionOrder ID") @RestPath("id") final long id,
            @Valid final BaseRequestBoundary<PromotionsOrdersBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final PromotionsOrdersEntity entity = service.update(
                id,
                PromotionsOrdersMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                PromotionsOrdersMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Operation(summary = "Lists promotionsOrders entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<PromotionsOrdersBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                PromotionsOrdersMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

    static class ResponseSchema extends BaseResponseBoundary<PromotionsOrdersBoundaryResponse> {

        public ResponseSchema(final PromotionsOrdersBoundaryResponse data) {
            super(data);
        }
    }

}
