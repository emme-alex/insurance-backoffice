package it.matteo.bo.rest.primary;


import it.matteo.bo.domain.primary.RelatedWarrantyEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.RelatedWarrantyMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.RelatedWarrantyBoundaryResponse;
import it.matteo.bo.model.primary.RelatedWarrantyValidationBoundaryRequest;
import it.matteo.bo.model.primary.RelatedWarrantyValidationBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.RelatedWarrantyService;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse;

import java.util.List;

@Path("/api/v1/relatedWarranties")
public class RelatedWarrantyResource extends BaseResource {

    @Inject
    RelatedWarrantyService service;

    @GET
    @Path("{id}")
    @Operation(summary = "Reads a related warranty entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<RelatedWarrantyBoundaryResponse> read(
            @Parameter(description = "Related warranty ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final RelatedWarrantyEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity related warranty by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                RelatedWarrantyMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byWarrantyAndPacket/{warranty_id}/{packet_id}")
    @Operation(summary = "Reads a related warranty entity by warranty id and packet id")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<RelatedWarrantyBoundaryResponse>> readByWarrantyAndPacket(
            @Parameter(description = "Warranty ID") @RestPath("warranty_id") final long warrantyId,
            @Parameter(description = "Packet ID") @RestPath("packet_id") final long packetId,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                RelatedWarrantyMapper.INSTANCE.entitiesToResponses(
                        service.findByWarrantyAndPacket(warrantyId, packetId))
        );
    }

    @GET
    @Path("/byPacketId/{packet_id}")
    @Operation(summary = "Reads a related warranty entity by packet id")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<RelatedWarrantyBoundaryResponse>> readByPacket(
            @Parameter(description = "Packet ID") @RestPath("packet_id") final long packetId,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                RelatedWarrantyMapper.INSTANCE.entitiesToResponses(
                        service.findByPacket(packetId))
        );
    }

    @POST
    @Path("/validate")
    @ResponseStatus(RestResponse.StatusCode.OK)
    @Operation(summary = "Get constraint rules of a related warranty entity")
    @APIResponse(responseCode = "200", description = "Entity read",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = WarrantyResource.ResponseSchema.class)))
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<RelatedWarrantyValidationBoundaryResponse> validateRules(
            @Valid final BaseRequestBoundary<RelatedWarrantyValidationBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        return new BaseResponseBoundary<>(
                service.validateRules(req.getData())
        );
    }

    @GET
    @Operation(summary = "Lists related warranty entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<RelatedWarrantyBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                RelatedWarrantyMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

}

