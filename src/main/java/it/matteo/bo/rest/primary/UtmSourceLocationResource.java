package it.matteo.bo.rest.primary;


import io.quarkus.security.Authenticated;
import it.matteo.bo.domain.primary.UtmSourceLocationEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.UtmSourceLocationMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.UtmSourceLocationBoundaryRequest;
import it.matteo.bo.model.primary.UtmSourceLocationBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.primary.UtmSourceLocationService;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse;

import java.util.List;

@Path("/api/v1/utmSourceLocations")
public class UtmSourceLocationResource extends BaseResource {

    @Inject
    UtmSourceLocationService service;

    @POST
    @ResponseStatus(RestResponse.StatusCode.CREATED)
    @Operation(summary = "Creates a new utmSource location returning the created entity")
    @APIResponse(responseCode = "201",
            description = "Entity created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ResponseSchema.class)))
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<UtmSourceLocationBoundaryResponse> create(
            @Valid final BaseRequestBoundary<UtmSourceLocationBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final UtmSourceLocationEntity entity = service.create(
                UtmSourceLocationMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                UtmSourceLocationMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("{id}")
    @Operation(summary = "Reads a utmSource location entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<UtmSourceLocationBoundaryResponse> read(
            @Parameter(description = "UtmSource location ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final UtmSourceLocationEntity entity = service.read(id).orElseThrow(() ->
                new EntityNotFoundException(String.format(
                        "Entity utmSource location by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                UtmSourceLocationMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("/byCode/{code}")
    @Operation(summary = "Reads an utmSource location entity by promotion code")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<UtmSourceLocationBoundaryResponse> readByCode(
            @Parameter(description = "UtmSource location code") @RestPath("code") final String utmSourceLocationCode,
            @HeaderParam("Authorization") final String token) {

        final UtmSourceLocationEntity entity = service.readByCode(utmSourceLocationCode)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity utmSource location by code=%s not found", utmSourceLocationCode)));
        return new BaseResponseBoundary<>(
                UtmSourceLocationMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @PUT
    @Path("{id}")
    @Operation(summary = "Updates a utmSource location returning the resulting entity")
    @APIResponse(responseCode = "200", description = "Entity updated")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<UtmSourceLocationBoundaryResponse> update(
            @Parameter(description = "UtmSource location ID") @RestPath("id") final long id,
            @Valid final BaseRequestBoundary<UtmSourceLocationBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final UtmSourceLocationEntity entity = service.update(
                id,
                UtmSourceLocationMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                UtmSourceLocationMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Authenticated
    @Operation(summary = "Lists utmSource location entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<UtmSourceLocationBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                UtmSourceLocationMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

    static class ResponseSchema extends BaseResponseBoundary<UtmSourceLocationBoundaryResponse> {

        public ResponseSchema(final UtmSourceLocationBoundaryResponse data) {
            super(data);
        }
    }

}
