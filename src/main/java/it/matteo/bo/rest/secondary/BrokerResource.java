package it.matteo.bo.rest.secondary;


import it.matteo.bo.domain.secondary.BrokerEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.BrokerMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.secondary.BrokerBoundaryRequest;
import it.matteo.bo.model.secondary.BrokerBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.secondary.BrokerService;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse;

import java.util.List;

@Path("/api/v1/brokers")
public class BrokerResource extends BaseResource {

    @Inject
    BrokerService service;

    @POST
    @ResponseStatus(RestResponse.StatusCode.CREATED)
    @Operation(summary = "Creates a new broker returning the created entity")
    @APIResponse(responseCode = "201",
            description = "Entity created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ResponseSchema.class)))
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<BrokerBoundaryResponse> create(
            @Valid final BaseRequestBoundary<BrokerBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final BrokerEntity entity = service.create(
                BrokerMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                BrokerMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Path("{id}")
    @Operation(summary = "Reads a broker entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<BrokerBoundaryResponse> read(
            @Parameter(description = "Broker ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final BrokerEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity broker by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                BrokerMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @PUT
    @Path("{id}")
    @Operation(summary = "Updates a broker returning the resulting entity")
    @APIResponse(responseCode = "200", description = "Entity updated")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<BrokerBoundaryResponse> update(
            @Parameter(description = "Broker ID") @RestPath("id") final long id,
            @Valid final BaseRequestBoundary<BrokerBoundaryRequest> req,
            @HeaderParam("Authorization") final String token) {

        final BrokerEntity entity = service.update(
                id,
                BrokerMapper.INSTANCE.requestToEntity(req.getData()));
        return new BaseResponseBoundary<>(
                BrokerMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET
    @Operation(summary = "Lists broker entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<BrokerBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filter") final String filter) {

        return new BaseResponseBoundary<>(
                BrokerMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filter))));
    }

    static class ResponseSchema extends BaseResponseBoundary<BrokerBoundaryResponse> {

        public ResponseSchema(final BrokerBoundaryResponse data) {
            super(data);
        }
    }

}

