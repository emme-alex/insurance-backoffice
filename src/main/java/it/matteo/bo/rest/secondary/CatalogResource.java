package it.matteo.bo.rest.secondary;


import it.matteo.bo.domain.secondary.CatalogEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.CatalogMapper;
import it.matteo.bo.model.ErrorResponse;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.secondary.CatalogBoundaryResponse;
import it.matteo.bo.rest.BaseResource;
import it.matteo.bo.service.secondary.CatalogService;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.RestPath;

import java.util.List;

@Path("/secondary/v1/catalogs")
public class CatalogResource extends BaseResource {

    @Inject
    CatalogService service;

    @GET

    @Path("{id}")
    @Operation(summary = "Reads a catalog entity")
    @APIResponse(responseCode = "200", description = "Entity read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<CatalogBoundaryResponse> read(
            @Parameter(description = "Catalog ID") @RestPath("id") final long id,
            @HeaderParam("Authorization") final String token) {

        final CatalogEntity entity = service.read(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity catalog by id=%d not found", id)));
        return new BaseResponseBoundary<>(
                CatalogMapper.INSTANCE.entityToResponse(entity)
        );
    }

    @GET

    @Operation(summary = "Lists catalog entities")
    @APIResponse(responseCode = "200", description = "Entities read")
    @APIResponse(description = "Error",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = ErrorResponse.class)))
    public BaseResponseBoundary<List<CatalogBoundaryResponse>> list(
            @QueryParam("page") final Integer page,
            @QueryParam("perPage") final Integer perPage,
            @QueryParam("order") final String order,
            @QueryParam("sort") final String sort,
            @QueryParam("filters") final String filters) {

        return new BaseResponseBoundary<>(
                CatalogMapper.INSTANCE.entitiesToResponses(
                        service.list(perPage, page, order, sort, getFilters(filters))));
    }

}
