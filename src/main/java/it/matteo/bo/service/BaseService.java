package it.matteo.bo.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersionDetector;
import com.networknt.schema.ValidationMessage;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Sort;
import it.matteo.bo.domain.primary.CouponEntity;
import it.matteo.bo.domain.primary.PacketEntity;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.domain.primary.PromotionBatchEntity;
import it.matteo.bo.domain.primary.PromotionEntity;
import it.matteo.bo.domain.primary.UtmSourceEntity;
import it.matteo.bo.domain.primary.UtmSourceLocationEntity;
import it.matteo.bo.exception.DataValidationException;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.exception.ValidationStructureJsonException;
import it.matteo.bo.repository.primary.ProductRepository;
import it.matteo.bo.repository.primary.PromotionRepository;
import it.matteo.bo.repository.primary.UtmSourceLocationRepository;
import it.matteo.bo.utils.UtilsBO;
import jakarta.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BaseService {

    @Inject
    protected ProductRepository productRepo;

    @Inject
    protected PromotionRepository promotionRepo;

    @Inject
    protected UtmSourceLocationRepository utmSourceLocationRepo;

    @ConfigProperty(name = "rest.list.default.limit")
    int defaultLimit;

    protected <T> List<T> defaultPaginatedList(
            final PanacheRepository<T> panacheRepository,
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return doDefaultPaginatedList(panacheRepository, null, pageSize, pageIndex, order, sort, filter);
    }

    protected <T> List<T> defaultPaginatedList(
            final PanacheRepository<T> panacheRepository,
            final String query,
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return doDefaultPaginatedList(panacheRepository, query, pageSize, pageIndex, order, sort, filter);
    }

    private <T> List<T> doDefaultPaginatedList(
            final PanacheRepository<T> panacheRepository,
            final String query,
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        final Pair<String, Map<String, Object>> filtering = getFiltering(filter, query);
        final PanacheQuery<T> panacheQuery =
                filter == null
                        ? panacheRepository.findAll(getSorting(order, sort))
                        : panacheRepository.find(filtering.getLeft(), getSorting(order, sort), filtering.getRight());

        return panacheQuery
                .page(pageIndex == null ? 0 : pageIndex, pageSize == null ? defaultLimit : pageSize)
                .list();
    }

    private Sort getSorting(final String order, final String sort) {
        final Sort.Direction direction = StringUtils.isBlank(order)
                ? Sort.Direction.Ascending
                : ("asc".equalsIgnoreCase(order) || "ascending".equalsIgnoreCase(order)
                ? Sort.Direction.Ascending
                : Sort.Direction.Descending);

        return StringUtils.isBlank(sort)
                ? Sort.empty().direction(direction)
                : Sort.by(sort).direction(direction);
    }

    private Pair<String, Map<String, Object>> getFiltering(
            final Map<String, Object> filter,
            final String baseQuery) {

        String finalQuery = null;
        final Map<String, Object> params = new HashMap<>();

        if (filter == null) {
            return Pair.of(finalQuery, params);
        }

        final StringBuilder query = new StringBuilder(
                StringUtils.isBlank(baseQuery) ? "" : baseQuery + " AND ");
        filter.keySet().forEach(key -> {
            if (filter.get(key) == null) {
                query.append(key).append(" IS NULL ");
            } else if (filter.get(key) instanceof final String value) {
                if (UtilsBO.isValidDate(value)) {
                    query.append("to_char(").append(key).append(",'YYYY-MM-DD') LIKE :").append(key);
                } else if (UtilsBO.isNumeric(value)) {
                    query.append(key).append(" LIKE :").append(key);
                } else {
                    // case insensitive
                    query.append("LOWER(").append(key).append(")");
                    query.append(" LIKE ")
                            .append("LOWER(CONCAT('%', :").append(key).append(", '%'))");
                }
            } else if (filter.get(key) instanceof Integer
                    || filter.get(key) instanceof Boolean
                    || filter.get(key) instanceof Long) {

                query.append(key).append("=:").append(key);
            } else {
                query.append(key);
            }
            // e.g. "LOWER(coupon_code) LIKE LOWER(CONCAT('%', :coupon_code, '%')) "
            query.append(" AND ");

            setParams(params, filter, key);
        });
        finalQuery = StringUtils.removeEnd(query.toString(), " AND ");

        return Pair.of(finalQuery, params);
    }

    private void setParams(
            final Map<String, Object> params,
            final Map<String, Object> filter,
            final String key) {

        final Object value = filter.get(key);
        if (value == null) {
            return;
        }
        switch (key.toLowerCase()) {
            case "id", "productid", "product_id", "packetid", "packet_id", "policyid", "policy_id", "anagstatesid",
                    "anagstates_id", "customerid", "customer_id", "brokerid", "broker_id", "couponid", "coupon_id",
                    "promotionid", "promotion_id", "warrantyid", "warranty_id", "orderid", "order_id",
                    "categoryid", "category_id", "promotionruleid", "ruleid", "rule_id" ->
                    params.put(key, Long.valueOf(value.toString()));
            default -> params.put(key,
                    value instanceof Boolean
                            ? value
                            : (
                            "%" + (value instanceof String && UtilsBO.isValidDate((String) value)
                                    ? UtilsBO.parseDateSqlFilter((String) value)
                                    : value) + "%")
            );
        }
    }

    protected ProductEntity readProduct(final long id) {
        final ProductEntity product = productRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity product by id=%d not found", id)));
        fixProduct(product);
        return product;
    }

    protected UtmSourceLocationEntity readUtmSourceLocation(final long id) {
        return utmSourceLocationRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity utmSource location by id=%d not found", id)));
    }

    protected void fixProduct(final ProductEntity product) {
        if (product == null) {
            return;
        }
        // WORKAROUND
        // org.hibernate.LazyInitializationException: failed to lazily
        // initialize a collection of role: it.matteo.entity.ProductEntity.splits,
        // could not initialize proxy - no Session
        if (product.getSplits() != null) {
            product.getSplits().size();
        }
        if (product.getCategories() != null) {
            product.getCategories().size();
        }
        if (product.getProductsPaymentMethods() != null) {
            product.getProductsPaymentMethods().size();
        }
        if (product.getQuestions() != null) {
            product.getQuestions().forEach((question) -> {
                if (question.getAnswers() != null) {
                    question.getAnswers().size();
                }
            });
        }
        if (product.getPackets() != null) {
            product.getPackets().size();
            product.getPackets().forEach(this::fixPacket);
        }
        if (product.getUtmSources() != null) {
            product.getUtmSources().size();
        }
    }

    protected void fixPacket(final PacketEntity packet) {
        if (packet == null) {
            return;
        }
        // WORKAROUND
        // org.hibernate.LazyInitializationException: failed to lazily
        // initialize a collection of role: it.matteo.entity.ProductEntity.splits,
        // could not initialize proxy - no Session
        if (packet.getWarranties() != null) {
            packet.getWarranties().size();
        }
    }

    protected void fixPromotion(final PromotionEntity promotion) {
        if (promotion == null) {
            return;
        }
        // WORKAROUND
        // org.hibernate.LazyInitializationException: failed to lazily
        // initialize a collection of role: it.matteo.entity.ProductEntity.splits,
        // could not initialize proxy - no Session
        if (promotion.getCoupons() != null) {
            promotion.getCoupons().size();
        }
    }

    protected void fixPromotionBatch(final PromotionBatchEntity promotionBatch) {
        if (promotionBatch == null) {
            return;
        }
        // WORKAROUND
        // org.hibernate.LazyInitializationException: failed to lazily
        // initialize a collection of role: it.matteo.entity.ProductEntity.splits,
        // could not initialize proxy - no Session
        if (promotionBatch.getCoupons() != null) {
            promotionBatch.getCoupons().size();
        }
    }

    protected void fixUtmSource(final UtmSourceEntity utmSource) {
        if (utmSource == null) {
            return;
        }
        // WORKAROUND
        // org.hibernate.LazyInitializationException: failed to lazily
        // initialize a collection of role: it.matteo.entity.ProductEntity.splits,
        // could not initialize proxy - no Session
        if (utmSource.getLocations() != null) {
            utmSource.getLocations().size();
        }
        if (utmSource.getProducts() != null) {
            utmSource.getProducts().size();
            utmSource.getProducts().forEach(this::fixProduct);
        }
    }

    protected void updateCoupon(
            final CouponEntity currentCoupon,
            final CouponEntity newCoupon) {

        currentCoupon.setActive(newCoupon.isActive());
        currentCoupon.setStartDate(newCoupon.getStartDate());
        currentCoupon.setEndDate(newCoupon.getEndDate());
        currentCoupon.setMaxUsage(newCoupon.getMaxUsage());
    }

    protected void validatePromotionRule(final JsonNode nodeToValidate)
            throws ValidationStructureJsonException {

        validateNode(nodeToValidate,
                "for Promotion rule",
                "schemas/promotionRule_v1.json");
    }

    protected void validateRelatedWarrantyRule(final JsonNode nodeToValidate)
            throws ValidationStructureJsonException {

        validateNode(
                nodeToValidate,
                "for RelatedWarranty rules",
                "schemas/relatedWarrantyRule_v1.json");
    }

    protected void setPromotion(final CouponEntity currentCoupon, final CouponEntity newCoupon) {
        if (newCoupon.getPromotion() != null && newCoupon.getPromotion().getId() != null) {
            final PromotionEntity promotion = promotionRepo.findByIdOptional(newCoupon.getPromotion().getId())
                    .orElseThrow(() -> new EntityNotFoundException(String.format(
                            "Entity promotion by id=%d not found", newCoupon.getPromotion().getId())));
            currentCoupon.setPromotion(promotion);
            fixPromotion(newCoupon.getPromotion());

            // Check range of dates
            checkDates(newCoupon, currentCoupon.getPromotion());
        }
    }

    private void checkDates(final CouponEntity coupon, final PromotionEntity promotion) {
        if (coupon.getStartDate() == null) {
            return;
        }
        if (coupon.getStartDate().isBefore(promotion.getStartDate())
                || (promotion.getEndDate() != null
                && coupon.getEndDate() != null
                && coupon.getStartDate().isAfter(promotion.getEndDate()))) {

            throw new DataValidationException(
                    "Coupon start date is not in the range of the related parent promotion dates");
        }
        if (coupon.getEndDate() == null) {
            return;
        }
        if (coupon.getEndDate().isAfter(promotion.getEndDate())
                || (promotion.getStartDate() != null
                && coupon.getEndDate().isBefore(promotion.getStartDate()))) {

            throw new DataValidationException(
                    "Coupon end date is not in the range of the related parent promotion dates");
        }
    }

    private void validateNode(final JsonNode nodeToValidate, final String msg, final String schemaPath)
            throws ValidationStructureJsonException {

        final JsonNode jsonSchemaNode;
        try {
            jsonSchemaNode = UtilsBO.OBJECT_MAPPER.readTree(
                    getClass().getClassLoader().getResourceAsStream(schemaPath)
            );
        } catch (final IOException e) {
            throw new ValidationStructureJsonException("While reading JSON schema " + msg + " to validate");
        }

        final JsonSchemaFactory factory = JsonSchemaFactory.getInstance(SpecVersionDetector.detect(jsonSchemaNode));
        final JsonSchema nodeJsonSchema = factory.getSchema(jsonSchemaNode);
        final Set<ValidationMessage> errors = nodeJsonSchema.validate(nodeToValidate);
        if (!errors.isEmpty()) {
            throw new ValidationStructureJsonException(
                    "While validating JSON schema " + msg + ": "
                            + errors.stream().toList().get(0).toString());
        }
    }

}
