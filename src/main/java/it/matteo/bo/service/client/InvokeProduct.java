package it.matteo.bo.service.client;


import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.ProductBoundaryResponse;

import java.util.List;

public interface InvokeProduct {

    BaseResponseBoundary<List<ProductBoundaryResponse>> findRecommendedProducts();

}
