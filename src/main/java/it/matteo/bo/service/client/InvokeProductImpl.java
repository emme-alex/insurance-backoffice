package it.matteo.bo.service.client;

import it.matteo.bo.client.product.ProductRecommendationsClient;
import it.matteo.bo.exception.ExternalServiceException;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.ProductBoundaryResponse;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import java.util.List;

@RequestScoped
public class InvokeProductImpl implements InvokeProduct {

    @Inject
    @RestClient
    ProductRecommendationsClient productRecommendationsClient;

    @Inject
    JsonWebToken jsonWebToken;

    @Override
    public BaseResponseBoundary<List<ProductBoundaryResponse>> findRecommendedProducts() {
        final String jwt = "Bearer " + jsonWebToken.getRawToken();

        final BaseResponseBoundary<List<ProductBoundaryResponse>> response;
        try (final Response res = productRecommendationsClient.list(jwt)) {
            response = res.readEntity(new GenericType<>() {
            });
        } catch (final Exception ex) {
            throw new ExternalServiceException(ex.getMessage());
        }
        return response;
    }

}
