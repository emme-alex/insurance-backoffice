package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.CategoryEntity;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.primary.CategoryRepository;
import it.matteo.bo.service.BaseService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class CategoryService extends BaseService {

    @Inject
    CategoryRepository categoryRepo;

    @Transactional
    public CategoryEntity create(final CategoryEntity newCategory) {
        if (newCategory.getProducts() != null && !newCategory.getProducts().isEmpty()) {
            final Set<ProductEntity> newProducts = newCategory.getProducts()
                    .stream()
                    .map((newProduct) -> {
                        final ProductEntity product = readProduct(newProduct.getId());
                        product.getCategories().add(newCategory);
                        return product;
                    })
                    .collect(Collectors.toSet());

            newCategory.getProducts().clear();
            newCategory.getProducts().addAll(newProducts);
        }

        categoryRepo.persist(newCategory);
        return newCategory;
    }

    public Optional<CategoryEntity> read(final long id) {
        return categoryRepo.findByIdOptional(id);
    }

    @Transactional
    public CategoryEntity update(final long id, final CategoryEntity newCategory) {
        final CategoryEntity currentCategory = categoryRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity category by id=%d not found", id)));

        currentCategory.setName(newCategory.getName());
        currentCategory.setExternalCode(newCategory.getExternalCode());
        currentCategory.setAsset(newCategory.getAsset());

        if (newCategory.getProducts() != null && !newCategory.getProducts().isEmpty()) {
            updateProducts(currentCategory, newCategory);
        } else if (currentCategory.getProducts() != null) {
            currentCategory.getProducts().forEach(this::fixProduct);
        }

        categoryRepo.getEntityManager().merge(currentCategory);
        return currentCategory;
    }

    private void updateProducts(
            final CategoryEntity currentCategory,
            final CategoryEntity newCategory) {

        currentCategory.getProducts().forEach(this::fixProduct);

        // Remove products
        final Set<ProductEntity> removingProducts =
                currentCategory.getProducts().stream()
                        .filter((currentCategoryProduct) ->
                                newCategory.getProducts().stream()
                                        .noneMatch((updaterProduct) ->
                                                Objects.equals(updaterProduct.getId(), currentCategoryProduct.getId())))
                        .collect(Collectors.toSet());

        removingProducts.forEach((removingProduct) ->
                removingProduct.getCategories().removeIf((removingCategory) ->
                        Objects.equals(removingCategory.getId(), currentCategory.getId())));

        currentCategory.getProducts().removeAll(removingProducts);

        // New products
        final Set<ProductEntity> newProducts = newCategory.getProducts()
                .stream()
                .filter((updaterProduct) ->
                        currentCategory.getProducts().stream()
                                .noneMatch((updatingProduct) ->
                                        Objects.equals(updatingProduct.getId(), updaterProduct.getId())))
                .map((updaterProduct) -> {
                    final ProductEntity product = readProduct(updaterProduct.getId());
                    product.getCategories().add(currentCategory);
                    return product;
                })
                .collect(Collectors.toSet());

        currentCategory.getProducts().addAll(newProducts);
    }

    @Transactional
    public void delete(final long id) {
        // remove relation with products, first
        final CategoryEntity currentCategory = categoryRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity category by id=%d not found", id)));

        currentCategory.getProducts().forEach((removingProduct) ->
                removingProduct.getCategories().removeIf((removingCategory) ->
                        Objects.equals(removingCategory.getId(), currentCategory.getId())));
        currentCategory.getProducts().removeAll(currentCategory.getProducts());
        categoryRepo.getEntityManager().merge(currentCategory);

        categoryRepo.deleteById(id);
    }

    public List<CategoryEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                categoryRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
