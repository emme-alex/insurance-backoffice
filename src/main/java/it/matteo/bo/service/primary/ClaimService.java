package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.ClaimEntity;
import it.matteo.bo.repository.primary.ClaimRepository;
import it.matteo.bo.service.BaseService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class ClaimService extends BaseService {

    @Inject
    ClaimRepository claimRepo;

    public Optional<ClaimEntity> read(final long id) {
        return claimRepo.findByIdOptional(id);
    }

    @Transactional
    public void delete(final long id) {
        claimRepo.deleteById(id);
    }

    public List<ClaimEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                claimRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
