package it.matteo.bo.service.primary;

import com.fasterxml.jackson.databind.JsonNode;
import it.matteo.bo.domain.primary.ConfigurationEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.primary.ConfigurationRepository;
import it.matteo.bo.service.BaseService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class ConfigurationService extends BaseService {

    @Inject
    ConfigurationRepository configurationRepo;

    public Optional<ConfigurationEntity> read(final long id) {
        return configurationRepo.findByIdOptional(id);
    }

    public JsonNode readConfJsonNode(final String nodeName) {
        final ConfigurationEntity entity = configurationRepo.findAll().firstResultOptional()
                .orElseThrow(() -> new EntityNotFoundException("No configuration entities found"));
        return entity.getConfiguration().findValue(nodeName);
    }

    public List<ConfigurationEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                configurationRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
