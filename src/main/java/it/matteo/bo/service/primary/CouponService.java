package it.matteo.bo.service.primary;

import it.matteo.bo.constants.CouponTypeEnum;
import it.matteo.bo.domain.primary.CouponEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.exception.ExportingDataException;
import it.matteo.bo.repository.primary.CouponRepository;
import it.matteo.bo.service.BaseService;
import it.matteo.bo.utils.UserUtils;
import it.matteo.bo.utils.UtilsBO;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class CouponService extends BaseService {

    @Inject
    CouponRepository couponRepo;

    @Inject
    UserUtils userUtils;

    @Transactional
    public CouponEntity createSingle(final CouponEntity newCoupon) {
        if (StringUtils.isBlank(newCoupon.getCouponCode())) {
            newCoupon.setCouponCode(
                    RandomStringUtils.randomAlphabetic(10));
        }
        newCoupon.setType(CouponTypeEnum.SINGLE.getValue());

        setPromotion(newCoupon, newCoupon);

        // use promotion dates if coupon dates are null
        if (newCoupon.getStartDate() == null) {
            newCoupon.setStartDate(newCoupon.getPromotion().getStartDate());
        }
        if (newCoupon.getEndDate() == null) {
            newCoupon.setEndDate(newCoupon.getPromotion().getEndDate());
        }

        if (newCoupon.getMaxUsage() == null || newCoupon.getMaxUsage() == -1) {
            newCoupon.setMaxUsage(Integer.MAX_VALUE);
        }

        newCoupon.setCreatedBy(userUtils.getUsername());

        couponRepo.persist(newCoupon);

        // Workaround - promotion not returning all data related to its rule
        fixPromotion(newCoupon.getPromotion());

        return newCoupon;
    }

    @Transactional
    public CouponEntity update(final long id, final CouponEntity newCoupon) {
        final CouponEntity currentCoupon = couponRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity coupon by id=%d not found", id)));

        setPromotion(currentCoupon, newCoupon);

        updateCoupon(currentCoupon, newCoupon);

        currentCoupon.setUpdatedBy(userUtils.getUsername());

        couponRepo.getEntityManager().merge(currentCoupon);
        return currentCoupon;
    }

    public Optional<CouponEntity> read(final Long id) {
        return couponRepo.findByIdOptional(id);
    }

    public Optional<CouponEntity> readByCode(final String couponCode, final boolean insensitive) {
        return couponRepo.findByCode(couponCode, insensitive);
    }

    public ByteArrayInputStream exportCsvMultiple() {
        return employeesToCSV(couponRepo.findMultiple());
    }

    private ByteArrayInputStream employeesToCSV(final List<CouponEntity> coupons) {
        final CSVFormat format = CSVFormat.DEFAULT.builder()
                .setHeader("ID", "Code", "Promotion ID", "Start date", "End date", "Active", "Max usage", "Type")
                .setDelimiter(";")
                .build();

        try (
                final ByteArrayOutputStream out = new ByteArrayOutputStream();
                final CSVPrinter csvPrinter = new CSVPrinter(new PrintWriter(out), format)
        ) {
            for (final CouponEntity coupon : coupons) {
                final List<String> data = Arrays.asList(
                        String.valueOf(coupon.getId()),
                        coupon.getCouponCode(),
                        String.valueOf(coupon.getPromotion().getId()),
                        coupon.getStartDate() == null
                                ? ""
                                : coupon.getStartDate().format(DateTimeFormatter.ofPattern(UtilsBO.DEFAULT_ISO_DATE_FORMAT)),
                        coupon.getEndDate() == null
                                ? ""
                                : coupon.getEndDate().format(DateTimeFormatter.ofPattern(UtilsBO.DEFAULT_ISO_DATE_FORMAT)),
                        coupon.isActive() ? "yes" : "no", String.valueOf(coupon.getMaxUsage()),
                        coupon.getMaxUsage() == null ? "" : String.valueOf(coupon.getMaxUsage()),
                        StringUtils.isBlank(coupon.getType()) ? "" : coupon.getType()
                );
                csvPrinter.printRecord(data);
            }

            csvPrinter.flush();
            return new ByteArrayInputStream(out.toByteArray());
        } catch (final Exception e) {
            throw new ExportingDataException("Failed to export data to CSV file: " + e.getMessage());
        }
    }

    @Transactional
    public void delete(final long id) {
        couponRepo.deleteById(id);
    }

    public List<CouponEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                couponRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
