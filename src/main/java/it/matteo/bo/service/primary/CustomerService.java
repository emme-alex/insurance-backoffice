package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.CustomerEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.primary.CustomerRepository;
import it.matteo.bo.service.BaseService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class CustomerService extends BaseService {

    @Inject
    CustomerRepository customerRepo;

    public Optional<CustomerEntity> read(final long id) {
        return customerRepo.findByIdOptional(id);
    }

    @Transactional
    public CustomerEntity update(final long id, final CustomerEntity customerEntity) {
        final CustomerEntity currentCustomer = customerRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity customer by id=%d not found", id)));

        currentCustomer.setCity(customerEntity.getCity());
        currentCustomer.setCountry(customerEntity.getCountry());
        currentCustomer.setGender(customerEntity.getGender());
        currentCustomer.setEducation(customerEntity.getEducation());
        currentCustomer.setProfession(customerEntity.getProfession());
        currentCustomer.setLanguage(customerEntity.getLanguage());
        currentCustomer.setLegalForm(customerEntity.getLegalForm());
        currentCustomer.setNdg(customerEntity.getNdg());
        currentCustomer.setSalary(customerEntity.getSalary());
        currentCustomer.setState(customerEntity.getState());
        currentCustomer.setStreet(customerEntity.getStreet());
        currentCustomer.setStreetNumber(customerEntity.getStreetNumber());
        currentCustomer.setName(customerEntity.getName());
        currentCustomer.setSurname(customerEntity.getSurname());
        currentCustomer.setUsername(customerEntity.getUsername()); // ?
        currentCustomer.setBirthCountry(customerEntity.getBirthCountry());
        currentCustomer.setBirthCity(customerEntity.getBirthCity());
        currentCustomer.setBirthState(customerEntity.getBirthState());
        currentCustomer.setDateOfBirth(customerEntity.getDateOfBirth());
        currentCustomer.setCountryId(customerEntity.getCountryId());
        currentCustomer.setStateId(customerEntity.getStateId());
        currentCustomer.setPrimaryMail(customerEntity.getPrimaryMail());
        currentCustomer.setSecondaryMail(customerEntity.getSecondaryMail());
        currentCustomer.setPrimaryPhone(customerEntity.getPrimaryPhone());
        currentCustomer.setSecondaryPhone(customerEntity.getSecondaryPhone());
        currentCustomer.setTaxCode(customerEntity.getTaxCode());
        currentCustomer.setZipCode(customerEntity.getZipCode());

        customerRepo.getEntityManager().merge(currentCustomer);
        return currentCustomer;
    }

    @Transactional
    public void delete(final long id) {
        customerRepo.deleteById(id);
    }

    public List<CustomerEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                customerRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
