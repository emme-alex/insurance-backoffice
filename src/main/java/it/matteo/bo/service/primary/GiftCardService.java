package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.GiftCardEntity;
import it.matteo.bo.domain.primary.UtmSourceEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.primary.GiftCardRepository;
import it.matteo.bo.repository.primary.UtmSourceRepository;
import it.matteo.bo.service.BaseService;
import it.matteo.bo.utils.UserUtils;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class GiftCardService extends BaseService {

    @Inject
    GiftCardRepository giftCardRepo;

    @Inject
    UtmSourceRepository utmSourceRepo;

    @Inject
    UserUtils userUtils;

    @Transactional
    public GiftCardEntity create(final GiftCardEntity giftCard) {
        giftCard.setCreatedBy(userUtils.getUsername());

        final UtmSourceEntity utmSource = utmSourceRepo.findByIdOptional(giftCard.getUtmSource().getId())
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity utmSource by id=%d not found", giftCard.getUtmSource().getId())));

        // Set utmSource and dates to the gift card
        giftCard.setUtmSource(utmSource);
        fixUtmSource(utmSource);

        giftCardRepo.persist(giftCard);

        return giftCard;
    }

    @Transactional
    public GiftCardEntity update(final Long id, final GiftCardEntity newGiftCard) {
        // Find the existing gift card by its ID
        final GiftCardEntity giftCard = giftCardRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity giftcard with ID '%s' does not exist", id)));

        // Update the gift card properties
        giftCard.setUniqueCardName(newGiftCard.getUniqueCardName());
        giftCard.setCardName(newGiftCard.getCardName());
        giftCard.setCardValue(newGiftCard.getCardValue());
        giftCard.setStartDate(newGiftCard.getStartDate());
        giftCard.setEndDate(newGiftCard.getEndDate());

        // Set and fix the utmSource
        final UtmSourceEntity utmSource = utmSourceRepo.findByIdOptional(newGiftCard.getUtmSource().getId())
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity utmSource by id=%d not found", giftCard.getUtmSource().getId())));

        giftCard.setUtmSource(utmSource);
        fixUtmSource(utmSource);

        // Add user and date details
        giftCard.setUpdatedBy(userUtils.getUsername());

        // Persist the updated gift card
        giftCardRepo.persist(giftCard);

        return giftCard;
    }


    public Optional<GiftCardEntity> read(final Long cardId) {
        return giftCardRepo.findByIdOptional(cardId);
    }

    @Transactional
    public void deleteGiftCardBatch(final Long batchId) {
        giftCardRepo.deleteById(batchId);
    }

    public List<GiftCardEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                giftCardRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}

