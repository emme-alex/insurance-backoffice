package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.OrderAnagStatesEntity;
import it.matteo.bo.domain.primary.OrderEntity;
import it.matteo.bo.domain.primary.PolicyEntity;
import it.matteo.bo.exception.DataValidationException;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.exception.ExternalServiceException;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.OrderBoundaryResponse;
import it.matteo.bo.repository.primary.OrderAnagStatesRepository;
import it.matteo.bo.repository.primary.OrderRepository;
import it.matteo.bo.repository.primary.PolicyRepository;
import it.matteo.bo.service.BaseService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class OrderService extends BaseService {

    @Inject
    OrderRepository orderRepo;

    @Inject
    PolicyRepository policyRepo;

    @Inject
    OrderAnagStatesRepository orderAnagStatesRepo;

    public Optional<OrderEntity> read(final Long id) {
        return orderRepo.findByIdOptional(id);
    }

    public Optional<OrderEntity> readOrderByCode(final String orderCode) {
        return orderRepo.findByCode(orderCode);
    }

    public Optional<OrderEntity> readOrderByPolicyId(final Long policyId) {
        final PolicyEntity policy = policyRepo.findByIdOptional(policyId)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity policy by id=%d not found", policyId)));
        if (policy.getOrder() == null || policy.getOrder().getId() == null) {
            throw new DataValidationException(
                    String.format("Entity policy %d has no order linked to it", policyId));
        }

        return orderRepo.findByIdOptional(policy.getOrder().getId());
    }

    public List<OrderEntity> readByPolicyCode(final String policyCode) {
        return orderRepo.findByPolicyCode(policyCode);
    }

    public List<OrderEntity> readOrderByCustomerId(final Long customerId) {
        return orderRepo.findByCustomerId(customerId);
    }

    @Transactional
    public OrderEntity cancel(final long id) throws ExternalServiceException {
        if (!policyRepo.findPoliciesByOrderId(id).isEmpty()) {
            throw new RuntimeException(String.format("Cannot cancel order=%d because it contains policies", id));
        }
        final Optional<OrderAnagStatesEntity> status = orderAnagStatesRepo.findCancelState();
        if (status.isEmpty()) {
            throw new RuntimeException("Could not find a cancel status in DB");
        }

        final OrderEntity currentOrder = orderRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity order id=%d not found", id)));
        fixOrder(currentOrder);
        currentOrder.setAnagState(status.get());

        fixProduct(currentOrder.getProduct());
        fixPacket(currentOrder.getPacket());

        orderRepo.getEntityManager().merge(currentOrder);
        return currentOrder;
    }

    public BaseResponseBoundary<OrderBoundaryResponse> confirm(final String orderCode) throws ExternalServiceException {
        return null;
    }

    private void fixOrder(final OrderEntity order) {
        if (order == null) {
            return;
        }
        // WORKAROUND
        // org.hibernate.LazyInitializationException: failed to lazily
        // initialize a collection of role: it.matteo.entity.ProductEntity.splits,
        // could not initialize proxy - no Session
        if (order.getOrderItem() != null) {
            order.getOrderItem().size();
        }
    }

    @Transactional
    public void delete(final long id) {
        orderRepo.deleteById(id);
    }

    public List<OrderEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                orderRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
