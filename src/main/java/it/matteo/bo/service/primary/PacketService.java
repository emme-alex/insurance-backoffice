package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.CategoryEntity;
import it.matteo.bo.domain.primary.PacketEntity;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.domain.primary.RelatedWarrantyEntity;
import it.matteo.bo.domain.primary.WarrantyEntity;
import it.matteo.bo.exception.DataValidationException;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.model.primary.RelatedWarrantyValidationBoundaryResponse;
import it.matteo.bo.repository.primary.CategoryRepository;
import it.matteo.bo.repository.primary.PacketRepository;
import it.matteo.bo.repository.primary.WarrantyRepository;
import it.matteo.bo.service.BaseService;
import it.matteo.bo.utils.RelatedWarrantiesEngine;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class PacketService extends BaseService {

    @Inject
    PacketRepository packetRepo;

    @Inject
    WarrantyRepository warrantyRepo;

    @Inject
    CategoryRepository categoryRepo;

    @Inject
    protected RelatedWarrantiesEngine relatedWarrantiesEngine;

    @Transactional
    public PacketEntity create(final PacketEntity newPacket) {
        if (newPacket.getWarranties() != null && !newPacket.getWarranties().isEmpty()) {
            newPacket.getWarranties()
                    .forEach((newWarranty) -> {
                        newWarranty.setPacket(newPacket);
                        updateWarranty(newWarranty, newWarranty);
                        updateCategory(newWarranty, newWarranty);
                    });
        }

        packetRepo.persist(newPacket);

        // Workaround - packet not returning all data related to its product
        if (newPacket.getProduct() != null && newPacket.getProduct().getId() != null) {
            final ProductEntity product = productRepo.findByIdOptional(newPacket.getProduct().getId())
                    .orElseThrow(() -> new EntityNotFoundException(String.format(
                            "Entity product by id=%d not found", newPacket.getProduct().getId())));
            newPacket.setProduct(product);
            fixProduct(newPacket.getProduct());
        } else {
            newPacket.setProduct(null);
        }

        return newPacket;
    }

    public Optional<PacketEntity> read(final long id) {
        return packetRepo.findByIdOptional(id);
    }

    public List<PacketEntity> readByIDs(final List<Long> id) {
        return packetRepo.findByPacketIds(id);
    }

    public List<PacketEntity> readByProductId(final Long productId) {
        return packetRepo.findByProductId(productId);
    }

    @Transactional
    public PacketEntity update(final long id, final PacketEntity newPacket) {
        final PacketEntity currentPacket = packetRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity packet by id=%d not found", id)));

        currentPacket.setName(newPacket.getName());
//        currentPacket.setBrokerId(newPacket.getBrokerId());
//        currentPacket.setProductId(newPacket.getProductId());
        currentPacket.setSku(newPacket.getSku());
        currentPacket.setDuration(newPacket.getDuration());
        currentPacket.setDescription(newPacket.getDescription());
        currentPacket.setDuration_type(newPacket.getDuration_type());
        currentPacket.setPlanName(newPacket.getPlanName());
        currentPacket.setFixedEndDate(newPacket.getFixedEndDate());
        currentPacket.setFixedStartDate(newPacket.getFixedStartDate());
        currentPacket.setPlanId(newPacket.getPlanId());
        currentPacket.setPlanName(newPacket.getPlanName());
        currentPacket.setPacketPremium(newPacket.getPacketPremium());

        fixPacket(currentPacket);
        if (currentPacket.getProduct() != null && currentPacket.getProduct().getId() != null) {
            fixProduct(currentPacket.getProduct());
        }

        if (newPacket.getWarranties() != null && !newPacket.getWarranties().isEmpty()) {
            updateRelatedWarranties(currentPacket, newPacket);
        }

        packetRepo.getEntityManager().merge(currentPacket);
        return currentPacket;
    }

    private void updateRelatedWarranties(
            final PacketEntity currentPacket,
            final PacketEntity newPacket) {

        final List<RelatedWarrantyEntity> currentRelatedWarranties = new ArrayList<>(currentPacket.getWarranties());
        final List<RelatedWarrantyEntity> newRelatedWarranties = new ArrayList<>(newPacket.getWarranties());

        for (int i = 0; i < newRelatedWarranties.size(); i++) {
            final RelatedWarrantyEntity currentRelatedWarranty;
            final RelatedWarrantyEntity newRelatedWarranty = newRelatedWarranties.get(i);

            if (i < currentRelatedWarranties.size()) {
                // Update warranty
                currentRelatedWarranty = currentRelatedWarranties.get(i);
            } else {
                // New warranty
                currentRelatedWarranty = new RelatedWarrantyEntity();
                currentRelatedWarranties.add(currentRelatedWarranty);
            }

            currentRelatedWarranty.setPacket(currentPacket);
            updateRelatedWarranty(currentRelatedWarranty, newRelatedWarranty);
        }

        // Remove warranties
        for (int i = currentRelatedWarranties.size() - 1; i >= newRelatedWarranties.size(); i--) {
            final RelatedWarrantyEntity currentRelatedWarranty = currentRelatedWarranties.remove(i);
            currentRelatedWarranty.setPacket(null);
        }

        currentPacket.getWarranties().clear();
        currentPacket.getWarranties().addAll(currentRelatedWarranties);

        checkRules(currentRelatedWarranties);
    }

    private void updateRelatedWarranty(
            final RelatedWarrantyEntity currentRelatedWarranty,
            final RelatedWarrantyEntity newRelatedWarranty) {

        updateWarranty(currentRelatedWarranty, newRelatedWarranty);
        currentRelatedWarranty.setParent(newRelatedWarranty.getParent());
        updateCategory(currentRelatedWarranty, newRelatedWarranty);
        currentRelatedWarranty.setMandatory(newRelatedWarranty.isMandatory());
        currentRelatedWarranty.setInsurancePremium(newRelatedWarranty.getInsurancePremium());
        currentRelatedWarranty.setStartDate(newRelatedWarranty.getStartDate());
        currentRelatedWarranty.setEndDate(newRelatedWarranty.getEndDate());
        currentRelatedWarranty.setRecurring(newRelatedWarranty.isRecurring());
    }

    private void updateWarranty(
            final RelatedWarrantyEntity currentRelatedWarranty,
            final RelatedWarrantyEntity newRelatedWarranty) {

        final WarrantyEntity updatingWarranty = warrantyRepo.findByIdOptional(
                        newRelatedWarranty.getWarranty().getId())
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity warranty by id=%d not found", newRelatedWarranty.getWarranty().getId())));
        currentRelatedWarranty.setWarranty(updatingWarranty);
    }

    private void updateCategory(
            final RelatedWarrantyEntity currentRelatedWarranty,
            final RelatedWarrantyEntity newRelatedWarranty) {

        if (newRelatedWarranty.getCategory() == null) {
            currentRelatedWarranty.setCategory(null);
            return;
        }

        final CategoryEntity updatingCategory =
                categoryRepo.findByIdOptional(newRelatedWarranty.getCategory().getId())
                        .orElseThrow(() -> new EntityNotFoundException(String.format(
                                "Entity category by id=%d not found", newRelatedWarranty.getCategory().getId())));
        currentRelatedWarranty.setCategory(updatingCategory);
    }

    private void checkRules(final List<RelatedWarrantyEntity> relatedWarranties) {
        relatedWarranties.forEach(relatedWarranty -> {
            if (relatedWarranty.getRule() != null) {
                validateRelatedWarrantyRule(relatedWarranty.getRule());
            }
        });

        // check constraint rules
        final RelatedWarrantyValidationBoundaryResponse relatedWarrantyRulesBoundaryResponse =
                relatedWarrantiesEngine.extractRelatedWarrantyRules(relatedWarranties, null);
        if (!relatedWarrantyRulesBoundaryResponse.isValid()) {
            throw new DataValidationException(
                    "Invalid set of related warranties. Check the rules of each related warranty!");
        }
    }

    @Transactional
    public void delete(final long id) {
        packetRepo.deleteById(id);
    }

    public List<PacketEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                packetRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
