package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.PaymentMethodEntity;
import it.matteo.bo.domain.primary.ProductsPaymentMethodsEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.primary.PaymentMethodRepository;
import it.matteo.bo.repository.primary.ProductsPaymentMethodsRepository;
import it.matteo.bo.service.BaseService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class PaymentMethodService extends BaseService {

    @Inject
    PaymentMethodRepository paymentMethodRepo;

    @Inject
    ProductsPaymentMethodsRepository productsPaymentMethodsRepo;

    @Transactional
    public PaymentMethodEntity create(final PaymentMethodEntity newPaymentMethod) {
        paymentMethodRepo.persist(newPaymentMethod);
        return newPaymentMethod;
    }

    public Optional<PaymentMethodEntity> read(final long id) {
        return paymentMethodRepo.findByIdOptional(id);
    }

    public List<PaymentMethodEntity> readByProductId(final long productId) {
        final List<ProductsPaymentMethodsEntity> productsPaymentMethods =
                productsPaymentMethodsRepo.findByProductId(productId);

        return productsPaymentMethods.stream()
                .map(productsPaymentMethodsEntity -> productsPaymentMethodsEntity.getPaymentMethod().getId())
                .map(paymentMethodId -> paymentMethodRepo.findById(paymentMethodId))
                .collect(Collectors.toList());
    }

    @Transactional
    public PaymentMethodEntity update(
            final long id,
            final PaymentMethodEntity newPaymentMethod) {

        final PaymentMethodEntity currentPaymentMethod = paymentMethodRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity payment method by id=%d not found", id)));

        currentPaymentMethod.setPaymentMethod(newPaymentMethod.getPaymentMethod());
        currentPaymentMethod.setType(newPaymentMethod.getType());
        currentPaymentMethod.setActive(newPaymentMethod.isActive());
        currentPaymentMethod.setExternalId(newPaymentMethod.getExternalId());

        paymentMethodRepo.getEntityManager().merge(currentPaymentMethod);
        return currentPaymentMethod;
    }

    @Transactional
    public void delete(final long id) {
        paymentMethodRepo.deleteById(id);
    }

    public List<PaymentMethodEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                paymentMethodRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}


