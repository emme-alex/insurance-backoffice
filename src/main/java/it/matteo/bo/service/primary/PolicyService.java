package it.matteo.bo.service.primary;

import it.matteo.bo.client.document.DocumentClient;
import it.matteo.bo.domain.primary.PolicyEntity;
import it.matteo.bo.model.primary.DownloadDocumentRequest;
import it.matteo.bo.model.primary.DownloadDocumentResponse;
import it.matteo.bo.repository.primary.PolicyRepository;
import it.matteo.bo.service.BaseService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class PolicyService extends BaseService {

    @Inject
    PolicyRepository policyRepo;

    @Inject
    @RestClient
    DocumentClient documentClient;

    public Optional<PolicyEntity> read(final long id) {
        return policyRepo.findByIdOptional(id);
    }

    public List<PolicyEntity> readPoliciesByOrderId(final Long orderId) {
        return policyRepo.findPoliciesByOrderId(orderId);
    }

    public Optional<PolicyEntity> readPolicyByCodeOrMasterNumber(final String code) {
        return policyRepo.findPolicyByCodeOrMasterNumber(code);
    }

    public List<PolicyEntity> readPoliciesByCustomerId(final Long customerId) {
        return policyRepo.findByCustomerId(customerId);
    }

    @Transactional
    public void delete(final long id) {
        policyRepo.deleteById(id);
    }

    public DownloadDocumentResponse downloadDocument(final String code) throws Exception {
        final PolicyEntity result = policyRepo.find("policy_code", code).firstResult();
        final DownloadDocumentRequest request = new DownloadDocumentRequest();
        request.setLink(result.getCertificateLink());
        try {
            final DownloadDocumentResponse response =
                    documentClient.download(request)
                            .readEntity(DownloadDocumentResponse.class);
            response.setFileName(result.getCertificateFileName());
            return response;
        } catch (final Exception e) {
            throw new Exception("While downloading certificate document");
        }
    }

    public List<PolicyEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                policyRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
