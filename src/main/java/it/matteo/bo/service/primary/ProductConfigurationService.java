package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.ProductConfigurationEntity;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.primary.ProductConfigurationRepository;
import it.matteo.bo.service.BaseService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class ProductConfigurationService extends BaseService {

    @Inject
    ProductConfigurationRepository configurationRepo;

    @Transactional
    public ProductConfigurationEntity create(final ProductConfigurationEntity newConfiguration) {
        if (newConfiguration.getProduct() != null && newConfiguration.getProduct().getId() != null) {
            newConfiguration.setProduct(
                    readProduct(newConfiguration.getProduct().getId())
            );
        } else {
            newConfiguration.setProduct(null);
        }

        configurationRepo.persist(newConfiguration);
        return newConfiguration;
    }

    public Optional<ProductConfigurationEntity> read(final long id) {
        return configurationRepo.findByIdOptional(id);
    }

    @Transactional
    public ProductConfigurationEntity update(final long id, final ProductConfigurationEntity newConfiguration) {
        final ProductConfigurationEntity currentConfiguration = configurationRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity configuration by id=%d not found", id)));

        currentConfiguration.setCertificate(newConfiguration.getCertificate());
        currentConfiguration.setEmission(newConfiguration.getEmission());
        currentConfiguration.setCanOpenClaim(newConfiguration.getCanOpenClaim());
        currentConfiguration.setEmissionPrefix(newConfiguration.getEmissionPrefix());
        currentConfiguration.setClaimProvider(newConfiguration.getClaimProvider());
        currentConfiguration.setClaimType(newConfiguration.getClaimType());

        if (newConfiguration.getProduct() != null && newConfiguration.getProduct().getId() != null) {
            final ProductEntity product = readProduct(newConfiguration.getProduct().getId());
            currentConfiguration.setProduct(product);
        }

        configurationRepo.getEntityManager().merge(currentConfiguration);
        return currentConfiguration;
    }

    @Transactional
    public void delete(final long id) {
        configurationRepo.deleteById(id);
    }

    public List<ProductConfigurationEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                configurationRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
