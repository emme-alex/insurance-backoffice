package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.CategoryEntity;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.domain.primary.SurveyAnswerEntity;
import it.matteo.bo.domain.primary.SurveyQuestionEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.model.primary.BaseResponseBoundary;
import it.matteo.bo.model.primary.ProductBoundaryResponse;
import it.matteo.bo.repository.primary.CategoryRepository;
import it.matteo.bo.repository.primary.ProductImagesRepository;
import it.matteo.bo.repository.primary.ProductRepository;
import it.matteo.bo.service.BaseService;
import it.matteo.bo.service.client.InvokeProduct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class ProductService extends BaseService {

    @Inject
    CategoryRepository categoryRepo;

    @Inject
    ProductImagesRepository productImagesRepo;

    @Inject
    InvokeProduct invokeProduct;

    public Optional<ProductEntity> read(final long id) {
        return productRepo.findByIdOptional(id);
    }

    public Optional<ProductEntity> readByCode(final String productCode) {
        return productRepo.find("product_code", productCode).singleResultOptional();
    }

    private CategoryEntity readCategory(final long id) {
        return categoryRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity category by id=%d not found", id)));
    }

    @Transactional
    public ProductEntity update(
            final long id,
            final ProductEntity newProduct) {

        final ProductEntity currentProduct = productRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity product by id=%d not found", id)));

        currentProduct.setProductDescription(newProduct.getProductDescription());
        currentProduct.setDescription(newProduct.getDescription());
        currentProduct.setShortDescription(newProduct.getShortDescription());
        currentProduct.setPrice(newProduct.getPrice());
        currentProduct.setDisplayPrice(newProduct.getDisplayPrice());
        currentProduct.setInformativeSet(newProduct.getInformativeSet());

//        updateQuestions(currentProduct, newProduct);
//        updateCategories(currentProduct, newProduct);

        fixProduct(currentProduct);

        productRepo.getEntityManager().merge(currentProduct);
        return currentProduct;
    }

    private void updateQuestions(
            final ProductEntity currentProduct,
            final ProductEntity newProduct) {

        final int updatingQuestionCount;
        final int updaterQuestionCount;
        SurveyQuestionEntity currentQuestion, newQuestion;
        updatingQuestionCount = currentProduct.getQuestions().size();
        updaterQuestionCount = newProduct.getQuestions().size();
        for (int i = 0; i < updaterQuestionCount; i++) {
            newQuestion = newProduct.getQuestions().get(i);
            if (i < updatingQuestionCount) {
                // Update question
                currentQuestion = currentProduct.getQuestions().get(i);
            } else {
                // New question
                currentQuestion = new SurveyQuestionEntity();
                currentProduct.getQuestions().add(currentQuestion);
            }

            currentQuestion.setProduct(currentProduct);
            currentQuestion.setPosition(i + 1);
            updateQuestion(currentQuestion, newQuestion);
        }

        // Remove questions
        for (int i = updatingQuestionCount - 1; i >= updaterQuestionCount; i--) {
            currentQuestion = currentProduct.getQuestions().remove(i);
            currentQuestion.setProduct(null);

            // Remove answers
            currentQuestion.getAnswers().forEach((removingAnswer) ->
                    removingAnswer.setQuestion(null));

            currentQuestion.getAnswers().clear();
        }
    }

    private void updateQuestion(
            final SurveyQuestionEntity currentQuestion,
            final SurveyQuestionEntity newQuestion) {

        SurveyAnswerEntity currentAnswer, newAnswer;

        currentQuestion.setContent(newQuestion.getContent());
        currentQuestion.setExternalCode(newQuestion.getExternalCode());

        final int currentAnswerCount = currentQuestion.getAnswers().size();
        final int newAnswerCount = newQuestion.getAnswers().size();
        for (int i = 0; i < newAnswerCount; i++) {
            newAnswer = newQuestion.getAnswers().get(i);
            if (i < currentAnswerCount) {
                // Update answer
                currentAnswer = currentQuestion.getAnswers().get(i);
            } else {
                // New answer
                currentAnswer = new SurveyAnswerEntity();
                currentQuestion.getAnswers().add(currentAnswer);
            }

            currentAnswer.setQuestion(currentQuestion);
            currentAnswer.setPosition(i + 1);
            updateAnswer(currentAnswer, newAnswer);
        }

        // Remove answer
        for (int i = currentAnswerCount - 1; i >= newAnswerCount; i--) {
            currentAnswer = currentQuestion.getAnswers().remove(i);
            currentAnswer.setQuestion(null);
        }
    }

    private void updateAnswer(
            final SurveyAnswerEntity currentAnswer,
            final SurveyAnswerEntity newAnswer) {

        currentAnswer.setValue(newAnswer.getValue());
        currentAnswer.setDefaultValue(newAnswer.isDefaultValue());
        currentAnswer.setExternalCode(newAnswer.getExternalCode());
    }

    private void updateCategories(
            final ProductEntity currentProduct,
            final ProductEntity newProduct) {

        // Remove categories
        currentProduct.getCategories().removeIf((updatingCategory) ->
                newProduct.getCategories().stream()
                        .noneMatch((updaterCategory) ->
                                Objects.equals(updaterCategory.getId(),
                                        updatingCategory.getId())));

        // New categories
        final Set<CategoryEntity> newCategories = newProduct.getCategories()
                .stream()
                .filter((updaterCategory) ->
                        currentProduct.getCategories().stream()
                                .noneMatch((updatingCategory) ->
                                        Objects.equals(updatingCategory.getId(), updaterCategory.getId())))
                .map((updaterCategory) -> readCategory(updaterCategory.getId()))
                .collect(Collectors.toSet());
        currentProduct.getCategories().addAll(newCategories);
    }

    @Transactional
    public void delete(final long id) {
        final Optional<ProductEntity> image = productRepo.findByIdOptional(id);
        productRepo.deleteById(id);
        image.ifPresent(productEntity -> productImagesRepo.delete(productEntity.getImage()));
    }

    public List<ProductEntity> readProductsByIDs(final List<Long> ids) {
        return productRepo.findByProductIDs(ids);
    }

    public List<ProductEntity> listActiveProducts(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                productRepo,
                ProductRepository.FIND_ACTIVE_PRODUCTS_QUERY,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

    public List<ProductEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                productRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

    public BaseResponseBoundary<List<ProductBoundaryResponse>> listRecommended() {
        return invokeProduct.findRecommendedProducts();
    }

}


