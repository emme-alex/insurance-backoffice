package it.matteo.bo.service.primary;

import it.matteo.bo.constants.CouponTypeEnum;
import it.matteo.bo.domain.primary.CouponEntity;
import it.matteo.bo.domain.primary.PromotionBatchEntity;
import it.matteo.bo.domain.primary.PromotionEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.primary.CouponRepository;
import it.matteo.bo.repository.primary.PromotionBatchRepository;
import it.matteo.bo.repository.primary.PromotionRuleRepository;
import it.matteo.bo.service.BaseService;
import it.matteo.bo.utils.UserUtils;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class PromotionBatchService extends BaseService {

    @Inject
    PromotionBatchRepository promotionBatchRepo;

    @Inject
    CouponRepository couponRepo;

    @Inject
    PromotionRuleRepository promotionRuleRepo;

    @Inject
    UserUtils userUtils;

    @Transactional
    public PromotionBatchEntity create(
            final PromotionBatchEntity promotionBatch,
            final LocalDateTime startDate,
            final LocalDateTime endDate) {

        promotionBatch.setCreatedBy(userUtils.getUsername());
        promotionBatchRepo.persist(promotionBatch);

        final PromotionEntity promotion = promotionRepo.findByIdOptional(promotionBatch.getPromotion().getId())
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity promotion by id=%d not found", promotionBatch.getPromotion().getId())));

        // create all the coupons
        final List<CouponEntity> newCoupons = new ArrayList<>();
        for (int i = 0; i < promotionBatch.getNumberOfCodes(); i++) {
            final String randVal = RandomStringUtils.randomAlphabetic(10) + i;

            final CouponEntity newCoupon = new CouponEntity();

            newCoupon.setPromotion(promotion);
            fixPromotion(newCoupon.getPromotion());

            newCoupon.setPromotionBatch(promotionBatch);
            fixPromotionBatch(newCoupon.getPromotionBatch());

            newCoupon.setActive(true);
            newCoupon.setType(CouponTypeEnum.MULTIPLE.getValue());
            newCoupon.setMaxUsage(1);

            if (startDate == null) {
                newCoupon.setStartDate(promotion.getStartDate());
            } else {
                newCoupon.setStartDate(startDate);
            }
            if (endDate == null) {
                newCoupon.setEndDate(promotion.getEndDate());
            } else {
                newCoupon.setEndDate(endDate);
            }

            if (StringUtils.isBlank(promotionBatch.getBaseCode())) {
                newCoupon.setCouponCode(randVal);
            } else {
                newCoupon.setCouponCode(promotionBatch.getBaseCode() + randVal);
            }

            newCoupon.setCreatedBy(userUtils.getUsername());
            newCoupons.add(newCoupon);
        }

        couponRepo.persist(newCoupons);

        // Workaround - promotionBatch not returning all coupons and data related to promotion
        promotionBatch.setCoupons(newCoupons);
        promotion.setRule(
                promotion.getRule() != null && promotion.getRule().getId() != null
                        ? promotionRuleRepo.findByIdOptional(promotion.getRule().getId()).orElse(null)
                        : null);
        promotionBatch.setPromotion(promotion);

        return promotionBatch;
    }

    public List<PromotionBatchEntity> readByPromotionId(final long id) {
        return promotionBatchRepo.listByPromotionId(id);
    }

    public List<PromotionBatchEntity> readByPromotionCode(final String promotionCode) {
        final PromotionEntity promotion = promotionRepo.findByCode(promotionCode)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity promotion by promotion_code=%s not found", promotionCode)));

        return promotionBatchRepo.listByPromotionId(promotion.getId());
    }

    public Optional<PromotionBatchEntity> read(final long id) {
        return promotionBatchRepo.findByIdOptional(id);
    }

    @Transactional
    public void delete(final long id) {
        promotionBatchRepo.deleteById(id);
    }

    public List<PromotionBatchEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                promotionBatchRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
