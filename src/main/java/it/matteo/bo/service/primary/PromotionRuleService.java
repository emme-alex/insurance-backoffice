package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.PromotionRuleEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.primary.PromotionRuleRepository;
import it.matteo.bo.service.BaseService;
import it.matteo.bo.utils.UserUtils;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class PromotionRuleService extends BaseService {

    @Inject
    PromotionRuleRepository promotionRuleRepo;

    @Inject
    UserUtils userUtils;

    @Transactional
    public PromotionRuleEntity create(final PromotionRuleEntity promotionRule) {
        validatePromotionRule(promotionRule.getRule());

        promotionRule.setCreatedBy(userUtils.getUsername());

        promotionRuleRepo.persist(promotionRule);

        return promotionRule;
    }

    @Transactional
    public PromotionRuleEntity update(final long id, final PromotionRuleEntity newPromotionRule) {
        final PromotionRuleEntity currentPromotionRule = promotionRuleRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity promotion rule by id=%d not found", id)));

        validatePromotionRule(newPromotionRule.getRule());

        currentPromotionRule.setName(newPromotionRule.getName());
        currentPromotionRule.setRule(newPromotionRule.getRule());

        currentPromotionRule.setUpdatedBy(userUtils.getUsername());

        promotionRuleRepo.getEntityManager().merge(currentPromotionRule);

        return currentPromotionRule;
    }


    public Optional<PromotionRuleEntity> read(final long id) {
        return promotionRuleRepo.findByIdOptional(id);
    }

    @Transactional
    public void delete(final long id) {
        promotionRuleRepo.deleteById(id);
    }

    public List<PromotionRuleEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                promotionRuleRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
