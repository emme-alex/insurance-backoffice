package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.CouponEntity;
import it.matteo.bo.domain.primary.PromotionEntity;
import it.matteo.bo.domain.primary.PromotionRuleEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.primary.PromotionRuleRepository;
import it.matteo.bo.service.BaseService;
import it.matteo.bo.utils.UserUtils;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class PromotionService extends BaseService {

    @Inject
    PromotionRuleRepository promotionRuleRepo;

    @Inject
    UserUtils userUtils;

    @Transactional
    public PromotionEntity create(final PromotionEntity newPromotion) {
        newPromotion.setCreatedBy(userUtils.getUsername());

        promotionRepo.persist(newPromotion);

        // Workaround - promotion not returning all data related to its rule
        setRule(newPromotion, newPromotion);

        return newPromotion;
    }

    @Transactional
    public PromotionEntity update(final long id, final PromotionEntity newPromotion) {
        final PromotionEntity currentPromotion = promotionRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity promotion by id=%d not found", id)));

        currentPromotion.setPromotionCode(newPromotion.getPromotionCode());
        currentPromotion.setEndDate(newPromotion.getEndDate());
        currentPromotion.setStartDate(newPromotion.getStartDate());
        currentPromotion.setDescription(newPromotion.getDescription());
        currentPromotion.setActive(newPromotion.isActive());

        fixPromotion(currentPromotion);

        if (newPromotion.getRule() != null && newPromotion.getRule().getId() != null) {
            setRule(currentPromotion, newPromotion);
        }

        if (newPromotion.getCoupons() != null && !newPromotion.getCoupons().isEmpty()) {
            updateCoupons(currentPromotion, newPromotion);
        }

        currentPromotion.setUpdatedBy(userUtils.getUsername());

        promotionRepo.getEntityManager().merge(currentPromotion);
        return currentPromotion;
    }

    private void setRule(final PromotionEntity currentPromotion, final PromotionEntity newPromotion) {
        if (newPromotion.getRule() != null && newPromotion.getRule().getId() != null) {
            final PromotionRuleEntity rule = promotionRuleRepo.findByIdOptional(newPromotion.getRule().getId())
                    .orElseThrow(() -> new EntityNotFoundException(String.format(
                            "Entity promotion rule by id=%d not found", newPromotion.getRule().getId())));
            validatePromotionRule(rule.getRule());

            currentPromotion.setRule(rule);
            fixRule(currentPromotion.getRule());
        } else {
            currentPromotion.setRule(null);
        }
    }

    private void updateCoupons(
            final PromotionEntity currentPromotion,
            final PromotionEntity newPromotion) {

        final List<CouponEntity> currentCoupons = new ArrayList<>(currentPromotion.getCoupons());
        final List<CouponEntity> newCoupons = new ArrayList<>(newPromotion.getCoupons());

        for (int i = 0; i < newCoupons.size(); i++) {
            final CouponEntity currentCoupon;
            final CouponEntity newCoupon = newCoupons.get(i);

            if (i < currentCoupons.size()) {
                // Update coupon
                currentCoupon = currentCoupons.get(i);
            } else {
                // New coupon
                currentCoupon = new CouponEntity();
                currentCoupons.add(currentCoupon);
            }

            currentCoupon.setPromotion(currentPromotion);
            updateCoupon(currentCoupon, newCoupon);
        }

        // Remove warranties
        for (int i = currentCoupons.size() - 1; i >= newCoupons.size(); i--) {
            final CouponEntity currentCoupon = currentCoupons.remove(i);
            currentCoupon.setPromotion(null);
        }

        currentPromotion.getCoupons().clear();
        currentPromotion.getCoupons().addAll(currentCoupons);
    }

    private void fixRule(final PromotionRuleEntity rule) {
        if (rule == null) {
            return;
        }
        // WORKAROUND
        // org.hibernate.LazyInitializationException: failed to lazily
        // initialize a collection of role: it.matteo.entity.ProductEntity.splits,
        // could not initialize proxy - no Session
        if (rule.getPromotions() != null) {
            rule.getPromotions().size();
        }
    }

    public Optional<PromotionEntity> read(final Long id) {
        return promotionRepo.findByIdOptional(id);
    }

    public Optional<PromotionEntity> readByCode(final String promotionCode) {
        return promotionRepo.findByCode(promotionCode);
    }

    @Transactional
    public void delete(final long id) {
        promotionRepo.deleteById(id);
    }

    public List<PromotionEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                promotionRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
