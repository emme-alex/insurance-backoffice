package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.CouponEntity;
import it.matteo.bo.domain.primary.CustomerEntity;
import it.matteo.bo.domain.primary.OrderEntity;
import it.matteo.bo.domain.primary.PromotionEntity;
import it.matteo.bo.domain.primary.PromotionsOrdersEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.primary.CouponRepository;
import it.matteo.bo.repository.primary.CustomerRepository;
import it.matteo.bo.repository.primary.OrderRepository;
import it.matteo.bo.repository.primary.PromotionsOrdersRepository;
import it.matteo.bo.service.BaseService;
import it.matteo.bo.utils.UserUtils;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;


@ApplicationScoped
public class PromotionsOrdersService extends BaseService {

    @Inject
    CouponRepository couponRepo;

    @Inject
    CustomerRepository customerRepo;

    @Inject
    OrderRepository orderRepo;

    @Inject
    PromotionsOrdersRepository promotionsOrdersRepo;

    @Inject
    UserUtils userUtils;

    @Transactional
    public PromotionsOrdersEntity create(final PromotionsOrdersEntity newPromotionsOrders) {
        setPromotionOrder(newPromotionsOrders);

        newPromotionsOrders.setCreatedBy(userUtils.getUsername());

        promotionsOrdersRepo.persist(newPromotionsOrders);

        return newPromotionsOrders;
    }

    @Transactional
    public PromotionsOrdersEntity update(final Long id, final PromotionsOrdersEntity updatedPromotionsOrder) {
        // Check if the promotion order exists
        final PromotionsOrdersEntity currentPromotionsOrder = promotionsOrdersRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity promotion orders by id=%d not found", id)));

        // Retrieve the customer entity and set it in the promotion order
        final String customerCode = updatedPromotionsOrder.getCustomer().getTaxCode();
        final CustomerEntity customer = customerRepo.findByTaxCode(customerCode)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity customer by tax_code=%s not found", customerCode)));
        currentPromotionsOrder.setCustomer(customer);

        doUpdate(currentPromotionsOrder, updatedPromotionsOrder);

        currentPromotionsOrder.setUpdatedBy(userUtils.getUsername());

        promotionsOrdersRepo.getEntityManager().merge(currentPromotionsOrder);

        return currentPromotionsOrder;
    }

    private void doUpdate(
            final PromotionsOrdersEntity currentPromotionsOrder,
            final PromotionsOrdersEntity updatedPromotionsOrder) {

        final OrderEntity order = orderRepo.findByCode(updatedPromotionsOrder.getOrder().getOrderCode())
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity order by order_code=%s not found",
                                updatedPromotionsOrder.getOrder().getOrderCode())));
        currentPromotionsOrder.setOrder(order);

        final String promotionCode = updatedPromotionsOrder.getPromotion().getPromotionCode();
        final PromotionEntity promotion = promotionRepo.findByCode(promotionCode)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity promotion by promotion_code=%s not found", promotionCode)));
        currentPromotionsOrder.setPromotion(promotion);

        // Retrieve the coupon entity and set it in the promotion order
        final String couponCode = updatedPromotionsOrder.getCoupon().getCouponCode();
        final CouponEntity coupon = couponRepo.findByCode(couponCode, false)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity coupon by coupon_code=%s not found", couponCode)));
        currentPromotionsOrder.setCoupon(coupon);
    }

    private void setPromotionOrder(final PromotionsOrdersEntity newPromotionsOrders) {
        // Set order_code
        final String orderCode = newPromotionsOrders.getOrder().getOrderCode();
        final OrderEntity order = orderRepo.findByCode(orderCode)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity order by order_code=%s not found", orderCode)));
        newPromotionsOrders.setOrder(order);

        // Set promotion_code
        final String promotionCode = newPromotionsOrders.getPromotion().getPromotionCode();
        final PromotionEntity promotion = promotionRepo.findByCode(promotionCode)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity promotion by promotion_code=%s not found", promotionCode)));
        newPromotionsOrders.setPromotion(promotion);

        // Set coupon_code
        final String couponCode = newPromotionsOrders.getCoupon().getCouponCode();
        final CouponEntity coupon = couponRepo.findByCode(couponCode, false)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity coupon by coupon_code=%s not found", couponCode)));
        newPromotionsOrders.setCoupon(coupon);

        // Set customer_code (customers.tax_code)
        final String customerCode = newPromotionsOrders.getCustomer().getTaxCode();
        final CustomerEntity customer = customerRepo.findByTaxCode(customerCode)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity customer by tax_code=%s not found", customerCode)));
        newPromotionsOrders.setCustomer(customer);
    }


    public List<PromotionsOrdersEntity> readByOrderCode(final String orderCode) {
        return promotionsOrdersRepo.findByOrderCode(orderCode);
    }

    public List<PromotionsOrdersEntity> readByPromotionCode(final String promotionCode) {
        return promotionsOrdersRepo.findByPromotionCode(promotionCode);
    }

    public List<PromotionsOrdersEntity> readByCouponCode(final String couponCode, final boolean insensitive) {
        return promotionsOrdersRepo.findByCouponCode(couponCode, insensitive);
    }

    public List<PromotionsOrdersEntity> readByCustomerCode(final String customerCode) {
        return promotionsOrdersRepo.findByCustomerCode(customerCode);
    }

    public boolean validateByCoupon(final String couponCode, final boolean insensitive) {
        final CouponEntity coupon = couponRepo.findByCode(couponCode, insensitive)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Entity coupon by coupon_code=%s not found", couponCode)));

        return coupon.getMaxUsage() == null
                || coupon.getMaxUsage() == -1
                || promotionsOrdersRepo.countByCouponCode(couponCode, insensitive) <= coupon.getMaxUsage();
    }

    public Optional<PromotionsOrdersEntity> read(final Long id) {
        return promotionsOrdersRepo.findByIdOptional(id);
    }


    @Transactional
    public void delete(final long id) {
        promotionsOrdersRepo.deleteById(id);
    }

    public List<PromotionsOrdersEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                promotionsOrdersRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
