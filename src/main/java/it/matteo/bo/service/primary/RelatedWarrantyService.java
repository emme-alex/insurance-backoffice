package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.PacketEntity;
import it.matteo.bo.domain.primary.RelatedWarrantyEntity;
import it.matteo.bo.exception.DataValidationException;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.mapper.RelatedWarrantyMapper;
import it.matteo.bo.model.primary.RelatedWarrantyValidationBoundaryRequest;
import it.matteo.bo.model.primary.RelatedWarrantyValidationBoundaryResponse;
import it.matteo.bo.repository.primary.PacketRepository;
import it.matteo.bo.repository.primary.RelatedWarrantyRepository;
import it.matteo.bo.service.BaseService;
import it.matteo.bo.utils.RelatedWarrantiesEngine;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class RelatedWarrantyService extends BaseService {

    @Inject
    PacketRepository packetRepo;

    @Inject
    protected RelatedWarrantyRepository relatedWarrantyRepo;

    @Inject
    protected RelatedWarrantiesEngine relatedWarrantiesEngine;

    public Optional<RelatedWarrantyEntity> read(final long id) {
        return relatedWarrantyRepo.findByIdOptional(id);
    }

    public List<RelatedWarrantyEntity> findByWarrantyAndPacket(final long warranty_id, final long packetId) {
        return relatedWarrantyRepo.list("warranties_id = ?1 AND packet_id = ?2", warranty_id, packetId);
    }

    public List<RelatedWarrantyEntity> findByPacket(final long packetId) {
        return relatedWarrantyRepo.list("packet_id", packetId);
    }

    public RelatedWarrantyValidationBoundaryResponse validateRules(
            final RelatedWarrantyValidationBoundaryRequest rq) {

        final List<RelatedWarrantyEntity> rqRelatedWarranties =
                RelatedWarrantyMapper.INSTANCE.requestToEntities(rq.getWarranties());
        // Requested by client: exception if duplicates are found
        final Set<Long> duplicates = getDuplicates(rqRelatedWarranties);
        if (!rqRelatedWarranties.isEmpty() && !duplicates.isEmpty()) {
            throw new DataValidationException(
                    String.format("The list of related warranties has duplicates (%s)! Please check the request!",
                            StringUtils.join(duplicates, ",")));
        }

        final List<RelatedWarrantyEntity> fixedListOfRelatedWarranties = new ArrayList<>();
        final PacketEntity packet = packetRepo.findByIdOptional(rq.getPacketId())
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Packet %d not found", rq.getPacketId())));
        // extract only the relatedWarranties related to the "packetId" specified in the request
        packet.getWarranties().forEach(packetRelatedWarranty -> {
            if (rqRelatedWarranties.stream().anyMatch(rw -> packetRelatedWarranty.getId().equals(rw.getId()))) {
                fixedListOfRelatedWarranties.add(packetRelatedWarranty);
            }
        });
        // Requested by client: exception if sending not existing warranties
        final Set<Long> notExistingOrNotRelatedToPacket =
                getNotExisting(fixedListOfRelatedWarranties, rqRelatedWarranties);
        if (!notExistingOrNotRelatedToPacket.isEmpty()) {
            throw new DataValidationException(
                    String.format(
                            "There are related warranties not existing in the DB or not related to packet %s: %s!" +
                                    " Please check the request!",
                            packet.getId(), StringUtils.join(notExistingOrNotRelatedToPacket, ",")));
        }

        return relatedWarrantiesEngine.extractRelatedWarrantyRules(fixedListOfRelatedWarranties, rq.getJustRemovedId());
    }

    private Set<Long> getDuplicates(final List<RelatedWarrantyEntity> list) {
        return list.stream()
                .collect(Collectors.groupingBy(RelatedWarrantyEntity::getId))
                .values().stream()
                .filter(duplicates -> duplicates.size() > 1)
                .flatMap(Collection::stream)
                .map(RelatedWarrantyEntity::getId)
                .collect(Collectors.toSet());
    }

    private Set<Long> getNotExisting(
            final List<RelatedWarrantyEntity> relatedToPacket,
            final List<RelatedWarrantyEntity> fromRq) {

        final Set<Long> relatedToPacketIds =
                relatedToPacket.stream().map(RelatedWarrantyEntity::getId).collect(Collectors.toSet());
        return fromRq.stream()
                .map(RelatedWarrantyEntity::getId)
                .filter(element -> !relatedToPacketIds.contains(element))
                .collect(Collectors.toSet());
    }

    @Transactional
    public void delete(final long id) {
        relatedWarrantyRepo.deleteById(id);
    }

    public List<RelatedWarrantyEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                relatedWarrantyRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
