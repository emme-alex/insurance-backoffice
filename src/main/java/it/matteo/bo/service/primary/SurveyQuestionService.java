package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.domain.primary.SurveyQuestionEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.primary.SurveyQuestionRepository;
import it.matteo.bo.service.BaseService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class SurveyQuestionService extends BaseService {

    @Inject
    SurveyQuestionRepository surveyQuestionRepo;

    @Transactional
    public SurveyQuestionEntity create(final SurveyQuestionEntity newSurveyQuestion) {
        if (newSurveyQuestion.getProduct() != null && newSurveyQuestion.getProduct().getId() != null) {
            updateProduct(newSurveyQuestion, newSurveyQuestion);
        } else {
            newSurveyQuestion.setProduct(null);
        }

        surveyQuestionRepo.persist(newSurveyQuestion);
        return newSurveyQuestion;
    }

    public Optional<SurveyQuestionEntity> read(final long id) {
        return surveyQuestionRepo.findByIdOptional(id);
    }

    @Transactional
    public SurveyQuestionEntity update(final long id, final SurveyQuestionEntity newSurveyQuestion) {
        final SurveyQuestionEntity currentSurveyQuestion = surveyQuestionRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity survey question by id=%d not found", id)));

        currentSurveyQuestion.setPosition(newSurveyQuestion.getPosition());
        currentSurveyQuestion.setContent(newSurveyQuestion.getContent());
        currentSurveyQuestion.setExternalCode(newSurveyQuestion.getExternalCode());

        if (newSurveyQuestion.getProduct() != null && newSurveyQuestion.getProduct().getId() != null) {
            updateProduct(currentSurveyQuestion, newSurveyQuestion);
        } else {
            newSurveyQuestion.setProduct(null);
        }

        surveyQuestionRepo.getEntityManager().merge(currentSurveyQuestion);

        return currentSurveyQuestion;
    }

    @Transactional
    public void delete(final long id) {
        surveyQuestionRepo.deleteById(id);
    }

    private void updateProduct(
            final SurveyQuestionEntity currentSurveyQuestion,
            final SurveyQuestionEntity newSurveyQuestion) {

        final ProductEntity newProductEntity =
                productRepo.findByIdOptional(newSurveyQuestion.getProduct().getId())
                        .orElseThrow(() -> new EntityNotFoundException(String.format(
                                "Entity product by id=%d not found", newSurveyQuestion.getProduct().getId())));

        currentSurveyQuestion.setProduct(newProductEntity);
    }

    public List<SurveyQuestionEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                surveyQuestionRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
