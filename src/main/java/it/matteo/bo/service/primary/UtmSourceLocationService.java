package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.UtmSourceEntity;
import it.matteo.bo.domain.primary.UtmSourceLocationEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.primary.UtmSourceRepository;
import it.matteo.bo.service.BaseService;
import it.matteo.bo.utils.UserUtils;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class UtmSourceLocationService extends BaseService {

    @Inject
    UtmSourceRepository utmSourceRepo;

    @Inject
    UserUtils userUtils;

    @Transactional
    public UtmSourceLocationEntity create(final UtmSourceLocationEntity newUtmSourceLocation) {
        newUtmSourceLocation.setCreatedBy(userUtils.getUsername());

        utmSourceLocationRepo.persist(newUtmSourceLocation);

        // Workaround - utmSource location not returning utmSource related data
        if (newUtmSourceLocation.getUtmSource() != null && newUtmSourceLocation.getUtmSource().getId() != null) {
            final UtmSourceEntity utmSourceEntity = utmSourceRepo.findByIdOptional(
                            newUtmSourceLocation.getUtmSource().getId())
                    .orElseThrow(() -> new EntityNotFoundException(String.format(
                            "Entity utmSource by id=%d not found", newUtmSourceLocation.getUtmSource().getId())));
            newUtmSourceLocation.setUtmSource(utmSourceEntity);
            fixUtmSource(utmSourceEntity);
        } else {
            newUtmSourceLocation.setUtmSource(null);
        }

        return newUtmSourceLocation;
    }

    public Optional<UtmSourceLocationEntity> read(final long id) {
        return utmSourceLocationRepo.findByIdOptional(id);
    }

    @Transactional
    public UtmSourceLocationEntity update(final long id, final UtmSourceLocationEntity newUtmSourceLocation) {
        final UtmSourceLocationEntity currentUtmSourceLocation = readUtmSourceLocation(id);

        currentUtmSourceLocation.setName(newUtmSourceLocation.getName());

        currentUtmSourceLocation.setCode(newUtmSourceLocation.getCode());

        currentUtmSourceLocation.setUpdatedBy(userUtils.getUsername());

        if (newUtmSourceLocation.getUtmSource() != null && newUtmSourceLocation.getUtmSource().getId() != null) {
            final UtmSourceEntity utmSourceEntity = utmSourceRepo.findByIdOptional(
                            newUtmSourceLocation.getUtmSource().getId())
                    .orElseThrow(() -> new EntityNotFoundException(String.format(
                            "Entity utmSource by id=%d not found", newUtmSourceLocation.getUtmSource().getId())));
            currentUtmSourceLocation.setUtmSource(utmSourceEntity);
        }
        if (currentUtmSourceLocation.getUtmSource() != null && currentUtmSourceLocation.getUtmSource().getId() != null) {
            fixUtmSource(currentUtmSourceLocation.getUtmSource());
        }

        utmSourceLocationRepo.getEntityManager().merge(currentUtmSourceLocation);

        return currentUtmSourceLocation;
    }

    public Optional<UtmSourceLocationEntity> readByCode(final String utmSourceLocationCode) {
        return utmSourceLocationRepo.findByCode(utmSourceLocationCode);
    }

    @Transactional
    public void delete(final long id) {
        utmSourceLocationRepo.deleteById(id);
    }

    public List<UtmSourceLocationEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                utmSourceLocationRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
