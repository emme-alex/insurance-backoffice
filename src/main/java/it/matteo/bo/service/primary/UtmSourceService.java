package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.domain.primary.UtmSourceEntity;
import it.matteo.bo.domain.primary.UtmSourceLocationEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.primary.UtmSourceRepository;
import it.matteo.bo.service.BaseService;
import it.matteo.bo.utils.UserUtils;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class UtmSourceService extends BaseService {

    @Inject
    UtmSourceRepository utmSourceRepo;

    @Inject
    UserUtils userUtils;

    @Transactional
    public UtmSourceEntity create(final UtmSourceEntity newUtmSource) {
        newUtmSource.setCreatedBy(userUtils.getUsername());

        if (newUtmSource.getLocations() != null && !newUtmSource.getLocations().isEmpty()) {
            final Set<UtmSourceLocationEntity> newLocations = newUtmSource.getLocations()
                    .stream()
                    .map((newLocation) -> {
                        final UtmSourceLocationEntity location = readUtmSourceLocation(newLocation.getId());
                        location.setUtmSource(newUtmSource);
                        return location;
                    })
                    .collect(Collectors.toSet());

            newUtmSource.getLocations().clear();
            newUtmSource.getLocations().addAll(newLocations);
        }

        if (newUtmSource.getProducts() != null && !newUtmSource.getProducts().isEmpty()) {
            final Set<ProductEntity> newProducts = newUtmSource.getProducts()
                    .stream()
                    .map((newProduct) -> {
                        final ProductEntity product = readProduct(newProduct.getId());
                        product.getUtmSources().add(newUtmSource);
                        return product;
                    })
                    .collect(Collectors.toSet());

            newUtmSource.getProducts().clear();
            newUtmSource.getProducts().addAll(newProducts);
        }

        utmSourceRepo.persist(newUtmSource);
        return newUtmSource;
    }

    public Optional<UtmSourceEntity> read(final long id) {
        return utmSourceRepo.findByIdOptional(id);
    }

    @Transactional
    public UtmSourceEntity update(final long id, final UtmSourceEntity newUtmSource) {
        final UtmSourceEntity currentUtmSource = utmSourceRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity utmSource by id=%d not found", id)));

        currentUtmSource.setName(newUtmSource.getName());

        currentUtmSource.setUpdatedBy(userUtils.getUsername());

        if (newUtmSource.getProducts() != null && !newUtmSource.getProducts().isEmpty()) {
            updateProducts(currentUtmSource, newUtmSource);
        } else if (currentUtmSource.getProducts() != null) {
            currentUtmSource.getProducts().forEach(this::fixProduct);
        }

        if (newUtmSource.getLocations() != null && !newUtmSource.getLocations().isEmpty()) {
            updateLocations(currentUtmSource, newUtmSource);
        } else {
            fixUtmSource(currentUtmSource);
        }

        utmSourceRepo.getEntityManager().merge(currentUtmSource);

        return currentUtmSource;
    }

    private void updateLocations(
            final UtmSourceEntity currentUtmSource,
            final UtmSourceEntity newUtmSource) {

        // Remove locations
        final Set<UtmSourceLocationEntity> removingLocations =
                currentUtmSource.getLocations().stream()
                        .filter((currentLocation) ->
                                newUtmSource.getLocations().stream()
                                        .noneMatch((updaterLocation) ->
                                                Objects.equals(updaterLocation.getId(), currentLocation.getId())))
                        .collect(Collectors.toSet());

        removingLocations.forEach((removingLocation) -> removingLocation.setUtmSource(null));

        currentUtmSource.getLocations().removeAll(removingLocations);

        // New products
        final Set<UtmSourceLocationEntity> newLocations = newUtmSource.getLocations()
                .stream()
                .filter((updaterLocation) ->
                        currentUtmSource.getLocations().stream()
                                .noneMatch((updatingLocation) ->
                                        Objects.equals(updatingLocation.getId(), updaterLocation.getId())))
                .map((updaterLocation) -> {
                    final UtmSourceLocationEntity location = readUtmSourceLocation(updaterLocation.getId());
                    location.setUtmSource(currentUtmSource);
                    return location;
                })
                .collect(Collectors.toSet());

        currentUtmSource.getLocations().addAll(newLocations);
    }

    private void updateLocation(
            final UtmSourceLocationEntity currentLocation,
            final UtmSourceLocationEntity newLocation) {

        currentLocation.setCode(newLocation.getCode());
        currentLocation.setName(newLocation.getName());
    }

    private void updateProducts(
            final UtmSourceEntity currentUtmSource,
            final UtmSourceEntity newUtmSource) {

        currentUtmSource.getProducts().forEach(this::fixProduct);

        // Remove products
        final Set<ProductEntity> removingProducts =
                currentUtmSource.getProducts().stream()
                        .filter((currentCategoryProduct) ->
                                newUtmSource.getProducts().stream()
                                        .noneMatch((updaterProduct) ->
                                                Objects.equals(updaterProduct.getId(), currentCategoryProduct.getId())))
                        .collect(Collectors.toSet());

        removingProducts.forEach((removingProduct) ->
                removingProduct.getCategories().removeIf((removingCategory) ->
                        Objects.equals(removingCategory.getId(), currentUtmSource.getId())));

        currentUtmSource.getProducts().removeAll(removingProducts);

        // New products
        final Set<ProductEntity> newProducts = newUtmSource.getProducts()
                .stream()
                .filter((updaterProduct) ->
                        currentUtmSource.getProducts().stream()
                                .noneMatch((updatingProduct) ->
                                        Objects.equals(updatingProduct.getId(), updaterProduct.getId())))
                .map((updaterProduct) -> {
                    final ProductEntity product = readProduct(updaterProduct.getId());
                    product.getUtmSources().add(currentUtmSource);
                    return product;
                })
                .collect(Collectors.toSet());

        currentUtmSource.getProducts().addAll(newProducts);
    }

    @Transactional
    public void delete(final long id) {
        utmSourceRepo.deleteById(id);
    }

    public Optional<UtmSourceEntity> readByCode(final String utmSourceCode) {
        return utmSourceRepo.findByCode(utmSourceCode);
    }

    public List<UtmSourceEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                utmSourceRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
