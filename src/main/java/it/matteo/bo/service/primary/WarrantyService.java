package it.matteo.bo.service.primary;

import it.matteo.bo.domain.primary.BranchEntity;
import it.matteo.bo.domain.primary.WarrantyEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.primary.BranchRepository;
import it.matteo.bo.repository.primary.WarrantyRepository;
import it.matteo.bo.service.BaseService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class WarrantyService extends BaseService {

    @Inject
    WarrantyRepository warrantyRepo;

    @Inject
    BranchRepository branchRepository;

    @Transactional
    public WarrantyEntity create(final WarrantyEntity newWarranty) {
        if (newWarranty.getBranch() != null && newWarranty.getBranch().getId() != null) {
            updateBranch(newWarranty, newWarranty);
        } else {
            newWarranty.setBranch(null);
        }

        warrantyRepo.persist(newWarranty);
        return newWarranty;
    }

    public Optional<WarrantyEntity> read(final long id) {
        return warrantyRepo.findByIdOptional(id);
    }

    @Transactional
    public WarrantyEntity update(final long id, final WarrantyEntity newWarranty) {
        final WarrantyEntity currentWarranty = warrantyRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity warranty by id=%d not found", id)));

        currentWarranty.setName(newWarranty.getName());
        currentWarranty.setCeilings(newWarranty.getCeilings());
        currentWarranty.setImages(newWarranty.getImages());
        currentWarranty.setInternalCode(newWarranty.getInternalCode());

        if (newWarranty.getBranch() != null && newWarranty.getBranch().getId() != null) {
            updateBranch(currentWarranty, newWarranty);
        } else {
            newWarranty.setBranch(null);
        }

        warrantyRepo.getEntityManager().merge(currentWarranty);

        return currentWarranty;
    }

    @Transactional
    public void delete(final long id) {
        warrantyRepo.deleteById(id);
    }

    private void updateBranch(
            final WarrantyEntity currentWarranty,
            final WarrantyEntity newWarranty) {

        final BranchEntity newBranchEntity =
                branchRepository.findByIdOptional(newWarranty.getBranch().getId())
                        .orElseThrow(() -> new EntityNotFoundException(String.format(
                                "Entity branch by id=%d not found", newWarranty.getBranch().getId())));

        currentWarranty.setBranch(newBranchEntity);
    }

    public List<WarrantyEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                warrantyRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
