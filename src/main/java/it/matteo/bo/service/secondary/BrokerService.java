package it.matteo.bo.service.secondary;

import it.matteo.bo.domain.secondary.BrokerEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.secondary.BrokerRepository;
import it.matteo.bo.service.BaseService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class BrokerService extends BaseService {

    @Inject
    BrokerRepository brokerRepo;

    @Transactional
    public BrokerEntity create(final BrokerEntity newBroker) {
        brokerRepo.persist(newBroker);
        return newBroker;
    }

    public Optional<BrokerEntity> read(final long id) {
        return brokerRepo.findByIdOptional(id);
    }

    @Transactional
    public BrokerEntity update(
            final long id,
            final BrokerEntity newBroker) {

        final BrokerEntity currentBroker = brokerRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity broker by id=%d not found", id)));

        currentBroker.setName(newBroker.getName());
        currentBroker.setIdentitycode(newBroker.getIdentitycode());
        currentBroker.setDescription(newBroker.getDescription());

        brokerRepo.getEntityManager().merge(currentBroker);
        return currentBroker;
    }

    @Transactional
    public void delete(final long id) {
        brokerRepo.deleteById(id);
    }

    public List<BrokerEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                brokerRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}


