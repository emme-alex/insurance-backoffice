package it.matteo.bo.service.secondary;

import it.matteo.bo.domain.secondary.CatalogEntity;
import it.matteo.bo.repository.secondary.CatalogRepository;
import it.matteo.bo.service.BaseService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class CatalogService extends BaseService {

    @Inject
    CatalogRepository catalogRepository;

    public Optional<CatalogEntity> read(final long id) {
        return catalogRepository.findByIdOptional(id);
    }

    public List<CatalogEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                catalogRepository,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}
