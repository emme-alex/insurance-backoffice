package it.matteo.bo.service.secondary;

import it.matteo.bo.domain.secondary.CompanyEntity;
import it.matteo.bo.exception.EntityNotFoundException;
import it.matteo.bo.repository.secondary.CompanyRepository;
import it.matteo.bo.service.BaseService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class CompanyService extends BaseService {

    @Inject
    CompanyRepository companyRepo;

    @Transactional
    public CompanyEntity create(final CompanyEntity newCompany) {
        companyRepo.persist(newCompany);
        return newCompany;
    }

    public Optional<CompanyEntity> read(final long id) {
        return companyRepo.findByIdOptional(id);
    }

    @Transactional
    public CompanyEntity update(
            final long id,
            final CompanyEntity newCompany) {

        final CompanyEntity currentCompany = companyRepo.findByIdOptional(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(
                        "Entity company by id=%d not found", id)));

        currentCompany.setName(newCompany.getName());
        currentCompany.setDescription(newCompany.getDescription());

        companyRepo.getEntityManager().merge(currentCompany);
        return currentCompany;
    }

    @Transactional
    public void delete(final long id) {
        companyRepo.deleteById(id);
    }

    public List<CompanyEntity> list(
            final Integer pageSize,
            final Integer pageIndex,
            final String order,
            final String sort,
            final Map<String, Object> filter) {

        return defaultPaginatedList(
                companyRepo,
                pageSize,
                pageIndex,
                order,
                sort,
                filter);
    }

}


