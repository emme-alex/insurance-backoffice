package it.matteo.bo.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import it.matteo.bo.constants.RelatedWarrantyRulesErrorsEnum;
import it.matteo.bo.domain.primary.RelatedWarrantyEntity;
import it.matteo.bo.exception.RelatedWarrantyRulesException;
import it.matteo.bo.model.RelatedWarrantyRulesExceptionTO;
import it.matteo.bo.model.RelatedWarrantyRulesExceptionWarrantyTO;
import it.matteo.bo.model.RelatedWarrantyRulesTO;
import it.matteo.bo.model.primary.RelatedWarrantyValidationBoundaryResponse;
import it.matteo.bo.repository.primary.RelatedWarrantyRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@ApplicationScoped
public class RelatedWarrantiesEngine {

    private static final Logger LOG = LoggerFactory.getLogger(RelatedWarrantiesEngine.class);

    @Inject
    protected RelatedWarrantyRepository relatedWarrantyRepo;

    public RelatedWarrantyValidationBoundaryResponse extractRelatedWarrantyRules(
            final List<RelatedWarrantyEntity> relatedWarranties,
            final Long justRemovedId) {

        final RelatedWarrantyValidationBoundaryResponse relatedWarrantyValidationBoundaryResponse =
                new RelatedWarrantyValidationBoundaryResponse();

        if (relatedWarranties == null || relatedWarranties.isEmpty()) {
            relatedWarrantyValidationBoundaryResponse.setValid(true);
            return relatedWarrantyValidationBoundaryResponse;
        }

        final List<RelatedWarrantyRulesExceptionTO> listOfExceptions = new ArrayList<>();

        relatedWarranties.forEach(relatedWarranty -> {
            final RelatedWarrantyRulesTO rulesTo = RelatedWarrantyRulesTO.builder().build();
            rulesTo.setId(relatedWarranty.getId());
            if (relatedWarranty.getPacket() != null) {
                rulesTo.setPacketId(relatedWarranty.getPacket().getId());
            }
            if (relatedWarranty.getExternalCode() != null) {
                rulesTo.setExternalCode(relatedWarranty.getExternalCode());
            }
            if (relatedWarranty.getWarranty() != null
                    && StringUtils.isNotBlank(relatedWarranty.getWarranty().getInternalCode())) {

                rulesTo.setWarrantyInternalCode(relatedWarranty.getWarranty().getInternalCode());
            }
            if (relatedWarranty.getWarranty() != null
                    && StringUtils.isNotBlank(relatedWarranty.getWarranty().getName())) {

                rulesTo.setWarrantyName(relatedWarranty.getWarranty().getName());
            }

            relatedWarrantyValidationBoundaryResponse.getRules().add(rulesTo);

            if (relatedWarranty.getRule() == null || relatedWarranty.getRule().isEmpty()) {
                LOG.warn("The relatedWarranty {} has NO rules. Skipping it...!", relatedWarranty.getId());
                return;
            }

            final JsonNode constraint = relatedWarranty.getRule();
            final JsonNode dependencies = constraint.get("dependencies");
            final JsonNode possibleDependencies = constraint.get("possible_dependencies");
            final JsonNode conflicts = constraint.get("conflicts");

            if (dependencies != null && dependencies.isArray()) {
                for (final JsonNode dependency : dependencies) {
                    rulesTo.getDependencies().add(dependency.longValue());
                }
            }
            if (possibleDependencies != null && possibleDependencies.isArray()) {
                for (final JsonNode possibleDependency : possibleDependencies) {
                    if (possibleDependency != null && possibleDependency.isArray()) {
                        try {
                            rulesTo.getPossibleDependencies().add(
                                    UtilsBO.OBJECT_MAPPER
                                            .readerFor(new TypeReference<List<Long>>() {
                                            })
                                            .readValue(possibleDependency)
                            );
                        } catch (final IOException e) {
                            LOG.error("While parsing possible_dependencies in related warranty rules", e);
                        }
                    }
                }
            }
            if (conflicts != null && conflicts.isArray()) {
                for (final JsonNode conflict : conflicts) {
                    rulesTo.getConflicts().add(conflict.longValue());
                }
            }

            rulesTo.setHasConstraints(
                    (conflicts != null && !conflicts.isEmpty()) ||
                            (possibleDependencies != null && !possibleDependencies.isEmpty()) ||
                            (dependencies != null && !dependencies.isEmpty())
            );
        });

        // validate
        for (final RelatedWarrantyRulesTO rulesTo : relatedWarrantyValidationBoundaryResponse.getRules()) {
            final List<RelatedWarrantyRulesTO> missingDeps = new ArrayList<>();
            rulesTo.getDependencies()
                    .forEach(depId -> {
                        if (relatedWarrantyValidationBoundaryResponse.getRules().stream()
                                .noneMatch(rwr -> Objects.equals(depId, rwr.getId()))) {

                            missingDeps.add(
                                    buildWarrantyRulesTO(relatedWarrantyRepo.safeFindById(depId), depId));
                        }
                    });
            if (!missingDeps.isEmpty()) {
                listOfExceptions.add(
                        RelatedWarrantyRulesExceptionTO.builder()
                                .errorCode(checkIsRemoving(
                                        justRemovedId,
                                        missingDeps.stream().map(RelatedWarrantyRulesTO::getId).toList())
                                        ? RelatedWarrantyRulesErrorsEnum.ERR_DEL_WARRANTY
                                        : RelatedWarrantyRulesErrorsEnum.ERR_ADD_WARRANTY)
                                .warranties(missingDeps.stream()
                                        .map(relatedWarrantyRulesTO ->
                                                checkIsRemoving(justRemovedId, List.of(relatedWarrantyRulesTO.getId()))
                                                        ? buildRulesExceptionWarrantyTO(rulesTo, relatedWarrantyRulesTO.getId())
                                                        : buildRulesExceptionWarrantyTO(relatedWarrantyRulesTO, rulesTo.getId()))
                                        .collect(Collectors.toSet()))
                                .build());
            }

            final List<RelatedWarrantyRulesTO> foundConflicts = new ArrayList<>();
            rulesTo.getConflicts()
                    .forEach(confId -> {
                        if (relatedWarrantyValidationBoundaryResponse.getRules().stream()
                                .anyMatch(rwr -> Objects.equals(confId, rwr.getId()))) {

                            foundConflicts.add(
                                    buildWarrantyRulesTO(relatedWarrantyRepo.safeFindById(confId), confId));
                        }
                    });
            if (!foundConflicts.isEmpty()) {
                listOfExceptions.add(
                        RelatedWarrantyRulesExceptionTO.builder()
                                .errorCode(RelatedWarrantyRulesErrorsEnum.ERR_DEL_WARRANTY)
                                .warranties(foundConflicts.stream()
                                        .map(relatedWarrantyRulesTO ->
                                                buildRulesExceptionWarrantyTO(relatedWarrantyRulesTO, rulesTo.getId()))
                                        .collect(Collectors.toSet()))
                                .build());
            }

            final AtomicBoolean validPossibleDep = new AtomicBoolean(rulesTo.getPossibleDependencies().isEmpty());
            // only one of the possibleDependencies blocks has to be true to be valid
            for (final List<Long> blockOfDependencies : rulesTo.getPossibleDependencies()) {
                final boolean blockOk = blockOfDependencies.stream()
                        .allMatch(depId ->
                                relatedWarrantyValidationBoundaryResponse.getRules().stream()
                                        .anyMatch(rwr -> depId.equals(rwr.getId())));
                if (blockOk) {
                    validPossibleDep.set(true);
                    break;
                }
            }
            if (!validPossibleDep.get()) {
                listOfExceptions.add(
                        RelatedWarrantyRulesExceptionTO.builder()
                                .errorCode(RelatedWarrantyRulesErrorsEnum.ERR_POSS_ADD_WARRANTY)
                                .possibleWarranties(rulesTo.getPossibleDependencies().stream()
                                        .map(blockDepIds ->
                                                blockDepIds.stream()
                                                        .map(possDepId -> buildRulesExceptionWarrantyTO(
                                                                relatedWarrantyRepo.safeFindById(possDepId),
                                                                possDepId,
                                                                rulesTo.getId()))
                                                        .toList())
                                        .collect(Collectors.toSet()))
                                .build());
            }
        }

        if (!listOfExceptions.isEmpty()) {
            listOfExceptions
                    .sort(Comparator.comparing(RelatedWarrantyRulesExceptionTO::getErrorCode));
            throw new RelatedWarrantyRulesException(listOfExceptions);
        }

        relatedWarrantyValidationBoundaryResponse.setValid(true);
        relatedWarrantyValidationBoundaryResponse.getRules()
                .sort(Comparator.comparing(RelatedWarrantyRulesTO::getId));

        return relatedWarrantyValidationBoundaryResponse;
    }

    private RelatedWarrantyRulesExceptionWarrantyTO buildRulesExceptionWarrantyTO(
            final RelatedWarrantyRulesTO relatedWarrantyRulesTO,
            final Long relatedWarrantyRequestId) {

        return RelatedWarrantyRulesExceptionWarrantyTO.builder()
                .internalCode(relatedWarrantyRulesTO.getWarrantyInternalCode())
                .id(relatedWarrantyRulesTO.getId())
                .name(relatedWarrantyRulesTO.getWarrantyName())
                .cause(relatedWarrantyRequestId)
                .build();
    }

    private RelatedWarrantyRulesExceptionWarrantyTO buildRulesExceptionWarrantyTO(
            final RelatedWarrantyEntity relatedWarranty,
            final Long id,
            final Long relatedWarrantyRequestId) {

        return RelatedWarrantyRulesExceptionWarrantyTO.builder()
                .internalCode(relatedWarranty.getWarranty() == null
                        ? "" : relatedWarranty.getWarranty().getInternalCode())
                .id(id)
                .name(relatedWarranty.getWarranty() == null
                        ? "" : relatedWarranty.getWarranty().getName())
                .cause(relatedWarrantyRequestId)
                .build();
    }

    private RelatedWarrantyRulesTO buildWarrantyRulesTO(
            final RelatedWarrantyEntity relatedWarranty,
            final Long id) {

        return RelatedWarrantyRulesTO.builder()
                .warrantyInternalCode(relatedWarranty.getWarranty() == null
                        ? "" : relatedWarranty.getWarranty().getInternalCode())
                .id(id)
                .warrantyName(relatedWarranty.getWarranty() == null
                        ? "" : relatedWarranty.getWarranty().getName())
                .externalCode(relatedWarranty.getExternalCode())
                .build();
    }

    private boolean checkIsRemoving(final Long removedId, final List<Long> causes) {
        return removedId != null && causes.stream().anyMatch(id -> id.equals(removedId));
    }

}
