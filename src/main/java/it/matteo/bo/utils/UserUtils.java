package it.matteo.bo.utils;

import io.quarkus.oidc.runtime.OidcJwtCallerPrincipal;
import io.quarkus.runtime.Startup;
import io.quarkus.security.identity.SecurityIdentity;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.jose4j.jwt.MalformedClaimException;

@Startup
@ApplicationScoped
public class UserUtils {

    @Inject
    SecurityIdentity securityIdentity;

    /**
     * Gets the current logged username.
     *
     * @return the current logged username
     */
    public String getUsername() {
        if (securityIdentity.isAnonymous()) {
            return "anonymous";
        } else {
            String username = "";
            if (securityIdentity.getPrincipal() instanceof final OidcJwtCallerPrincipal principal) {
                try {
                    username = principal.getClaims()
                            .getClaimValue("username", String.class);
                } catch (final MalformedClaimException ignored) {
                }
            }
            return username;
        }
    }

}