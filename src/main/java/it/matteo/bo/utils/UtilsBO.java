package it.matteo.bo.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class UtilsBO {

    private static final Logger LOG = LoggerFactory.getLogger(UtilsBO.class);

    public final static ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);

    public static final String DEFAULT_ISO_DATE_FORMAT = "yyyy-MM-dd";

    private static final SimpleDateFormat DEFAULT_SQL_SEARCH_DATES = new SimpleDateFormat(DEFAULT_ISO_DATE_FORMAT);

    private final static String[] DATE_POSSIBLE_FORMATS = {
            "yyyy-MM-dd'T'HH:mm:ss.SSS",
            "yyyy-MM-dd'T'HH:mm:ss.SS",
            "yyyy-MM-dd'T'HH:mm:ss",
            "yyyy-MM-dd'T'HH:mm",
            "yyyy-MM-dd HH:mm",
            "yyyy-MM-dd HH:mm:ss",
            "yyyy-MM-dd HH:mm:ss.SSS",
            "yyyy-MM-dd HH:mm:ss.SS",
            "yyyy-MM-dd HH:mm:ss.SSSSSS",
            "yyyy/MM/dd'T'HH:mm:ss.SSS'Z'",
            "yyyy/MM/dd'T'HH:mm:ss.SSS",
            "yyyy/MM/dd'T'HH:mm:ss.SS",
            "yyyy/MM/dd'T'HH:mm:ss.SSS+0000",
            "yyyy/MM/dd'T'HH:mm:ss",
            "yyyy/MM/dd'T'HH:mm",
            DEFAULT_ISO_DATE_FORMAT,
            "yyyy/MM/dd",
            "dd-MM-yyyy",
            "dd/MM/yyyy",
            "yyyyMMdd",
            "yyyyddMM"
    };

    public static boolean isValidDate(final String str) {
        try {
            return DateUtils.parseDate(str, DATE_POSSIBLE_FORMATS) != null;
        } catch (final ParseException e) {
            return false;
        }
    }

    public static boolean isNumeric(final String str) {
        if (str == null) {
            return false;
        }
        try {
            final double d = Double.parseDouble(str);
        } catch (final NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static String parseDateSqlFilter(final String date) {
        try {
            return DEFAULT_SQL_SEARCH_DATES.format(
                    DateUtils.parseDate(date, DATE_POSSIBLE_FORMATS));
        } catch (final ParseException e) {
            LOG.error("While parsing date {}", date, e);
            return date;
        }
    }

}
