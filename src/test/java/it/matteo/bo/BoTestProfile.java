package it.matteo.bo;

import io.quarkus.test.junit.QuarkusTestProfile;

import java.util.Map;

public class BoTestProfile implements QuarkusTestProfile {

    /**
     * Returns additional config to be applied to the test. This
     * will override any existing config (including in application.properties),
     * however existing config will be merged with this (i.e. application.properties
     * config will still take effect, unless a specific config key has been overridden).
     */
    @Override
    public Map<String, String> getConfigOverrides() {
        return Map.of(
                "security.disable.authorization", "true"
        );
    }

    /**
     * Allows the default config profile to be overridden. This basically just sets the quarkus.test.profile system
     * property before the test is run.
     */
    @Override
    public String getConfigProfile() {
        return this.getClass().getSimpleName();
    }

}
