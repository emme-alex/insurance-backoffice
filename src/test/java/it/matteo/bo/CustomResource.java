package it.matteo.bo;

import io.quarkus.test.common.DevServicesContext;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import org.jetbrains.annotations.NotNull;
import org.testcontainers.containers.JdbcDatabaseContainer;
import org.testcontainers.containers.PostgreSQLContainer;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CustomResource implements QuarkusTestResourceLifecycleManager, DevServicesContext.ContextAware {

    private Optional<String> containerNetworkIdPrimary;
    private Optional<String> containerNetworkIdSecondary;
    private JdbcDatabaseContainer<?> containerPrimary;
    private JdbcDatabaseContainer<?> containerSecondary;

    @Override
    public void setIntegrationTestContext(final DevServicesContext context) {
        containerNetworkIdPrimary = context.containerNetworkId();
        containerNetworkIdSecondary = context.containerNetworkId();
    }

    @Override
    public Map<String, String> start() {
        // start a container making sure to call withNetworkMode() with the value of containerNetworkId if present
        containerPrimary = new PostgreSQLContainer<>("postgres")
                .withLogConsumer(outputFrame -> {
                })
                .withDatabaseName("primary")
                .withEnv("POSTGRES_ROOT_PASSWORD", "test")
                .withUsername("test")
                .withPassword("test");
        containerSecondary = new PostgreSQLContainer<>("postgres")
                .withLogConsumer(outputFrame -> {
                })
                .withDatabaseName("secondary")
                .withEnv("POSTGRES_ROOT_PASSWORD", "test")
                .withUsername("test")
                .withPassword("test");

        // apply the network to the container
        containerNetworkIdPrimary.ifPresent(containerPrimary::withNetworkMode);
        containerNetworkIdSecondary.ifPresent(containerPrimary::withNetworkMode);

        // start container before retrieving its URL or other properties
        containerPrimary.start();
        containerSecondary.start();

        String jdbcUrlPrimary = containerPrimary.getJdbcUrl();
        String jdbcUrlSecondary = containerSecondary.getJdbcUrl();
        if (containerNetworkIdPrimary.isPresent()) {
            // Replace hostname + port in the provided JDBC URL with the hostname of the Docker container
            // running PostgreSQL and the listening port.
            jdbcUrlPrimary = fixJdbcUrl(jdbcUrlPrimary, containerPrimary);
        }
        if (containerNetworkIdSecondary.isPresent()) {
            // Replace hostname + port in the provided JDBC URL with the hostname of the Docker container
            // running PostgreSQL and the listening port.
            jdbcUrlSecondary = fixJdbcUrl(jdbcUrlSecondary, containerSecondary);
        }

        // return a map containing the configuration the application needs to use the service
        return getProperties(jdbcUrlPrimary, jdbcUrlSecondary);
    }

    @NotNull
    private Map<String, String> getProperties(final String jdbcUrlPrimary, final String jdbcUrlSecondary) {
        final Map<String, String> properties = new HashMap<>();
        properties.put("quarkus.datasource.\"primary\".username", containerPrimary.getUsername());
        properties.put("quarkus.datasource.\"primary\".password", containerPrimary.getPassword());
        properties.put("quarkus.datasource.\"primary\".jdbc.url", jdbcUrlPrimary);
        properties.put("quarkus.datasource.\"secondary\".username", containerSecondary.getUsername());
        properties.put("quarkus.datasource.\"secondary\".password", containerSecondary.getPassword());
        properties.put("quarkus.datasource.\"secondary\".jdbc.url", jdbcUrlSecondary);
        properties.put("quarkus.hibernate-orm.\"primary\".datasource", "primary");
        properties.put("quarkus.hibernate-orm.\"primary\".packages", "it.matteo.bo.domain.primary");
        properties.put("quarkus.hibernate-orm.\"primary\".database.generation", "drop-and-create");
        properties.put("quarkus.hibernate-orm.\"primary\".database.generation.create-schemas", "true");
        properties.put("quarkus.hibernate-orm.\"secondary\".datasource", "secondary");
        properties.put("quarkus.hibernate-orm.\"secondary\".packages", "it.matteo.bo.domain.secondary");
        properties.put("quarkus.hibernate-orm.\"secondary\".database.generation", "drop-and-create");
        properties.put("quarkus.hibernate-orm.\"secondary\".database.generation.create-schemas", "true");
        return properties;
    }

    private String fixJdbcUrl(final String jdbcUrl, final JdbcDatabaseContainer<?> container) {
        // Part of the JDBC URL to replace
        final String hostPort = container.getHost() + ':' + container.getMappedPort(PostgreSQLContainer.POSTGRESQL_PORT);

        // Host/IP on the container network plus the unmapped port
        final String networkHostPort =
                container.getCurrentContainerInfo().getConfig().getHostName()
                        + ':'
                        + PostgreSQLContainer.POSTGRESQL_PORT;

        return jdbcUrl.replace(hostPort, networkHostPort);
    }

    @Override
    public void stop() {
        // close containers
        containerPrimary.close();
        containerSecondary.close();
    }

}
