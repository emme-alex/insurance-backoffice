package it.matteo.bo.rest;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.secondary.CatalogEntity;
import it.matteo.bo.repository.secondary.CatalogRepository;
import it.matteo.bo.service.secondary.CatalogService;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class CatalogResourceTest extends BaseTest {

    @InjectMock
    CatalogRepository catalogRepository;

    @InjectMock
    CatalogService catalogService;

    private final static String BASE_PATH = "/secondary/v1/catalogs";

    final CatalogEntity catalog = CatalogEntity.builder()
            .id(1L)
            .type("Pet")
            .company("Net Insurance")
            .revision("rev00")
            .enabled(true)
            .productCode("net-pet-silver")
            .asset(JsonNodeFactory.instance.objectNode())
            .build();

    @Test
    public void testGetAll() {
        final PanacheQuery<CatalogEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(catalog));

        when(catalogRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(catalogService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(catalog));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].productCode", is(catalog.getProductCode())
                );
    }

}
