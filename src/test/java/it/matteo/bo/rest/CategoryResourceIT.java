package it.matteo.bo.rest;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusIntegrationTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.BoTestProfile;
import it.matteo.bo.CustomResource;
import it.matteo.bo.domain.primary.CategoryEntity;
import it.matteo.bo.mapper.CategoryMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.CategoryBoundaryRequest;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;

@QuarkusIntegrationTest
@TestSecurity(authorizationEnabled = false)
@QuarkusTestResource(CustomResource.class)
@TestProfile(BoTestProfile.class)
public class CategoryResourceIT extends BaseTest {

    protected final static String BASE_PATH = "/api/v1/categories";

    final CategoryEntity category1 = CategoryEntity.builder()
            .id(1L)
            .name("Category 1")
            .externalCode("Category 1 code")
            .asset(JsonNodeFactory.instance.objectNode())
            .build();

    final CategoryEntity category2 = CategoryEntity.builder()
            .id(2L)
            .name("Category 2")
            .externalCode("Category 2 code")
            .asset(JsonNodeFactory.instance.objectNode())
            .build();

    @BeforeEach
    void setUp() {
        // create category 1
        final BaseRequestBoundary<CategoryBoundaryRequest> rq1 = new BaseRequestBoundary<>();
        rq1.setData(CategoryMapper.INSTANCE.entityToRequest(category1));
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq1)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(category1.getName()),
                        "data.externalCode", is(category1.getExternalCode())
                );
        // create category 2
        final BaseRequestBoundary<CategoryBoundaryRequest> rq2 = new BaseRequestBoundary<>();
        rq2.setData(CategoryMapper.INSTANCE.entityToRequest(category2));
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq2)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(category2.getName()),
                        "data.externalCode", is(category2.getExternalCode())
                );
    }

    @Test
    public void testGetAllPaginatedFiltered() {
        given()
                .contentType(ContentType.JSON)
                .when()
                .queryParam("page", "0")
                .queryParam("perPage", "1")
                .queryParam("sort", "id")
                .queryParam("order", "asc")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].name", is(category1.getName()),
                        "data[0].externalCode", is(category1.getExternalCode())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .queryParam("page", "0")
                .queryParam("perPage", "1")
                .queryParam("sort", "id")
                .queryParam("order", "desc")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].name", is(category2.getName()),
                        "data[0].externalCode", is(category2.getExternalCode())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .queryParam("filter",
                        "{\"name\":\"" + category1.getName() + "\"}")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].name", is(category1.getName()),
                        "data[0].externalCode", is(category1.getExternalCode())
                );

        // test case insensitivity
        given()
                .contentType(ContentType.JSON)
                .when()
                .queryParam("filter",
                        "{\"name\":\"" + category1.getName().toUpperCase() + "\"}")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].name", is(category1.getName()),
                        "data[0].externalCode", is(category1.getExternalCode())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .queryParam("filter",
                        "{\"name\":\"" + category1.getName() + "\",\"externalCode\":\"" + category1.getExternalCode() + "\"}")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].name", is(category1.getName()),
                        "data[0].externalCode", is(category1.getExternalCode())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .queryParam("filter",
                        "{\"id\":" + category1.getId().intValue() + "}")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(category1.getId().intValue()),
                        "data[0].name", is(category1.getName()),
                        "data[0].externalCode", is(category1.getExternalCode())
                );
    }

}
