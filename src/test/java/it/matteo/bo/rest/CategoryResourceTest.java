package it.matteo.bo.rest;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.common.collect.Sets;
import io.quarkus.hibernate.orm.PersistenceUnit;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.CategoryEntity;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.domain.primary.ProductImagesEntity;
import it.matteo.bo.mapper.CategoryMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.CategoryBoundaryRequest;
import it.matteo.bo.repository.primary.CategoryRepository;
import it.matteo.bo.repository.primary.ProductRepository;
import it.matteo.bo.service.primary.CategoryService;
import jakarta.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class CategoryResourceTest extends BaseTest {

    @InjectMock
    CategoryRepository categoryRepository;

    @InjectMock
    ProductRepository productRepository;

    @InjectMock
    CategoryService categoryService;

    @InjectMock
    @PersistenceUnit("primary")
    Session session;

    @InjectMock
    @PersistenceUnit("primary")
    EntityManager entityManager;

    private final static String BASE_PATH = "/api/v1/categories";

    final ProductEntity product = ProductEntity.builder()
            .id(1L)
            .code("net-pet-gold")
            .image(ProductImagesEntity.builder()
                    .id(1L)
                    .images(JsonNodeFactory.instance.objectNode()).build())
            .build();

    final CategoryEntity category = CategoryEntity.builder()
            .id(1L)
            .name("test_category")
            .externalCode("test_category_code")
            .asset(JsonNodeFactory.instance.objectNode())
            .products(Sets.newHashSet(product))
            .build();

    final CategoryEntity newCategory = CategoryEntity.builder()
            .id(2L)
            .name("new test category")
            .externalCode("new test category code")
            .asset(JsonNodeFactory.instance.objectNode())
            .products(Sets.newHashSet(product))
            .build();

    final ProductEntity newAddedProduct = ProductEntity.builder()
            .id(2L)
            .code("net-pet-silver")
            .image(ProductImagesEntity.builder()
                    .id(2L)
                    .images(JsonNodeFactory.instance.objectNode()).build())
            .build();

    final CategoryEntity updatedCategory = CategoryEntity.builder()
            .id(2L)
            .name(newCategory.getName())
            .externalCode("updated_category_code")
            .asset(JsonNodeFactory.instance.objectNode())
            .products(Sets.newHashSet(newAddedProduct))
            .build();

    @BeforeEach
    void setUp() {
        // necessary for both CREATE and UPDATE
        when(productRepository.findByIdOptional(any(Long.class)))
                .thenReturn(Optional.of(product));
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<CategoryEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(category));

        when(categoryRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(categoryService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(category));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].name", is(category.getName())
                );
    }

    @Test
    public void testGet() {
        when(categoryService.read(1L))
                .thenReturn(Optional.of(category));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(category.getName())
                );
    }

    @Test
    public void testCreate() {
        when(categoryService.create(any(CategoryEntity.class)))
                .thenReturn(newCategory);

        when(categoryService.read(2L))
                .thenReturn(Optional.of(newCategory));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<CategoryBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(CategoryMapper.INSTANCE.entityToRequest(newCategory));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newCategory.getName())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newCategory.getName())
                );
    }

    @Test
    public void testUpdate() {
        when(categoryService.update(any(Long.class), any(CategoryEntity.class)))
                .thenReturn(updatedCategory);
        when(entityManager.merge(any(CategoryEntity.class)))
                .thenReturn(updatedCategory);

        when(categoryService.read(2L))
                .thenReturn(Optional.of(updatedCategory));

        final BaseRequestBoundary<CategoryBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(CategoryMapper.INSTANCE.entityToRequest(updatedCategory));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .put(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newCategory.getName()),
                        "data.externalCode", is(updatedCategory.getExternalCode()),
                        "data.products[0].code", is(newAddedProduct.getCode())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newCategory.getName()),
                        "data.externalCode", is(updatedCategory.getExternalCode()),
                        "data.products[0].code", is(newAddedProduct.getCode())
                );
    }

}
