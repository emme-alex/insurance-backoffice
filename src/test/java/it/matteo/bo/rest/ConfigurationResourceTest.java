package it.matteo.bo.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.ConfigurationEntity;
import it.matteo.bo.repository.primary.ConfigurationRepository;
import it.matteo.bo.service.primary.ConfigurationService;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class ConfigurationResourceTest extends BaseTest {

    @InjectMock
    ConfigurationRepository configurationRepository;

    @InjectMock
    ConfigurationService configurationService;

    private final static String BASE_PATH = "/api/v1/configurations";

    private final ConfigurationEntity configuration = ConfigurationEntity.builder()
            .id(1L)
            .configuration(
                    OBJECT_MAPPER.readTree("{\"s3Info\": {\"bucket\": \"test\" } }")
            )
            .createdBy("admin")
            .updatedBy("admin")
            .build();

    public ConfigurationResourceTest() throws JsonProcessingException {
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<ConfigurationEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(configuration));

        when(configurationRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(configurationService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(configuration));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].configuration", equalTo(
                                new JsonPath(configuration.getConfiguration().toString()).getMap(""))
                );
    }

    @Test
    public void testGet() {
        when(configurationService.read(1L))
                .thenReturn(Optional.of(configuration));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.configuration", equalTo(
                                new JsonPath(configuration.getConfiguration().toString()).getMap(""))
                );
    }

}
