package it.matteo.bo.rest;

import io.quarkus.hibernate.orm.PersistenceUnit;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.CouponEntity;
import it.matteo.bo.domain.primary.PromotionEntity;
import it.matteo.bo.mapper.CouponMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.CouponBoundaryRequest;
import it.matteo.bo.repository.primary.CouponRepository;
import it.matteo.bo.repository.primary.PromotionRepository;
import it.matteo.bo.service.primary.CouponService;
import jakarta.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class CouponResourceTest extends BaseTest {

    @InjectMock
    CouponRepository couponRepository;

    @InjectMock
    PromotionRepository promotionRepository;

    @InjectMock
    CouponService couponService;

    @InjectMock
    @PersistenceUnit("primary")
    Session session;

    @InjectMock
    @PersistenceUnit("primary")
    EntityManager entityManager;

    private final static String BASE_PATH = "/api/v1/coupons";

    final PromotionEntity promotion = PromotionEntity.builder()
            .id(1L)
            .promotionCode("test_promotion_code")
            .description("test_promotion_desc")
            .build();

    final CouponEntity coupon = CouponEntity.builder()
            .id(1L)
            .couponCode("test_coupon_code")
            .promotion(promotion)
            .type("single")
            .maxUsage(100)
            .build();

    final CouponEntity newCoupon = CouponEntity.builder()
            .id(2L)
            .couponCode("test_new_coupon_code")
            .promotion(coupon.getPromotion())
            .type("multiple")
            .maxUsage(200)
            .build();

    final CouponEntity updatedCoupon = CouponEntity.builder()
            .id(2L)
            .couponCode(newCoupon.getCouponCode())
            .promotion(newCoupon.getPromotion())
            .type("single")
            .maxUsage(newCoupon.getMaxUsage())
            .build();

    @BeforeEach
    void setUp() {
        // necessary for both CREATE and UPDATE
        when(promotionRepository.findByIdOptional(any(Long.class)))
                .thenReturn(Optional.of(promotion));
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<CouponEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(coupon));

        when(couponRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(couponService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(coupon));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].promotion", is(not(empty())),
                        "data[0].couponCode", is(coupon.getCouponCode()),
                        "data[0].maxUsage", is(coupon.getMaxUsage()),
                        "data[0].type", is(coupon.getType())
                );
    }

    @Test
    public void testGet() {
        when(couponService.read(1L))
                .thenReturn(Optional.of(coupon));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.promotion", is(not(empty())),
                        "data.couponCode", is(coupon.getCouponCode()),
                        "data.maxUsage", is(coupon.getMaxUsage()),
                        "data.type", is(coupon.getType())
                );
    }

    @Test
    public void testGetByPromotionCode() {
        when(couponService.readByCode(coupon.getCouponCode(), false))
                .thenReturn(Optional.of(coupon));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byCode/" + coupon.getCouponCode())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.promotion", is(not(empty())),
                        "data.couponCode", is(coupon.getCouponCode()),
                        "data.maxUsage", is(coupon.getMaxUsage()),
                        "data.type", is(coupon.getType())
                );
    }

    @Test
    public void testCreateSingle() {
        when(couponService.createSingle(any(CouponEntity.class)))
                .thenReturn(newCoupon);

        when(couponService.read(2L))
                .thenReturn(Optional.of(newCoupon));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<CouponBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(CouponMapper.INSTANCE.entityToRequest(newCoupon));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH + "/single")
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.promotion", is(not(empty())),
                        "data.couponCode", is(newCoupon.getCouponCode()),
                        "data.maxUsage", is(newCoupon.getMaxUsage()),
                        "data.type", is(newCoupon.getType())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.promotion", is(not(empty())),
                        "data.couponCode", is(newCoupon.getCouponCode()),
                        "data.maxUsage", is(newCoupon.getMaxUsage()),
                        "data.type", is(newCoupon.getType())
                );
    }

    @Test
    public void testUpdate() {
        when(couponService.update(any(Long.class), any(CouponEntity.class)))
                .thenReturn(updatedCoupon);
        when(entityManager.merge(any(CouponEntity.class)))
                .thenReturn(updatedCoupon);

        when(couponService.read(2L))
                .thenReturn(Optional.of(updatedCoupon));

        final BaseRequestBoundary<CouponBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(CouponMapper.INSTANCE.entityToRequest(updatedCoupon));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .put(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.promotion", is(not(empty())),
                        "data.couponCode", is(updatedCoupon.getCouponCode()),
                        "data.maxUsage", is(updatedCoupon.getMaxUsage()),
                        "data.type", is(updatedCoupon.getType())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.promotion", is(not(empty())),
                        "data.couponCode", is(updatedCoupon.getCouponCode()),
                        "data.maxUsage", is(updatedCoupon.getMaxUsage()),
                        "data.type", is(updatedCoupon.getType())
                );
    }

}
