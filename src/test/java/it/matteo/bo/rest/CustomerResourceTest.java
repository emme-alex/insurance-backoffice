package it.matteo.bo.rest;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.CustomerEntity;
import it.matteo.bo.service.primary.CustomerService;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class CustomerResourceTest extends BaseTest {

    @InjectMock
    CustomerService customerService;

    private final static String BASE_PATH = "/api/v1/customers";

    final CustomerEntity customer = CustomerEntity.builder()
            .id(1L)
            .name("test customer")
            .surname("test customer surname")
            .build();

    @Test
    public void testGetAll() {
        final PanacheQuery<CustomerEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(customer));

        when(customerService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(customer));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].name", is(customer.getName()),
                        "data[0].surname", is(customer.getSurname())
                );
    }

    @Test
    public void testGet() {
        when(customerService.read(1L))
                .thenReturn(Optional.of(customer));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.name", is(customer.getName()),
                        "data.surname", is(customer.getSurname())
                );
    }

}
