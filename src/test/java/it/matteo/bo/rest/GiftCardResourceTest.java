package it.matteo.bo.rest;

import io.quarkus.hibernate.orm.PersistenceUnit;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.GiftCardEntity;
import it.matteo.bo.domain.primary.UtmSourceEntity;
import it.matteo.bo.mapper.GiftCardMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.GiftCardBoundaryRequest;
import it.matteo.bo.repository.primary.GiftCardRepository;
import it.matteo.bo.service.primary.GiftCardService;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class GiftCardResourceTest extends BaseTest {

    @InjectMock
    GiftCardRepository giftCardRepository;

    @InjectMock
    GiftCardService giftCardService;

    @InjectMock
    @PersistenceUnit("primary")
    Session session;

    private final static String BASE_PATH = "/api/v1/giftcards";

    final UtmSourceEntity utmSource = UtmSourceEntity.builder()
            .id(1L)
            .name("test_name")
            .code("test_code")
            .build();

    final GiftCardEntity giftCard = GiftCardEntity.builder()
            .id(1L)
            .uniqueCardName("test_unique_card_name")
            .cardName("test_card_name")
            .cardValue(BigDecimal.valueOf(50.0))
            .startDate(LocalDateTime.now())
            .endDate(LocalDateTime.now().plusDays(30))
            .utmSource(utmSource)
            .build();

    @BeforeEach
    void setUp() {
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<GiftCardEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(giftCard));

        when(giftCardRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(giftCardService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(giftCard));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].uniqueCardName", is(not(empty())),
                        "data[0].cardName", is(giftCard.getCardName()),
                        "data[0].cardValue", is(giftCard.getCardValue().floatValue()),
                        "data[0].startDate", is(notNullValue()),
                        "data[0].endDate", is(notNullValue()),
                        "data[0].utmSource", is(not(empty()))
                );
    }

    @Test
    public void testGet() {
        when(giftCardService.read(1L))
                .thenReturn(Optional.of(giftCard));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.uniqueCardName", is(not(empty())),
                        "data.cardName", is(giftCard.getCardName()),
                        "data.cardValue", is(giftCard.getCardValue().floatValue()),
                        "data.startDate", is(notNullValue()),
                        "data.endDate", is(notNullValue()),
                        "data.utmSource", is(not(empty()))
                );
    }

    @Test
    public void testCreate() {
        when(giftCardService.create(any(GiftCardEntity.class)))
                .thenReturn(giftCard);

        when(giftCardService.read(1L))
                .thenReturn(Optional.of(giftCard));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<GiftCardBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(GiftCardMapper.INSTANCE.entityToRequest(giftCard));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.uniqueCardName", is(not(empty())),
                        "data.cardName", is(giftCard.getCardName()),
                        "data.cardValue", is(giftCard.getCardValue().floatValue()),
                        "data.startDate", is(notNullValue()),
                        "data.endDate", is(notNullValue()),
                        "data.utmSource", is(not(empty()))
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.uniqueCardName", is(not(empty())),
                        "data.cardName", is(giftCard.getCardName()),
                        "data.cardValue", is(giftCard.getCardValue().floatValue()),
                        "data.startDate", is(notNullValue()),
                        "data.endDate", is(notNullValue()),
                        "data.utmSource", is(not(empty()))
                );
    }

}
