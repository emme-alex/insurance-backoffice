package it.matteo.bo.rest;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.OrderAnagStatesEntity;
import it.matteo.bo.domain.primary.OrderEntity;
import it.matteo.bo.domain.primary.PacketEntity;
import it.matteo.bo.domain.primary.PolicyAnagStatesEntity;
import it.matteo.bo.domain.primary.PolicyEntity;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.repository.primary.PolicyRepository;
import it.matteo.bo.service.primary.OrderService;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class OrderResourceTest extends BaseTest {

    @InjectMock
    OrderService orderService;

    @InjectMock
    PolicyRepository policyRepo;

    private final static String BASE_PATH = "/api/v1/orders";

    final ProductEntity product = ProductEntity.builder()
            .id(1L)
            .build();

    final OrderEntity order = OrderEntity.builder()
            .id(1L)
            .orderCode("test_order_code")
            .brokerId(1L)
            .anagState(OrderAnagStatesEntity.builder().id(1L).state("Deleted").build())
            .packet(PacketEntity.builder().id(1L).build())
            .product(product)
            .build();

    final PolicyEntity policy = PolicyEntity.builder()
            .id(1L)
            .policyCode("test policy code")
            .order(order)
            .state(PolicyAnagStatesEntity.builder()
                    .id(1L)
                    .state("Active")
                    .updatedAt(LocalDateTime.now())
                    .createdAt(LocalDateTime.now())
                    .createdBy("admin")
                    .updatedBy("admin")
                    .build())
            .product(product)
            .build();

    final OrderEntity order2 = OrderEntity.builder()
            .id(2L)
            .orderCode("another")
            .brokerId(2L)
            .anagState(OrderAnagStatesEntity.builder().id(1L).state("Deleted").build())
            .packet(PacketEntity.builder().id(2L).build())
            .product(ProductEntity.builder().id(2L).build())
            .build();

    private final OrderEntity updatedOrder = OrderEntity.builder()
            .id(1L)
            .orderCode("updated_test_order_code")
            .brokerId(order.getBrokerId())
            .anagState(OrderAnagStatesEntity.builder().id(3L).state("Confirmed").build())
            .build();

    @BeforeEach
    void setUp() {

    }

    @Test
    public void testGetAll() {
        final PanacheQuery<OrderEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(order));

        when(orderService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(order));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].anagState.id", is(order.getAnagState().getId().intValue()),
                        "data[0].packet.id", is(order.getPacket().getId().intValue()),
                        "data[0].product.id", is(order.getProduct().getId().intValue()),
                        "data[0].orderCode", is(order.getOrderCode())
                );
    }

    @Test
    public void testGet() {
        when(orderService.read(1L))
                .thenReturn(Optional.of(order));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.anagState.id", is(order.getAnagState().getId().intValue()),
                        "data.packet.id", is(order.getPacket().getId().intValue()),
                        "data.product.id", is(order.getProduct().getId().intValue()),
                        "data.orderCode", is(order.getOrderCode())
                );
    }

    @Test
    public void testGetByPolicyId() {
        when(policyRepo.findByIdOptional(1L))
                .thenReturn(Optional.of(policy));

        when(orderService.readOrderByPolicyId(1L))
                .thenReturn(Optional.of(order));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byPolicyId/" + policy.getId().intValue())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].anagState.id", is(order.getAnagState().getId().intValue()),
                        "data[0].packet.id", is(order.getPacket().getId().intValue()),
                        "data[0].product.id", is(order.getProduct().getId().intValue()),
                        "data[0].orderCode", is(order.getOrderCode())
                );
    }

    @Test
    public void testGetByCode() {
        when(orderService.readOrderByCode(any()))
                .thenReturn(Optional.of(order));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byCode/" + order.getOrderCode())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.anagState.id", is(order.getAnagState().getId().intValue()),
                        "data.packet.id", is(order.getPacket().getId().intValue()),
                        "data.product.id", is(order.getProduct().getId().intValue()),
                        "data.orderCode", is(order.getOrderCode())
                );
    }

}
