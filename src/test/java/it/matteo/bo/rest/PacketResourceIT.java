package it.matteo.bo.rest;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusIntegrationTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.BoTestProfile;
import it.matteo.bo.CustomResource;
import it.matteo.bo.domain.primary.PacketEntity;
import it.matteo.bo.mapper.PacketMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.PacketBoundaryRequest;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;

@QuarkusIntegrationTest
@TestSecurity(authorizationEnabled = false)
@QuarkusTestResource(CustomResource.class)
@TestProfile(BoTestProfile.class)
public class PacketResourceIT extends BaseTest {

    private final static String BASE_PATH = "/api/v1/packets";

    final PacketEntity packet1 = PacketEntity.builder()
            .id(1L)
            .name("test_packet 1")
            .sku("test_sku 1")
            .brokerId(1L)
            .build();
    final PacketEntity packet2 = PacketEntity.builder()
            .id(2L)
            .name("test_packet 2")
            .sku("test_sku 2")
            .brokerId(2L)
            .build();

    @BeforeEach
    void setUp() {
        final BaseRequestBoundary<PacketBoundaryRequest> rq1 = new BaseRequestBoundary<>();
        rq1.setData(PacketMapper.INSTANCE.entityToRequest(packet1));
        given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq1)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.broker", is(not(empty())),
                        "data.name", is(packet1.getName()),
                        "data.sku", is(packet1.getSku()));

        final BaseRequestBoundary<PacketBoundaryRequest> rq2 = new BaseRequestBoundary<>();
        rq2.setData(PacketMapper.INSTANCE.entityToRequest(packet2));
        given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq2)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.broker", is(not(empty())),
                        "data.name", is(packet2.getName()),
                        "data.sku", is(packet2.getSku()));
    }

    @Test
    public void testGetAllPaginatedFiltered() {
        given().contentType(ContentType.JSON)
                .when()
                .queryParam("page", "0")
                .queryParam("perPage", "1")
                .queryParam("sort", "id")
                .queryParam("order", "asc")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1), "data[0].id", is(not(empty())),
                        "data[0].broker", is(not(empty())),
                        "data[0].name", is(packet1.getName()),
                        "data[0].sku", is(packet1.getSku()));

        given().contentType(ContentType.JSON)
                .when()
                .queryParam("page", "0")
                .queryParam("perPage", "1")
                .queryParam("sort", "id")
                .queryParam("order", "desc")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].broker", is(not(empty())),
                        "data[0].name", is(packet2.getName()),
                        "data[0].sku", is(packet2.getSku()));

        given().contentType(ContentType.JSON)
                .when()
                .queryParam("filter", "{\"name\":\"" + packet1.getName() + "\"}")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].broker", is(not(empty())),
                        "data[0].name", is(packet1.getName()),
                        "data[0].sku", is(packet1.getSku()));

        given().contentType(ContentType.JSON)
                .when()
                .queryParam("filter", "{\"id\":" + packet1.getId().intValue() + "}")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].broker", is(not(empty())),
                        "data[0].name", is(packet1.getName()), "data[0].sku", is(packet1.getSku()));

        given().contentType(ContentType.JSON)
                .when()
                .queryParam("filter", "{\"brokerId\":" + 1 + "}")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1));
    }

}
