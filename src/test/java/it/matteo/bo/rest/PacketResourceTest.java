package it.matteo.bo.rest;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.common.collect.Sets;
import io.quarkus.hibernate.orm.PersistenceUnit;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.CategoryEntity;
import it.matteo.bo.domain.primary.PacketEntity;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.domain.primary.ProductImagesEntity;
import it.matteo.bo.domain.primary.RelatedWarrantyEntity;
import it.matteo.bo.domain.primary.WarrantyEntity;
import it.matteo.bo.mapper.PacketMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.PacketBoundaryRequest;
import it.matteo.bo.repository.primary.CategoryRepository;
import it.matteo.bo.repository.primary.PacketRepository;
import it.matteo.bo.repository.primary.ProductRepository;
import it.matteo.bo.repository.primary.WarrantyRepository;
import it.matteo.bo.service.primary.PacketService;
import jakarta.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class PacketResourceTest extends BaseTest {

    @InjectMock
    PacketRepository packetRepository;

    @InjectMock
    ProductRepository productRepository;

    @InjectMock
    WarrantyRepository warrantyRepository;

    @InjectMock
    CategoryRepository categoryRepository;

    @InjectMock
    PacketService packetService;

    @InjectMock
    @PersistenceUnit("primary")
    Session session;

    @InjectMock
    @PersistenceUnit("primary")
    EntityManager entityManager;

    private final static String BASE_PATH = "/api/v1/packets";

    final ProductEntity product = ProductEntity.builder()
            .id(1L)
            .code("net-pet-gold")
            .image(ProductImagesEntity.builder()
                    .id(1L)
                    .images(JsonNodeFactory.instance.objectNode()).build())
            .build();

    final CategoryEntity category = CategoryEntity.builder()
            .id(1L)
            .name("test_category")
            .externalCode("test_category_code")
            .asset(JsonNodeFactory.instance.objectNode())
            .products(Sets.newHashSet(product))
            .build();

    final WarrantyEntity warranty = WarrantyEntity.builder()
            .name("test_warranty")
            .build();

    final RelatedWarrantyEntity relatedWarranty = RelatedWarrantyEntity.builder()
            .category(category)
            .parent(RelatedWarrantyEntity.builder().id(1L).build())
            .warranty(warranty)
            .build();

    final PacketEntity packet = PacketEntity.builder()
            .id(1L)
            .name("test_packet")
            .sku("test_sku")
            .brokerId(1L)
            .product(product)
            .warranties(Sets.newHashSet(relatedWarranty))
            .build();

    private final PacketEntity newPacket = PacketEntity.builder()
            .id(2L)
            .name("test_new_packet")
            .sku("test_new_sku")
            .brokerId(2L)
            .product(product)
            .warranties(Sets.newHashSet(relatedWarranty))
            .build();

    private final PacketEntity newPacketNoSpecificFields = PacketEntity.builder()
            .id(2L)
            .name(newPacket.getName())
            .sku(newPacket.getSku())
            .build();

    private final PacketEntity updatedPacket = PacketEntity.builder()
            .id(2L)
            .name(newPacket.getName())
            .sku("test_updated_sku")
            .brokerId(packet.getBrokerId())
            .product(packet.getProduct())
            .warranties(packet.getWarranties())
            .build();

    @BeforeEach
    void setUp() {
        // necessary for both CREATE and UPDATE
        when(productRepository.findByIdOptional(any(Long.class)))
                .thenReturn(Optional.of(product));
        when(warrantyRepository.findByIdOptional(any(Long.class)))
                .thenReturn(Optional.of(warranty));
        when(categoryRepository.findByIdOptional(any(Long.class)))
                .thenReturn(Optional.of(category));
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<PacketEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(packet));

        when(packetRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(packetService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(packet));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].broker", is(not(empty())),
                        "data[0].warranties", is(not(empty())),
                        "data[0].name", is(packet.getName()),
                        "data[0].product.id", is(packet.getProduct().getId().intValue()),
                        "data[0].sku", is(packet.getSku())
                );
    }

    @Test
    public void testGet() {
        when(packetService.read(1L))
                .thenReturn(Optional.of(packet));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.broker", is(not(empty())),
                        "data.warranties", is(not(empty())),
                        "data.name", is(packet.getName()),
                        "data.product.id", is(packet.getProduct().getId().intValue()),
                        "data.sku", is(packet.getSku())
                );
    }

    @Test
    public void testGetByProductId() {
        when(packetService.readByProductId(product.getId()))
                .thenReturn(List.of(packet));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byProductId/" + product.getId().intValue())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].broker", is(not(empty())),
                        "data[0].warranties", is(not(empty())),
                        "data[0].name", is(packet.getName()),
                        "data[0].product.id", is(packet.getProduct().getId().intValue()),
                        "data[0].sku", is(packet.getSku())
                );
    }

    @Test
    public void testCreate() {
        when(packetService.create(any(PacketEntity.class)))
                .thenReturn(newPacket);

        when(packetService.read(2L))
                .thenReturn(Optional.of(newPacket));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<PacketBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(PacketMapper.INSTANCE.entityToRequest(newPacket));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newPacket.getName()),
                        "data.sku", is(newPacket.getSku()),
                        "data.brokerId", is(not(empty())),
                        "data.warranties[0].id", is(not(empty())),
                        "data.product.id", is(newPacket.getProduct().getId().intValue())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newPacket.getName()),
                        "data.sku", is(newPacket.getSku()),
                        "data.brokerId", is(not(empty())),
                        "data.warranties[0].id", is(not(empty())),
                        "data.product.id", is(newPacket.getProduct().getId().intValue())
                );
    }

    @Test
    public void testUpdate() {
        when(packetService.update(any(Long.class), any(PacketEntity.class)))
                .thenReturn(updatedPacket);
        when(entityManager.merge(any(PacketEntity.class)))
                .thenReturn(updatedPacket);

        when(packetService.read(2L))
                .thenReturn(Optional.of(updatedPacket));

        final BaseRequestBoundary<PacketBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(PacketMapper.INSTANCE.entityToRequest(updatedPacket));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .put(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(updatedPacket.getName()),
                        "data.sku", is(updatedPacket.getSku()),
                        "data.brokerId", is(not(empty())),
                        "data.warranties[0].id", is(not(empty())),
                        "data.product.id", is(updatedPacket.getProduct().getId().intValue())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(updatedPacket.getName()),
                        "data.sku", is(updatedPacket.getSku()),
                        "data.brokerId", is(not(empty())),
                        "data.warranties[0].id", is(not(empty())),
                        "data.product.id", is(updatedPacket.getProduct().getId().intValue())
                );
    }

    @Test
    public void testCreateNoSpecificFields() {
        when(packetService.create(any(PacketEntity.class)))
                .thenReturn(newPacketNoSpecificFields);

        when(packetService.read(2L))
                .thenReturn(Optional.of(newPacketNoSpecificFields));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<PacketBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(PacketMapper.INSTANCE.entityToRequest(newPacketNoSpecificFields));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newPacket.getName()),
                        "data.sku", is(newPacket.getSku()),
                        "data.brokerId", is(nullValue()),
                        "data.warranties", is(nullValue()),
                        "data.product", is(nullValue())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newPacket.getName()),
                        "data.sku", is(newPacket.getSku()),
                        "data.brokerId", is(nullValue()),
                        "data.warranties", is(nullValue()),
                        "data.product", is(nullValue())
                );
    }

}
