package it.matteo.bo.rest;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusIntegrationTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.BoTestProfile;
import it.matteo.bo.CustomResource;
import it.matteo.bo.domain.primary.PaymentMethodEntity;
import it.matteo.bo.mapper.PaymentMethodMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.PaymentMethodBoundaryRequest;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;

@QuarkusIntegrationTest
@TestSecurity(authorizationEnabled = false)
@QuarkusTestResource(CustomResource.class)
@TestProfile(BoTestProfile.class)
public class PaymentMethodResourceIT extends BaseTest {

    private final static String BASE_PATH = "/api/v1/payment_methods";

    final PaymentMethodEntity paymentMethod1 = PaymentMethodEntity.builder()
            .id(1L)
            .paymentMethod("Store Credit 1")
            .active(true)
            .paymentMethodType("Store Credit 1")
            .build();
    final PaymentMethodEntity paymentMethod2 = PaymentMethodEntity.builder()
            .id(2L)
            .paymentMethod("Store Credit 2")
            .active(true)
            .paymentMethodType("Store Credit 2")
            .build();

    @BeforeEach
    void setUp() {
        final BaseRequestBoundary<PaymentMethodBoundaryRequest> rq1 = new BaseRequestBoundary<>();
        rq1.setData(PaymentMethodMapper.INSTANCE.entityToRequest(paymentMethod1));
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq1)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.paymentMethod", is(paymentMethod1.getPaymentMethod()),
                        "data.active", is(true), "data.paymentMethodType", is(paymentMethod1.getPaymentMethod()));

        final BaseRequestBoundary<PaymentMethodBoundaryRequest> rq2 = new BaseRequestBoundary<>();
        rq2.setData(PaymentMethodMapper.INSTANCE.entityToRequest(paymentMethod2));
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON).when()
                .body(rq2)
                .post(BASE_PATH).then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.paymentMethod", is(paymentMethod2.getPaymentMethod()),
                        "data.active", is(true),
                        "data.paymentMethodType", is(paymentMethod2.getPaymentMethod()));
    }

    @Test
    public void testGetAllPaginatedFiltered() {
        given()
                .contentType(ContentType.JSON)
                .when().queryParam("page", "0")
                .queryParam("perPage", "1")
                .queryParam("sort", "id")
                .queryParam("order", "asc")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].paymentMethod", is(paymentMethod1.getPaymentMethod()));

        given()
                .contentType(ContentType.JSON)
                .when().queryParam("page", "0")
                .queryParam("perPage", "1")
                .queryParam("sort", "id")
                .queryParam("order", "desc")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].paymentMethod", is(paymentMethod2.getPaymentMethod()));

        given()
                .contentType(ContentType.JSON)
                .when()
                .queryParam("filter", "{\"paymentMethod\":\"" + paymentMethod1.getPaymentMethod() + "\"}")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].paymentMethod", is(paymentMethod1.getPaymentMethod()));

        given()
                .contentType(ContentType.JSON)
                .when()
                .queryParam("filter", "{\"id\":" + paymentMethod1.getId().intValue() + "}")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK).body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(paymentMethod1.getId().intValue()),
                        "data[0].paymentMethod", is(paymentMethod1.getPaymentMethod()));

        given()
                .contentType(ContentType.JSON)
                .when()
                .queryParam("filter", "{\"active\":true}")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(2));
    }

}