package it.matteo.bo.rest;

import io.quarkus.hibernate.orm.PersistenceUnit;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.PaymentMethodEntity;
import it.matteo.bo.mapper.PaymentMethodMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.PaymentMethodBoundaryRequest;
import it.matteo.bo.repository.primary.PaymentMethodRepository;
import it.matteo.bo.service.primary.PaymentMethodService;
import jakarta.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class PaymentMethodResourceTest extends BaseTest {

    @InjectMock
    PaymentMethodRepository paymentMethodRepository;

    @InjectMock
    PaymentMethodService paymentMethodService;

    @InjectMock
    @PersistenceUnit("primary")
    Session session;

    @InjectMock
    @PersistenceUnit("primary")
    EntityManager entityManager;

    private final static String BASE_PATH = "/api/v1/payment_methods";

    final PaymentMethodEntity paymentMethod = PaymentMethodEntity.builder()
            .id(1L)
            .paymentMethod("Store Credit")
            .active(true)
            .paymentMethodType("Store Credit")
            .build();

    private final PaymentMethodEntity newPaymentMethod = PaymentMethodEntity.builder()
            .id(2L)
            .paymentMethod("new payment method")
            .active(true)
            .paymentMethodType(paymentMethod.getPaymentMethodType())
            .build();

    private final PaymentMethodEntity updatedPaymentMethod = PaymentMethodEntity.builder()
            .id(newPaymentMethod.getId())
            .paymentMethod("updated payment method")
            .active(true)
            .paymentMethodType(paymentMethod.getPaymentMethodType())
            .build();

    @BeforeEach
    void setUp() {
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<PaymentMethodEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(paymentMethod));

        when(paymentMethodRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(paymentMethodService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(paymentMethod));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].paymentMethod", is(paymentMethod.getPaymentMethod()),
                        "data[0].paymentMethodType", is(paymentMethod.getPaymentMethodType()),
                        "data[0].active", is(paymentMethod.isActive())
                );
    }

    @Test
    public void testGet() {
        when(paymentMethodService.read(1L))
                .thenReturn(Optional.of(paymentMethod));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.paymentMethod", is(paymentMethod.getPaymentMethod()),
                        "data.paymentMethodType", is(paymentMethod.getPaymentMethodType()),
                        "data.active", is(paymentMethod.isActive())
                );
    }

    @Test
    public void testCreate() {
        when(paymentMethodService.create(any(PaymentMethodEntity.class)))
                .thenReturn(newPaymentMethod);

        when(paymentMethodService.read(2L))
                .thenReturn(Optional.of(newPaymentMethod));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<PaymentMethodBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(PaymentMethodMapper.INSTANCE.entityToRequest(newPaymentMethod));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.paymentMethod", is(newPaymentMethod.getPaymentMethod()),
                        "data.paymentMethodType", is(newPaymentMethod.getPaymentMethodType()),
                        "data.active", is(newPaymentMethod.isActive())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.paymentMethod", is(newPaymentMethod.getPaymentMethod()),
                        "data.paymentMethodType", is(newPaymentMethod.getPaymentMethodType()),
                        "data.active", is(newPaymentMethod.isActive())
                );
    }

    @Test
    public void testUpdate() {
        when(paymentMethodService.update(any(Long.class), any(PaymentMethodEntity.class)))
                .thenReturn(updatedPaymentMethod);
        when(entityManager.merge(any(PaymentMethodEntity.class)))
                .thenReturn(updatedPaymentMethod);

        when(paymentMethodService.read(2L))
                .thenReturn(Optional.of(updatedPaymentMethod));

        final BaseRequestBoundary<PaymentMethodBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(PaymentMethodMapper.INSTANCE.entityToRequest(updatedPaymentMethod));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .put(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.paymentMethod", is(updatedPaymentMethod.getPaymentMethod()),
                        "data.paymentMethodType", is(updatedPaymentMethod.getPaymentMethodType()),
                        "data.active", is(updatedPaymentMethod.isActive())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.paymentMethod", is(updatedPaymentMethod.getPaymentMethod()),
                        "data.paymentMethodType", is(updatedPaymentMethod.getPaymentMethodType()),
                        "data.active", is(updatedPaymentMethod.isActive())
                );
    }

}
