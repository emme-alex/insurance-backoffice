package it.matteo.bo.rest;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.CustomerEntity;
import it.matteo.bo.domain.primary.OrderAnagStatesEntity;
import it.matteo.bo.domain.primary.OrderEntity;
import it.matteo.bo.domain.primary.PacketEntity;
import it.matteo.bo.domain.primary.PolicyAnagStatesEntity;
import it.matteo.bo.domain.primary.PolicyEntity;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.repository.primary.PolicyRepository;
import it.matteo.bo.service.primary.PolicyService;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class PolicyResourceTest extends BaseTest {

    @InjectMock
    PolicyRepository policyRepo;

    @InjectMock
    PolicyService policyService;

    private final static String BASE_PATH = "/api/v1/policies";

    final OrderEntity order = OrderEntity.builder()
            .id(1L)
            .orderCode("test_order_code")
            .brokerId(1L)
            .anagState(OrderAnagStatesEntity.builder().id(1L).state("Deleted").build())
            .packet(PacketEntity.builder().id(1L).build())
            .product(ProductEntity.builder().id(1L).build())
            .customer(CustomerEntity.builder().id(1L).build())
            .build();

    final PolicyEntity policy = PolicyEntity.builder()
            .id(1L)
            .policyCode("test_policy_code")
            .order(order)
            .state(PolicyAnagStatesEntity.builder()
                    .id(1L)
                    .state("Active")
                    .updatedAt(LocalDateTime.now())
                    .createdAt(LocalDateTime.now())
                    .createdBy("admin")
                    .updatedBy("admin")
                    .build())
            .product(ProductEntity.builder().id(1L).build())
            .build();

    @Test
    public void testGetAll() {
        final PanacheQuery<PolicyEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(policy));

        when(policyService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(policy));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].policyCode", is(policy.getPolicyCode()),
                        "data[0].product.id", is(policy.getProduct().getId().intValue()),
                        "data[0].state.state", is(policy.getState().getState())
                );
    }

    @Test
    public void testGet() {
        when(policyService.read(1L))
                .thenReturn(Optional.of(policy));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.policyCode", is(policy.getPolicyCode()),
                        "data.product.id", is(policy.getProduct().getId().intValue()),
                        "data.state.state", is(policy.getState().getState())
                );
    }

    @Test
    public void testGetByCode() {
        when(policyRepo.findPolicyByCodeOrMasterNumber(any()))
                .thenReturn(Optional.of(policy));

        when(policyService.readPolicyByCodeOrMasterNumber(policy.getPolicyCode()))
                .thenReturn(Optional.of(policy));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byPolicyCode/" + policy.getPolicyCode())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.policyCode", is(policy.getPolicyCode()),
                        "data.product.id", is(policy.getProduct().getId().intValue()),
                        "data.order.id", is(order.getId().intValue()),
                        "data.state.state", is(policy.getState().getState())
                );
    }

    @Test
    public void testGetByOrderId() {
        when(policyService.readPoliciesByOrderId(order.getId()))
                .thenReturn(List.of(policy));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byOrderId/" + order.getId().intValue())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].policyCode", is(policy.getPolicyCode()),
                        "data[0].product.id", is(policy.getProduct().getId().intValue()),
                        "data[0].order.id", is(order.getId().intValue()),
                        "data[0].state.state", is(policy.getState().getState())
                );
    }

    @Test
    public void testGetByCustomerId() {
        when(policyService.readPoliciesByCustomerId(1L))
                .thenReturn(List.of(policy));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byCustomerId/" + order.getCustomer().getId().intValue())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].policyCode", is(not(empty())),
                        "data[0].product.id", is(policy.getProduct().getId().intValue()),
                        "data[0].order.id", is(order.getId().intValue()),
                        "data[0].state.state", is(policy.getState().getState())
                );
    }

}
