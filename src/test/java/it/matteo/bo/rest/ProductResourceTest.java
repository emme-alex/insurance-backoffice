package it.matteo.bo.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.common.collect.Sets;
import io.quarkus.hibernate.orm.PersistenceUnit;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.CategoryEntity;
import it.matteo.bo.domain.primary.PacketEntity;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.domain.primary.ProductImagesEntity;
import it.matteo.bo.domain.primary.SurveyAnswerEntity;
import it.matteo.bo.domain.primary.SurveyQuestionEntity;
import it.matteo.bo.mapper.ProductMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.ProductBoundaryRequest;
import it.matteo.bo.repository.primary.CategoryRepository;
import it.matteo.bo.repository.primary.ProductImagesRepository;
import it.matteo.bo.repository.primary.ProductRepository;
import it.matteo.bo.service.primary.ProductService;
import jakarta.persistence.EntityManager;
import org.hibernate.Session;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class ProductResourceTest extends BaseTest {

    @InjectMock
    ProductRepository productRepository;

    @InjectMock
    ProductImagesRepository productImagesRepository;

    @InjectMock
    CategoryRepository categoryRepository;

    @InjectMock
    ProductService productService;

    @InjectMock
    @PersistenceUnit("primary")
    Session session;

    @InjectMock
    @PersistenceUnit("primary")
    EntityManager entityManager;

    private final static String BASE_PATH = "/api/v1/products";

    final SurveyQuestionEntity surveyQuestion = SurveyQuestionEntity.builder()
            .id(1L)
            .content("test")
            .answers(List.of(
                    SurveyAnswerEntity.builder()
                            .value("Yes")
                            .build()))
            .build();

    final ProductImagesEntity image = ProductImagesEntity.builder()
            .id(1L)
            .images(JsonNodeFactory.instance.objectNode().put("images", "[]"))
            .build();

    final ProductEntity product = ProductEntity.builder()
            .id(1L)
            .code("test_product")
            .image(image)
            .questions(List.of(surveyQuestion))
            .categories(Sets.newHashSet(
                    CategoryEntity.builder()
                            .id(1L)
                            .build()
            ))
            .packets(List.of(PacketEntity.builder().id(1L).build()))
            .build();

    final CategoryEntity category = CategoryEntity.builder()
            .id(1L)
            .name("test_category")
            .externalCode("test_category_code")
            .asset(JsonNodeFactory.instance.objectNode())
            .products(Sets.newHashSet(product))
            .build();

    private final ProductEntity updatedProduct = ProductEntity.builder()
            .id(2L)
            .code(product.getCode())
            .productDescription("updated_test_product_desc")
            .description("updated_description")
            .shortDescription("updated_short_description")
            .informativeSet("updated_informative_set")
            .price(22.00)
            .displayPrice("22,00 €")
            .image(product.getImage())
            .questions(product.getQuestions())
            .categories(product.getCategories())
            .packets(product.getPackets())
            .build();

    @BeforeEach
    void setUp() {
        // necessary for both CREATE and UPDATE
        when(productImagesRepository.findByIdOptional(any(Long.class)))
                .thenReturn(Optional.of(image));
        when(categoryRepository.findByIdOptional(any(Long.class)))
                .thenReturn(Optional.of(category));
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<ProductEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(product));

        when(productRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(productService.listActiveProducts(any(), any(), any(), any(), any()))
                .thenReturn(List.of(product));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].packets[0].id", is(product.getPackets().get(0).getId().intValue()),
                        "data[0].questions[0].id", is(product.getQuestions().get(0).getId().intValue()),
                        "data[0].image.id", is(product.getQuestions().get(0).getId().intValue()),
                        "data[0].code", is(product.getCode())
                );
    }

    @Test
    public void testGet() {
        when(productService.read(1L))
                .thenReturn(Optional.of(product));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.packets[0].id", is(product.getPackets().get(0).getId().intValue()),
                        "data.questions[0].id", is(product.getQuestions().get(0).getId().intValue()),
                        "data.image.id", is(product.getQuestions().get(0).getId().intValue()),
                        "data.code", is(product.getCode())
                );
    }

    @Test
    public void testGetByProductId() {
        when(productService.readByCode(product.getCode()))
                .thenReturn(Optional.of(product));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byCode/" + product.getCode())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.packets[0].id", is(product.getPackets().get(0).getId().intValue()),
                        "data.questions[0].id", is(product.getQuestions().get(0).getId().intValue()),
                        "data.image.id", is(product.getQuestions().get(0).getId().intValue()),
                        "data.code", is(product.getCode())
                );
    }

    @Test
    public void testUpdate() throws JsonProcessingException {
        when(productService.update(any(Long.class), any(ProductEntity.class)))
                .thenReturn(updatedProduct);
        when(entityManager.merge(any(ProductEntity.class)))
                .thenReturn(updatedProduct);

        when(productService.read(2L))
                .thenReturn(Optional.of(updatedProduct));

        final BaseRequestBoundary<ProductBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(ProductMapper.INSTANCE.entityToRequest(updatedProduct));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .put(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.packets[0].id", is(updatedProduct.getPackets().get(0).getId().intValue()),
                        "data.questions[0].id", is(updatedProduct.getQuestions().get(0).getId().intValue()),
                        "data.image.id", is(updatedProduct.getQuestions().get(0).getId().intValue()),
                        "data.code", is(updatedProduct.getCode()),
                        "data.productDescription", is(updatedProduct.getProductDescription()),
                        "data.description", is(updatedProduct.getDescription()),
                        "data.shortDescription", is(updatedProduct.getShortDescription()),
                        "data.price", is(updatedProduct.getPrice().floatValue()),
                        "data.displayPrice", is(updatedProduct.getDisplayPrice()),
                        "data.informativeSet", is(updatedProduct.getInformativeSet())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.packets[0].id", is(updatedProduct.getPackets().get(0).getId().intValue()),
                        "data.questions[0].id", is(updatedProduct.getQuestions().get(0).getId().intValue()),
                        "data.image.id", is(updatedProduct.getQuestions().get(0).getId().intValue()),
                        "data.code", is(updatedProduct.getCode()),
                        "data.productDescription", is(updatedProduct.getProductDescription()),
                        "data.description", is(updatedProduct.getDescription()),
                        "data.shortDescription", is(updatedProduct.getShortDescription()),
                        "data.price", is(updatedProduct.getPrice().floatValue()),
                        "data.displayPrice", is(updatedProduct.getDisplayPrice()),
                        "data.informativeSet", is(updatedProduct.getInformativeSet())
                );
    }

}
