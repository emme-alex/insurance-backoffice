package it.matteo.bo.rest;

import io.quarkus.hibernate.orm.PersistenceUnit;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.CouponEntity;
import it.matteo.bo.domain.primary.PromotionBatchEntity;
import it.matteo.bo.domain.primary.PromotionEntity;
import it.matteo.bo.mapper.PromotionBatchMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.PromotionBatchBoundaryRequest;
import it.matteo.bo.repository.primary.PromotionBatchRepository;
import it.matteo.bo.repository.primary.PromotionRepository;
import it.matteo.bo.service.primary.PromotionBatchService;
import jakarta.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class PromotionBatchResourceTest extends BaseTest {

    @InjectMock
    PromotionBatchRepository promotionBatchRepository;

    @InjectMock
    PromotionBatchService promotionBatchService;

    @InjectMock
    PromotionRepository promotionRepository;

    @InjectMock
    @PersistenceUnit("primary")
    Session session;

    @InjectMock
    @PersistenceUnit("primary")
    EntityManager entityManager;

    private final static String BASE_PATH = "/api/v1/promotionBatches";

    final PromotionEntity promotion = PromotionEntity.builder()
            .id(1L)
            .promotionCode("test_promotion_code")
            .description("test_promotion_desc")
            .build();

    final CouponEntity coupon1 = CouponEntity.builder()
            .id(1L)
            .couponCode("test_coupon_code")
            .promotion(promotion)
            .type("multiple")
            .maxUsage(1)
            .build();

    final CouponEntity coupon2 = CouponEntity.builder()
            .id(2L)
            .couponCode("test_coupon_code")
            .promotion(promotion)
            .type("multiple")
            .maxUsage(1)
            .build();

    final PromotionBatchEntity batch = PromotionBatchEntity.builder()
            .id(1L)
            .promotion(promotion)
            .description("test promotion batch description")
            .baseCode("TIM_")
            .numberOfCodes(2)
            .build();

    final PromotionBatchEntity newBatch = PromotionBatchEntity.builder()
            .id(2L)
            .promotion(promotion)
            .coupons(List.of(coupon1, coupon2))
            .description("test new promotion batch description")
            .baseCode("TEST_")
            .numberOfCodes(2)
            .build();

    @BeforeEach
    void setUp() {
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<PromotionBatchEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(batch));

        when(promotionBatchRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(promotionBatchService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(batch));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].promotion", is(not(empty())),
                        "data[0].baseCode", is(batch.getBaseCode()),
                        "data[0].description", is(batch.getDescription()),
                        "data[0].numberOfCodes", is(batch.getNumberOfCodes()),
                        "data[0].coupons", is(not(empty()))
                );
    }

    @Test
    public void testGet() {
        when(promotionBatchService.read(1L))
                .thenReturn(Optional.of(batch));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.promotion", is(not(empty())),
                        "data.baseCode", is(batch.getBaseCode()),
                        "data.description", is(batch.getDescription()),
                        "data.numberOfCodes", is(batch.getNumberOfCodes()),
                        "data.coupons", is(not(empty()))
                );
    }

    @Test
    public void testCreate() {
        when(promotionRepository.findByIdOptional(any(Long.class)))
                .thenReturn(Optional.of(promotion));

        when(promotionBatchService.create(any(PromotionBatchEntity.class), any(), any()))
                .thenReturn(newBatch);

        when(promotionBatchService.read(2L))
                .thenReturn(Optional.of(newBatch));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<PromotionBatchBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(PromotionBatchMapper.INSTANCE.entityToRequest(newBatch));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.promotion", is(not(empty())),
                        "data.baseCode", is(newBatch.getBaseCode()),
                        "data.description", is(newBatch.getDescription()),
                        "data.numberOfCodes", is(newBatch.getNumberOfCodes()),
                        "data.coupons", is(not(empty()))
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.promotion", is(not(empty())),
                        "data.baseCode", is(newBatch.getBaseCode()),
                        "data.description", is(newBatch.getDescription()),
                        "data.numberOfCodes", is(newBatch.getNumberOfCodes()),
                        "data.coupons", is(not(empty()))
                );
    }

}
