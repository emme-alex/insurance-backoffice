package it.matteo.bo.rest;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import io.quarkus.hibernate.orm.PersistenceUnit;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.PromotionEntity;
import it.matteo.bo.domain.primary.PromotionRuleEntity;
import it.matteo.bo.mapper.PromotionMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.PromotionBoundaryRequest;
import it.matteo.bo.repository.primary.PromotionRepository;
import it.matteo.bo.repository.primary.PromotionRuleRepository;
import it.matteo.bo.service.primary.PromotionService;
import jakarta.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class PromotionResourceTest extends BaseTest {

    @InjectMock
    PromotionRepository promotionRepository;

    @InjectMock
    PromotionRuleRepository promotionRulesRepository;

    @InjectMock
    PromotionService promotionService;

    @InjectMock
    @PersistenceUnit("primary")
    Session session;

    @InjectMock
    @PersistenceUnit("primary")
    EntityManager entityManager;

    private final static String BASE_PATH = "/api/v1/promotions";

    private final static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    final PromotionRuleEntity promotionRule = PromotionRuleEntity.builder()
            .id(1L)
            .rule(JsonNodeFactory.instance.objectNode())
            .name("test_promotion_rule")
            .build();

    final PromotionEntity promotion = PromotionEntity.builder()
            .id(1L)
            .promotionCode("test_promotion_code")
            .description("test_promotion_desc")
            .rule(promotionRule)
            .startDate(LocalDateTime.now())
            .endDate(LocalDateTime.now().plusMonths(2))
            .build();

    final PromotionEntity newPromotion = PromotionEntity.builder()
            .id(2L)
            .promotionCode("test_new_promotion_code")
            .description("test_new_promotion_desc")
            .rule(promotionRule)
            .startDate(promotion.getStartDate())
            .endDate(promotion.getEndDate())
            .build();

    final PromotionEntity updatedPromotion = PromotionEntity.builder()
            .id(2L)
            .promotionCode(newPromotion.getPromotionCode())
            .description("test_updated_promotion_desc")
            .rule(newPromotion.getRule())
            .startDate(newPromotion.getStartDate())
            .endDate(newPromotion.getEndDate())
            .build();

    @BeforeEach
    void setUp() {
        // necessary for both CREATE and UPDATE
        when(promotionRulesRepository.findByIdOptional(any(Long.class)))
                .thenReturn(Optional.of(promotionRule));
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<PromotionEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(promotion));

        when(promotionRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(promotionService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(promotion));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].rule", is(not(empty())),
                        "data[0].promotionCode", is(promotion.getPromotionCode()),
                        "data[0].description", is(promotion.getDescription()),
                        "data[0].startDate", containsString(promotion.getStartDate().format(FORMATTER)),
                        "data[0].endDate", containsString(promotion.getEndDate().format(FORMATTER))
                );
    }

    @Test
    public void testGet() {
        when(promotionService.read(1L))
                .thenReturn(Optional.of(promotion));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.rule", is(not(empty())),
                        "data.promotionCode", is(promotion.getPromotionCode()),
                        "data.description", is(promotion.getDescription()),
                        "data.startDate", containsString(promotion.getStartDate().format(FORMATTER)),
                        "data.endDate", containsString(promotion.getEndDate().format(FORMATTER))
                );
    }

    @Test
    public void testGetByPromotionCode() {
        when(promotionService.readByCode(promotion.getPromotionCode()))
                .thenReturn(Optional.of(promotion));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byCode/" + promotion.getPromotionCode())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.rule", is(not(empty())),
                        "data.promotionCode", is(promotion.getPromotionCode()),
                        "data.description", is(promotion.getDescription()),
                        "data.startDate", containsString(promotion.getStartDate().format(FORMATTER)),
                        "data.endDate", containsString(promotion.getEndDate().format(FORMATTER))
                );
    }

    @Test
    public void testCreate() {
        when(promotionService.create(any(PromotionEntity.class)))
                .thenReturn(newPromotion);

        when(promotionService.read(2L))
                .thenReturn(Optional.of(newPromotion));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<PromotionBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(PromotionMapper.INSTANCE.entityToRequest(promotion));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.rule", is(not(empty())),
                        "data.promotionCode", is(newPromotion.getPromotionCode()),
                        "data.description", is(newPromotion.getDescription()),
                        "data.startDate", containsString(newPromotion.getStartDate().format(FORMATTER)),
                        "data.endDate", containsString(newPromotion.getEndDate().format(FORMATTER))
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.rule", is(not(empty())),
                        "data.promotionCode", is(newPromotion.getPromotionCode()),
                        "data.description", is(newPromotion.getDescription()),
                        "data.startDate", containsString(newPromotion.getStartDate().format(FORMATTER)),
                        "data.endDate", containsString(newPromotion.getEndDate().format(FORMATTER))
                );
    }

    @Test
    public void testUpdate() {
        when(promotionService.update(any(Long.class), any(PromotionEntity.class)))
                .thenReturn(updatedPromotion);
        when(entityManager.merge(any(PromotionEntity.class)))
                .thenReturn(updatedPromotion);

        when(promotionService.read(2L))
                .thenReturn(Optional.of(updatedPromotion));

        final BaseRequestBoundary<PromotionBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(PromotionMapper.INSTANCE.entityToRequest(updatedPromotion));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .put(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.rule", is(not(empty())),
                        "data.promotionCode", is(updatedPromotion.getPromotionCode()),
                        "data.description", is(updatedPromotion.getDescription()),
                        "data.startDate", containsString(updatedPromotion.getStartDate().format(FORMATTER)),
                        "data.endDate", containsString(updatedPromotion.getEndDate().format(FORMATTER))
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.rule", is(not(empty())),
                        "data.promotionCode", is(updatedPromotion.getPromotionCode()),
                        "data.description", is(updatedPromotion.getDescription()),
                        "data.startDate", containsString(updatedPromotion.getStartDate().format(FORMATTER)),
                        "data.endDate", containsString(updatedPromotion.getEndDate().format(FORMATTER))
                );
    }

}
