package it.matteo.bo.rest;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import io.quarkus.hibernate.orm.PersistenceUnit;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.PromotionRuleEntity;
import it.matteo.bo.mapper.PromotionRuleMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.PromotionRuleBoundaryRequest;
import it.matteo.bo.repository.primary.PromotionRuleRepository;
import it.matteo.bo.service.primary.PromotionRuleService;
import jakarta.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class PromotionRuleResourceTest extends BaseTest {

    @InjectMock
    PromotionRuleRepository promotionRuleRepository;

    @InjectMock
    PromotionRuleService promotionRuleService;

    @InjectMock
    @PersistenceUnit("primary")
    Session session;

    @InjectMock
    @PersistenceUnit("primary")
    EntityManager entityManager;

    private final static String BASE_PATH = "/api/v1/promotionRules";

    final PromotionRuleEntity rule = PromotionRuleEntity.builder()
            .id(1L)
            .rule(JsonNodeFactory.instance.objectNode())
            .name("test_promotion_rule")
            .build();

    final PromotionRuleEntity newRule = PromotionRuleEntity.builder()
            .id(2L)
            .rule(rule.getRule())
            .name("new_test_promotion_rule")
            .build();

    final PromotionRuleEntity updatedRule = PromotionRuleEntity.builder()
            .id(2L)
            .rule(newRule.getRule())
            .name("updated_test_promotion_rule")
            .build();

    @BeforeEach
    void setUp() {
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<PromotionRuleEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(rule));

        when(promotionRuleRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(promotionRuleService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(rule));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].name", is(rule.getName()),
                        "data[0].rule", is(not(empty()))
                );
    }

    @Test
    public void testGet() {
        when(promotionRuleService.read(1L))
                .thenReturn(Optional.of(rule));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(rule.getName()),
                        "data.rule", is(not(empty()))
                );
    }

    @Test
    public void testCreate() {
        when(promotionRuleService.create(any(PromotionRuleEntity.class)))
                .thenReturn(newRule);

        when(promotionRuleService.read(2L))
                .thenReturn(Optional.of(newRule));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<PromotionRuleBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(PromotionRuleMapper.INSTANCE.entityToRequest(newRule));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newRule.getName()),
                        "data.rule", is(not(empty()))
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newRule.getName()),
                        "data.rule", is(not(empty()))
                );
    }

    @Test
    public void testUpdate() {
        when(promotionRuleService.update(any(Long.class), any(PromotionRuleEntity.class)))
                .thenReturn(updatedRule);
        when(entityManager.merge(any(PromotionRuleEntity.class)))
                .thenReturn(updatedRule);

        when(promotionRuleService.read(2L))
                .thenReturn(Optional.of(updatedRule));

        final BaseRequestBoundary<PromotionRuleBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(PromotionRuleMapper.INSTANCE.entityToRequest(updatedRule));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .put(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(updatedRule.getName()),
                        "data.rule", is(not(empty()))
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(updatedRule.getName()),
                        "data.rule", is(not(empty()))
                );
    }

}
