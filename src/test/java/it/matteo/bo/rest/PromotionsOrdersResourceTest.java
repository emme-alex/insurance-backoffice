package it.matteo.bo.rest;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import io.quarkus.hibernate.orm.PersistenceUnit;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.CouponEntity;
import it.matteo.bo.domain.primary.CustomerEntity;
import it.matteo.bo.domain.primary.OrderAnagStatesEntity;
import it.matteo.bo.domain.primary.OrderEntity;
import it.matteo.bo.domain.primary.PacketEntity;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.domain.primary.PromotionEntity;
import it.matteo.bo.domain.primary.PromotionRuleEntity;
import it.matteo.bo.domain.primary.PromotionsOrdersEntity;
import it.matteo.bo.mapper.PromotionsOrdersMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.PromotionsOrdersBoundaryRequest;
import it.matteo.bo.repository.primary.PromotionsOrdersRepository;
import it.matteo.bo.service.primary.PromotionsOrdersService;
import jakarta.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class PromotionsOrdersResourceTest extends BaseTest {

    @InjectMock
    PromotionsOrdersRepository promotionsOrdersRepository;

    @InjectMock
    PromotionsOrdersService promotionsOrdersService;

    @InjectMock
    @PersistenceUnit("primary")
    Session session;

    @InjectMock
    @PersistenceUnit("primary")
    EntityManager entityManager;

    private final static String BASE_PATH = "/api/v1/promotionsOrders";

    final PromotionRuleEntity promotionRule = PromotionRuleEntity.builder()
            .id(1L)
            .rule(JsonNodeFactory.instance.objectNode())
            .name("test_promotion_rule")
            .build();

    final PromotionEntity promotion = PromotionEntity.builder()
            .id(1L)
            .promotionCode("test_promotion_code")
            .description("test_promotion_desc")
            .rule(promotionRule)
            .startDate(LocalDateTime.now())
            .endDate(LocalDateTime.now().plusMonths(2))
            .build();

    final CouponEntity coupon = CouponEntity.builder()
            .id(1L)
            .couponCode("test_coupon_code")
            .promotion(promotion)
            .type("single")
            .maxUsage(100)
            .build();

    final ProductEntity product = ProductEntity.builder()
            .id(1L)
            .build();

    final OrderEntity order = OrderEntity.builder()
            .id(1L)
            .orderCode("test_order_code")
            .brokerId(1L)
            .anagState(OrderAnagStatesEntity.builder().id(1L).state("Deleted").build())
            .packet(PacketEntity.builder().id(1L).build())
            .product(product)
            .build();

    final CustomerEntity customer = CustomerEntity.builder()
            .id(1L)
            .name("test customer")
            .surname("test customer surname")
            .taxCode("STLLGU95A01F839A")
            .build();

    final CustomerEntity customer2 = CustomerEntity.builder()
            .id(2L)
            .name("test customer 2")
            .surname("test customer 2 surname")
            .taxCode("STLLGU96A01F839B")
            .build();

    final PromotionsOrdersEntity promotionsOrders = PromotionsOrdersEntity.builder()
            .id(1L)
            .promotion(promotion)
            .order(order)
            .coupon(coupon)
            .customer(customer)
            .build();

    final PromotionsOrdersEntity newPromotionsOrders = PromotionsOrdersEntity.builder()
            .id(2L)
            .promotion(promotionsOrders.getPromotion())
            .order(promotionsOrders.getOrder())
            .coupon(promotionsOrders.getCoupon())
            .customer(customer2)
            .build();

    final PromotionsOrdersEntity updatedPromotionsOrders = PromotionsOrdersEntity.builder()
            .id(2L)
            .promotion(promotionsOrders.getPromotion())
            .order(promotionsOrders.getOrder())
            .coupon(promotionsOrders.getCoupon())
            .customer(customer2)
            .build();

    @BeforeEach
    void setUp() {
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<PromotionsOrdersEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(promotionsOrders));

        when(promotionsOrdersRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(promotionsOrdersService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(promotionsOrders));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].promotionCode", is(promotionsOrders.getPromotion().getPromotionCode()),
                        "data[0].orderCode", is(promotionsOrders.getOrder().getOrderCode()),
                        "data[0].couponCode", is(promotionsOrders.getCoupon().getCouponCode()),
                        "data[0].customerCode", is(promotionsOrders.getCustomer().getTaxCode())
                );
    }

    @Test
    public void testGet() {
        when(promotionsOrdersService.read(1L))
                .thenReturn(Optional.of(promotionsOrders));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.promotionCode", is(promotionsOrders.getPromotion().getPromotionCode()),
                        "data.orderCode", is(promotionsOrders.getOrder().getOrderCode()),
                        "data.couponCode", is(promotionsOrders.getCoupon().getCouponCode()),
                        "data.customerCode", is(promotionsOrders.getCustomer().getTaxCode())
                );
    }

    @Test
    public void testGetByPromotionCode() {
        when(promotionsOrdersService.readByPromotionCode(promotionsOrders.getPromotion().getPromotionCode()))
                .thenReturn(List.of(promotionsOrders));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byPromotionCode/" + promotionsOrders.getPromotion().getPromotionCode())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].promotionCode", is(promotionsOrders.getPromotion().getPromotionCode()),
                        "data[0].orderCode", is(promotionsOrders.getOrder().getOrderCode()),
                        "data[0].couponCode", is(promotionsOrders.getCoupon().getCouponCode()),
                        "data[0].customerCode", is(promotionsOrders.getCustomer().getTaxCode())
                );
    }

    @Test
    public void testGetByCouponCode() {
        when(promotionsOrdersService.readByCouponCode(promotionsOrders.getCoupon().getCouponCode(), false))
                .thenReturn(List.of(promotionsOrders));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byCouponCode/" + promotionsOrders.getCoupon().getCouponCode())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].promotionCode", is(promotionsOrders.getPromotion().getPromotionCode()),
                        "data[0].orderCode", is(promotionsOrders.getOrder().getOrderCode()),
                        "data[0].couponCode", is(promotionsOrders.getCoupon().getCouponCode()),
                        "data[0].customerCode", is(promotionsOrders.getCustomer().getTaxCode())
                );
    }

    @Test
    public void testGetByOrderCode() {
        when(promotionsOrdersService.readByOrderCode(promotionsOrders.getOrder().getOrderCode()))
                .thenReturn(List.of(promotionsOrders));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byOrderCode/" + promotionsOrders.getOrder().getOrderCode())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].promotionCode", is(promotionsOrders.getPromotion().getPromotionCode()),
                        "data[0].orderCode", is(promotionsOrders.getOrder().getOrderCode()),
                        "data[0].couponCode", is(promotionsOrders.getCoupon().getCouponCode()),
                        "data[0].customerCode", is(promotionsOrders.getCustomer().getTaxCode())
                );
    }

    @Test
    public void testGetByCustomerCode() {
        when(promotionsOrdersService.readByCustomerCode(promotionsOrders.getCustomer().getTaxCode()))
                .thenReturn(List.of(promotionsOrders));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byCustomerCode/" + promotionsOrders.getCustomer().getTaxCode())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].promotionCode", is(promotionsOrders.getPromotion().getPromotionCode()),
                        "data[0].orderCode", is(promotionsOrders.getOrder().getOrderCode()),
                        "data[0].couponCode", is(promotionsOrders.getCoupon().getCouponCode()),
                        "data[0].customerCode", is(promotionsOrders.getCustomer().getTaxCode())
                );
    }

    @Test
    public void testCreate() {
        when(promotionsOrdersService.create(any(PromotionsOrdersEntity.class)))
                .thenReturn(newPromotionsOrders);

        when(promotionsOrdersService.read(2L))
                .thenReturn(Optional.of(newPromotionsOrders));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<PromotionsOrdersBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(PromotionsOrdersMapper.INSTANCE.entityToRequest(newPromotionsOrders));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.promotionCode", is(newPromotionsOrders.getPromotion().getPromotionCode()),
                        "data.orderCode", is(newPromotionsOrders.getOrder().getOrderCode()),
                        "data.couponCode", is(newPromotionsOrders.getCoupon().getCouponCode()),
                        "data.customerCode", is(newPromotionsOrders.getCustomer().getTaxCode())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.promotionCode", is(newPromotionsOrders.getPromotion().getPromotionCode()),
                        "data.orderCode", is(newPromotionsOrders.getOrder().getOrderCode()),
                        "data.couponCode", is(newPromotionsOrders.getCoupon().getCouponCode()),
                        "data.customerCode", is(newPromotionsOrders.getCustomer().getTaxCode())
                );
    }

    @Test
    public void testUpdate() {
        when(promotionsOrdersService.update(any(Long.class), any(PromotionsOrdersEntity.class)))
                .thenReturn(updatedPromotionsOrders);
        when(entityManager.merge(any(PromotionsOrdersEntity.class)))
                .thenReturn(updatedPromotionsOrders);

        when(promotionsOrdersService.read(2L))
                .thenReturn(Optional.of(updatedPromotionsOrders));

        final BaseRequestBoundary<PromotionsOrdersBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(PromotionsOrdersMapper.INSTANCE.entityToRequest(updatedPromotionsOrders));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .put(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.promotionCode", is(updatedPromotionsOrders.getPromotion().getPromotionCode()),
                        "data.orderCode", is(updatedPromotionsOrders.getOrder().getOrderCode()),
                        "data.couponCode", is(updatedPromotionsOrders.getCoupon().getCouponCode()),
                        "data.customerCode", is(updatedPromotionsOrders.getCustomer().getTaxCode())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.promotionCode", is(updatedPromotionsOrders.getPromotion().getPromotionCode()),
                        "data.orderCode", is(updatedPromotionsOrders.getOrder().getOrderCode()),
                        "data.couponCode", is(updatedPromotionsOrders.getCoupon().getCouponCode()),
                        "data.customerCode", is(updatedPromotionsOrders.getCustomer().getTaxCode())
                );
    }

}
