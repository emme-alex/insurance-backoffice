package it.matteo.bo.rest;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.BranchEntity;
import it.matteo.bo.domain.primary.PacketEntity;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.domain.primary.ProductImagesEntity;
import it.matteo.bo.domain.primary.RelatedWarrantyEntity;
import it.matteo.bo.domain.primary.WarrantyEntity;
import it.matteo.bo.repository.primary.RelatedWarrantyRepository;
import it.matteo.bo.service.primary.RelatedWarrantyService;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class RelatedWarrantyResourceTest extends BaseTest {

    @InjectMock
    RelatedWarrantyRepository relatedWarrantyRepository;

    @InjectMock
    RelatedWarrantyService relatedWarrantyService;

    private final static String BASE_PATH = "/api/v1/relatedWarranties";

    final BranchEntity branch = BranchEntity.builder()
            .id(1L)
            .name("Malattia")
            .externalCode("DANNI-2")
            .section("DANNI")
            .build();

    final WarrantyEntity warranty = WarrantyEntity.builder()
            .id(1L)
            .name("test_warranty")
            .ceilings(JsonNodeFactory.instance.objectNode())
            .images(JsonNodeFactory.instance.objectNode())
            .branch(branch)
            .build();

    final ProductEntity product = ProductEntity.builder()
            .id(1L)
            .code("net-pet-gold")
            .image(ProductImagesEntity.builder()
                    .id(1L)
                    .images(JsonNodeFactory.instance.objectNode()).build())
            .build();

    final PacketEntity packet = PacketEntity.builder()
            .id(1L)
            .name("test_packet")
            .sku("test_sku")
            .brokerId(1L)
            .product(product)
            .build();

    final RelatedWarrantyEntity relatedWarranty = RelatedWarrantyEntity.builder()
            .id(1L)
            .warranty(warranty)
            .ceilings(JsonNodeFactory.instance.objectNode())
            .packet(packet)
            .build();

    @BeforeEach
    void setUp() {
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<RelatedWarrantyEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(relatedWarranty));

        when(relatedWarrantyRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(relatedWarrantyService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(relatedWarranty));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].externalCode", is(relatedWarranty.getExternalCode()),
                        "data[0].warranty.id", is(relatedWarranty.getWarranty().getId().intValue()),
                        "data[0].packet.id", is(relatedWarranty.getPacket().getId().intValue())
                );
    }

    @Test
    public void testGet() {
        when(relatedWarrantyService.read(1L))
                .thenReturn(Optional.of(relatedWarranty));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.externalCode", is(relatedWarranty.getExternalCode()),
                        "data.warranty.id", is(relatedWarranty.getWarranty().getId().intValue()),
                        "data.packet.id", is(relatedWarranty.getPacket().getId().intValue())
                );
    }

    @Test
    public void testGetByWarrantyAndPacket() {
        when(relatedWarrantyService.findByWarrantyAndPacket(1L, 1L))
                .thenReturn(List.of(relatedWarranty));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byWarrantyAndPacket/" + warranty.getId().intValue() + "/" + packet.getId().intValue())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].externalCode", is(relatedWarranty.getExternalCode()),
                        "data[0].warranty.id", is(relatedWarranty.getWarranty().getId().intValue()),
                        "data[0].packet.id", is(relatedWarranty.getPacket().getId().intValue())
                );
    }

    @Test
    public void testGetByPacketId() {
        when(relatedWarrantyService.findByPacket(1L))
                .thenReturn(List.of(relatedWarranty));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/byPacketId/" + packet.getId().intValue())
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].externalCode", is(relatedWarranty.getExternalCode()),
                        "data[0].warranty.id", is(relatedWarranty.getWarranty().getId().intValue()),
                        "data[0].packet.id", is(relatedWarranty.getPacket().getId().intValue())
                );
    }

}
