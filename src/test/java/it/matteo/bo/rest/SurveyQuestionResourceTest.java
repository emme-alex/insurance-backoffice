package it.matteo.bo.rest;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.domain.primary.ProductImagesEntity;
import it.matteo.bo.domain.primary.SurveyQuestionEntity;
import it.matteo.bo.repository.primary.ProductRepository;
import it.matteo.bo.repository.primary.SurveyQuestionRepository;
import it.matteo.bo.service.primary.SurveyQuestionService;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class SurveyQuestionResourceTest extends BaseTest {

    @InjectMock
    SurveyQuestionRepository surveyQuestionRepository;

    @InjectMock
    ProductRepository productRepository;

    @InjectMock
    SurveyQuestionService surveyQuestionService;

    private final static String BASE_PATH = "/api/v1/surveyQuestions";

    final ProductEntity product = ProductEntity.builder()
            .id(1L)
            .code("net-pet-gold")
            .image(ProductImagesEntity.builder()
                    .id(1L)
                    .images(JsonNodeFactory.instance.objectNode()).build())
            .build();

    final SurveyQuestionEntity surveyQuestion = SurveyQuestionEntity.builder()
            .id(1L)
            .content("test_survey_question")
            .position(2)
            .product(product)
            .build();

    @BeforeEach
    void setUp() {
        // necessary for both CREATE and UPDATE
        when(productRepository.findByIdOptional(any(Long.class)))
                .thenReturn(Optional.of(product));
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<SurveyQuestionEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(surveyQuestion));

        when(surveyQuestionRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(surveyQuestionService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(surveyQuestion));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].content", is(surveyQuestion.getContent()),
                        "data[0].position", is(surveyQuestion.getPosition())
                );
    }

    @Test
    public void testGet() {
        when(surveyQuestionService.read(1L))
                .thenReturn(Optional.of(surveyQuestion));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.content", is(surveyQuestion.getContent()),
                        "data.position", is(surveyQuestion.getPosition())
                );
    }

}
