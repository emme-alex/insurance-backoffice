package it.matteo.bo.rest;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import io.quarkus.hibernate.orm.PersistenceUnit;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.domain.primary.ProductImagesEntity;
import it.matteo.bo.domain.primary.UtmSourceEntity;
import it.matteo.bo.domain.primary.UtmSourceLocationEntity;
import it.matteo.bo.mapper.UtmSourceLocationMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.UtmSourceLocationBoundaryRequest;
import it.matteo.bo.repository.primary.UtmSourceLocationRepository;
import it.matteo.bo.service.primary.UtmSourceLocationService;
import jakarta.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class UtmSourceLocationResourceTest extends BaseTest {

    @InjectMock
    UtmSourceLocationRepository utmSourceLocationRepository;

    @InjectMock
    UtmSourceLocationService utmSourceLocationService;

    @InjectMock
    @PersistenceUnit("primary")
    Session session;

    @InjectMock
    @PersistenceUnit("primary")
    EntityManager entityManager;

    private final static String BASE_PATH = "/api/v1/utmSourceLocations";

    final ProductEntity product = ProductEntity.builder()
            .id(1L)
            .code("net-pet-gold")
            .image(ProductImagesEntity.builder()
                    .id(1L)
                    .images(JsonNodeFactory.instance.objectNode()).build())
            .build();

    final UtmSourceEntity utmSource = UtmSourceEntity.builder()
            .id(1L)
            .name("test utm source")
            .code("test_utm_source")
            .products(Set.of(product))
            .build();

    final UtmSourceLocationEntity location = UtmSourceLocationEntity.builder()
            .id(1L)
            .code("conad_1")
            .name("Conad, Via Roma 1")
            .utmSource(utmSource)
            .build();

    private final UtmSourceLocationEntity newLocation = UtmSourceLocationEntity.builder()
            .id(2L)
            .name("test new location")
            .code("test_new_location")
            .utmSource(utmSource)
            .build();

    private final UtmSourceLocationEntity locationNoUtmSource = UtmSourceLocationEntity.builder()
            .id(2L)
            .name("test new location no utm source")
            .code("test_new_location_no_utm_source")
            .build();

    private final UtmSourceLocationEntity updatedLocation = UtmSourceLocationEntity.builder()
            .id(2L)
            .name("updated test location")
            .code(location.getCode())
            .utmSource(location.getUtmSource())
            .build();

    @BeforeEach
    void setUp() {
        // necessary for both CREATE and UPDATE
        when(utmSourceLocationRepository.findByIdOptional(any(Long.class)))
                .thenReturn(Optional.of(location));
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<UtmSourceLocationEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(location));

        when(utmSourceLocationRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(utmSourceLocationService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(location));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].name", is(location.getName()),
                        "data[0].code", is(location.getCode()),
                        "data[0].utmSource", is(notNullValue())
                );
    }

    @Test
    public void testGet() {
        when(utmSourceLocationService.read(1L))
                .thenReturn(Optional.of(location));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(location.getName()),
                        "data.code", is(location.getCode()),
                        "data.utmSource", is(notNullValue())
                );
    }

    @Test
    public void testCreate() {
        when(utmSourceLocationService.create(any(UtmSourceLocationEntity.class)))
                .thenReturn(newLocation);

        when(utmSourceLocationService.read(2L))
                .thenReturn(Optional.of(newLocation));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<UtmSourceLocationBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(UtmSourceLocationMapper.INSTANCE.entityToRequest(newLocation));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newLocation.getName()),
                        "data.code", is(newLocation.getCode()),
                        "data.utmSource", is(notNullValue())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newLocation.getName()),
                        "data.code", is(newLocation.getCode()),
                        "data.utmSource", is(notNullValue())
                );
    }

    @Test
    public void testUpdate() {
        when(utmSourceLocationService.update(any(Long.class), any(UtmSourceLocationEntity.class)))
                .thenReturn(updatedLocation);
        when(entityManager.merge(any(UtmSourceLocationEntity.class)))
                .thenReturn(updatedLocation);

        when(utmSourceLocationService.read(2L))
                .thenReturn(Optional.of(updatedLocation));

        final BaseRequestBoundary<UtmSourceLocationBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(UtmSourceLocationMapper.INSTANCE.entityToRequest(updatedLocation));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .put(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(updatedLocation.getName()),
                        "data.code", is(updatedLocation.getCode()),
                        "data.utmSource", is(notNullValue())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(updatedLocation.getName()),
                        "data.code", is(updatedLocation.getCode()),
                        "data.utmSource", is(notNullValue())
                );
    }

    @Test
    public void testCreateNoUtmSource() {
        when(utmSourceLocationService.create(any(UtmSourceLocationEntity.class)))
                .thenReturn(locationNoUtmSource);

        when(utmSourceLocationService.read(2L))
                .thenReturn(Optional.of(locationNoUtmSource));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<UtmSourceLocationBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(UtmSourceLocationMapper.INSTANCE.entityToRequest(locationNoUtmSource));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(locationNoUtmSource.getName()),
                        "data.code", is(locationNoUtmSource.getCode()),
                        "data.utmSource", is(nullValue())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(locationNoUtmSource.getName()),
                        "data.code", is(locationNoUtmSource.getCode()),
                        "data.utmSource", is(nullValue())
                );
    }

}
