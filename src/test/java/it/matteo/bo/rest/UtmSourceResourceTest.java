package it.matteo.bo.rest;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import io.quarkus.hibernate.orm.PersistenceUnit;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.ProductEntity;
import it.matteo.bo.domain.primary.ProductImagesEntity;
import it.matteo.bo.domain.primary.UtmSourceEntity;
import it.matteo.bo.domain.primary.UtmSourceLocationEntity;
import it.matteo.bo.mapper.UtmSourceMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.UtmSourceBoundaryRequest;
import it.matteo.bo.repository.primary.UtmSourceLocationRepository;
import it.matteo.bo.repository.primary.UtmSourceRepository;
import it.matteo.bo.service.primary.UtmSourceService;
import jakarta.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class UtmSourceResourceTest extends BaseTest {

    @InjectMock
    UtmSourceRepository utmSourceRepository;

    @InjectMock
    UtmSourceLocationRepository utmSourceLocationRepository;

    @InjectMock
    UtmSourceService utmSourceService;

    @InjectMock
    @PersistenceUnit("primary")
    Session session;

    @InjectMock
    @PersistenceUnit("primary")
    EntityManager entityManager;

    private final static String BASE_PATH = "/api/v1/utmSources";

    final ProductEntity product = ProductEntity.builder()
            .id(1L)
            .code("net-pet-gold")
            .image(ProductImagesEntity.builder()
                    .id(1L)
                    .images(JsonNodeFactory.instance.objectNode()).build())
            .build();

    final ProductEntity product2 = ProductEntity.builder()
            .id(2L)
            .code("net-pet-silver")
            .image(ProductImagesEntity.builder()
                    .id(1L)
                    .images(JsonNodeFactory.instance.objectNode()).build())
            .build();

    final UtmSourceLocationEntity location = UtmSourceLocationEntity.builder()
            .id(1L)
            .code("conad_1")
            .name("Conad, Via Roma 1")
            .build();

    final UtmSourceEntity utmSource = UtmSourceEntity.builder()
            .id(1L)
            .name("test utm source")
            .code("test_utm_source")
            .products(Set.of(product))
            .locations(Set.of(location))
            .build();

    private final UtmSourceEntity newUtmSource = UtmSourceEntity.builder()
            .id(2L)
            .name("test new utm source")
            .code("test_new_utm_source")
            .products(Set.of(product2))
            .locations(utmSource.getLocations())
            .build();

    private final UtmSourceEntity newUtmSourceNoLocation = UtmSourceEntity.builder()
            .id(2L)
            .name("test new utm source no location")
            .code("test_new_utm_source_no_location")
            .products(Set.of(product, product2))
            .build();

    private final UtmSourceEntity updatedUtmSource = UtmSourceEntity.builder()
            .id(2L)
            .name("updated test utm source")
            .code(utmSource.getCode())
            .products(Set.of(product, product2))
            .locations(utmSource.getLocations())
            .build();

    @BeforeEach
    void setUp() {
        // necessary for both CREATE and UPDATE
        when(utmSourceLocationRepository.findByIdOptional(any(Long.class)))
                .thenReturn(Optional.of(location));
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<UtmSourceEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(utmSource));

        when(utmSourceRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(utmSourceService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(utmSource));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].name", is(utmSource.getName()),
                        "data[0].code", is(utmSource.getCode()),
                        "data[0].products", is(not(empty())),
                        "data[0].locations", is(not(empty())),
                        "data[0].products.size()", is(1),
                        "data[0].products.stream().map(p -> p.id).toList()",
                        is(utmSource.getProducts().stream()
                                .map(ProductEntity::getId)
                                .map(Long::intValue)
                                .toList()),
                        "data[0].locations", is(not(empty())),
                        "data[0].locations.stream().map(p -> p.id).toList()",
                        is(utmSource.getLocations().stream()
                                .map(UtmSourceLocationEntity::getId)
                                .map(Long::intValue)
                                .toList())
                );
    }

    @Test
    public void testGet() {
        when(utmSourceService.read(1L))
                .thenReturn(Optional.of(utmSource));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(utmSource.getName()),
                        "data.code", is(utmSource.getCode()),
                        "data.products", is(not(empty())),
                        "data.locations", is(not(empty())),
                        "data.products.size()", is(1),
                        "data.products.stream().map(p -> p.id).toList()",
                        is(utmSource.getProducts().stream()
                                .map(ProductEntity::getId)
                                .map(Long::intValue)
                                .toList()),
                        "data.locations", is(not(empty())),
                        "data.locations.stream().map(p -> p.id).toList()",
                        is(utmSource.getLocations().stream()
                                .map(UtmSourceLocationEntity::getId)
                                .map(Long::intValue)
                                .toList())
                );
    }

    @Test
    public void testCreate() {
        when(utmSourceService.create(any(UtmSourceEntity.class)))
                .thenReturn(newUtmSource);

        when(utmSourceService.read(2L))
                .thenReturn(Optional.of(newUtmSource));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<UtmSourceBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(UtmSourceMapper.INSTANCE.entityToRequest(newUtmSource));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newUtmSource.getName()),
                        "data.code", is(newUtmSource.getCode()),
                        "data.products", is(not(empty())),
                        "data.locations", is(not(empty())),
                        "data.products.size()", is(1),
                        "data.products.stream().map(p -> p.id).toList()",
                        is(newUtmSource.getProducts().stream()
                                .map(ProductEntity::getId)
                                .map(Long::intValue)
                                .toList()),
                        "data.locations", is(not(empty())),
                        "data.locations.stream().map(p -> p.id).toList()",
                        is(newUtmSource.getLocations().stream()
                                .map(UtmSourceLocationEntity::getId)
                                .map(Long::intValue)
                                .toList())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newUtmSource.getName()),
                        "data.code", is(newUtmSource.getCode()),
                        "data.products", is(not(empty())),
                        "data.locations", is(not(empty())),
                        "data.products.size()", is(1),
                        "data.products.stream().map(p -> p.id).toList()",
                        is(newUtmSource.getProducts().stream()
                                .map(ProductEntity::getId)
                                .map(Long::intValue)
                                .toList()),
                        "data.locations", is(not(empty())),
                        "data.locations.stream().map(p -> p.id).toList()",
                        is(newUtmSource.getLocations().stream()
                                .map(UtmSourceLocationEntity::getId)
                                .map(Long::intValue)
                                .toList())
                );
    }

    @Test
    public void testUpdate() {
        when(utmSourceService.update(any(Long.class), any(UtmSourceEntity.class)))
                .thenReturn(updatedUtmSource);
        when(entityManager.merge(any(UtmSourceEntity.class)))
                .thenReturn(updatedUtmSource);

        when(utmSourceService.read(2L))
                .thenReturn(Optional.of(updatedUtmSource));

        final BaseRequestBoundary<UtmSourceBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(UtmSourceMapper.INSTANCE.entityToRequest(updatedUtmSource));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .put(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(updatedUtmSource.getName()),
                        "data.code", is(updatedUtmSource.getCode()),
                        "data.products", is(not(empty())),
                        "data.locations", is(not(empty())),
                        "data.products.size()", is(2),
                        "data.products.stream().map(p -> p.id).toList()",
                        is(updatedUtmSource.getProducts().stream()
                                .map(ProductEntity::getId)
                                .map(Long::intValue)
                                .toList()),
                        "data.locations", is(not(empty())),
                        "data.locations.stream().map(p -> p.id).toList()",
                        is(updatedUtmSource.getLocations().stream()
                                .map(UtmSourceLocationEntity::getId)
                                .map(Long::intValue)
                                .toList())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(updatedUtmSource.getName()),
                        "data.code", is(updatedUtmSource.getCode()),
                        "data.products", is(not(empty())),
                        "data.locations", is(not(empty())),
                        "data.products.size()", is(2),
                        "data.products.stream().map(p -> p.id).toList()",
                        is(updatedUtmSource.getProducts().stream()
                                .map(ProductEntity::getId)
                                .map(Long::intValue)
                                .toList()),
                        "data.locations", is(not(empty())),
                        "data.locations.stream().map(p -> p.id).toList()",
                        is(newUtmSource.getLocations().stream()
                                .map(UtmSourceLocationEntity::getId)
                                .map(Long::intValue)
                                .toList())
                );
    }

    @Test
    public void testCreateNoLocation() {
        when(utmSourceService.create(any(UtmSourceEntity.class)))
                .thenReturn(newUtmSourceNoLocation);

        when(utmSourceService.read(2L))
                .thenReturn(Optional.of(newUtmSourceNoLocation));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<UtmSourceBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(UtmSourceMapper.INSTANCE.entityToRequest(newUtmSourceNoLocation));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newUtmSourceNoLocation.getName()),
                        "data.code", is(newUtmSourceNoLocation.getCode()),
                        "data.products", is(not(empty())),
                        "data.locations", is(nullValue()),
                        "data.products.size()", is(2),
                        "data.products.stream().map(p -> p.id).toList()",
                        is(newUtmSourceNoLocation.getProducts().stream()
                                .map(ProductEntity::getId)
                                .map(Long::intValue)
                                .toList()),
                        "data.locations", is(nullValue())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newUtmSourceNoLocation.getName()),
                        "data.code", is(newUtmSourceNoLocation.getCode()),
                        "data.products", is(not(empty())),
                        "data.locations", is(nullValue()),
                        "data.products.size()", is(2),
                        "data.products.stream().map(p -> p.id).toList()",
                        is(newUtmSourceNoLocation.getProducts().stream()
                                .map(ProductEntity::getId)
                                .map(Long::intValue)
                                .toList()),
                        "data.locations", is(nullValue())
                );
    }

}
