package it.matteo.bo.rest;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusIntegrationTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.BoTestProfile;
import it.matteo.bo.CustomResource;
import it.matteo.bo.domain.primary.WarrantyEntity;
import it.matteo.bo.mapper.WarrantyMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.WarrantyBoundaryRequest;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;

@QuarkusIntegrationTest
@TestSecurity(authorizationEnabled = false)
@QuarkusTestResource(CustomResource.class)
@TestProfile(BoTestProfile.class)
public class WarrantyResourceIT extends BaseTest {

    protected final static String BASE_PATH = "/api/v1/warranties";

    final WarrantyEntity warranty1 = WarrantyEntity.builder()
            .id(1L)
            .name("test warranty 1")
            .ceilings(JsonNodeFactory.instance.objectNode())
            .images(JsonNodeFactory.instance.objectNode())
            .build();

    private final WarrantyEntity warranty2 = WarrantyEntity.builder()
            .id(2L)
            .name("test warranty 2")
            .ceilings(JsonNodeFactory.instance.objectNode())
            .images(JsonNodeFactory.instance.objectNode())
            .build();

    @BeforeEach
    void setUp() {
        // create warranty 1
        final BaseRequestBoundary<WarrantyBoundaryRequest> rq1 = new BaseRequestBoundary<>();
        rq1.setData(WarrantyMapper.INSTANCE.entityToRequest(warranty1));
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq1)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(warranty1.getName())
                );
        // create warranty 2
        final BaseRequestBoundary<WarrantyBoundaryRequest> rq2 = new BaseRequestBoundary<>();
        rq2.setData(WarrantyMapper.INSTANCE.entityToRequest(warranty2));
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq2)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(warranty2.getName())
                );
    }

    @Test
    public void testGetAllPaginatedFiltered() {
        given()
                .contentType(ContentType.JSON)
                .when()
                .queryParam("page", "0")
                .queryParam("perPage", "1")
                .queryParam("sort", "id")
                .queryParam("order", "asc")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].name", is(warranty1.getName())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .queryParam("page", "0")
                .queryParam("perPage", "1")
                .queryParam("sort", "id")
                .queryParam("order", "desc")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].name", is(warranty2.getName())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .queryParam("filter", "{\"name\":\"" + warranty1.getName() + "\"}")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].name", is(warranty1.getName())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .queryParam("filter", "{\"id\":" + warranty1.getId().intValue() + "}")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(warranty1.getId().intValue()),
                        "data[0].name", is(warranty1.getName())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .queryParam("filter", "{\"branch\":null}")
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(2)
                );
    }

}
