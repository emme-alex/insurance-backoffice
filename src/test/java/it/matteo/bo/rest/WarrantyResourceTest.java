package it.matteo.bo.rest;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import io.quarkus.hibernate.orm.PersistenceUnit;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import it.matteo.bo.BaseTest;
import it.matteo.bo.domain.primary.BranchEntity;
import it.matteo.bo.domain.primary.WarrantyEntity;
import it.matteo.bo.mapper.WarrantyMapper;
import it.matteo.bo.model.primary.BaseRequestBoundary;
import it.matteo.bo.model.primary.WarrantyBoundaryRequest;
import it.matteo.bo.repository.primary.BranchRepository;
import it.matteo.bo.repository.primary.WarrantyRepository;
import it.matteo.bo.service.primary.WarrantyService;
import jakarta.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
public class WarrantyResourceTest extends BaseTest {

    @InjectMock
    WarrantyRepository warrantyRepository;

    @InjectMock
    BranchRepository branchRepository;

    @InjectMock
    WarrantyService warrantyService;

    @InjectMock
    @PersistenceUnit("primary")
    Session session;

    @InjectMock
    @PersistenceUnit("primary")
    EntityManager entityManager;

    private final static String BASE_PATH = "/api/v1/warranties";

    final BranchEntity branch = BranchEntity.builder()
            .id(1L)
            .name("Malattia")
            .externalCode("DANNI-2")
            .section("DANNI")
            .build();

    final WarrantyEntity warranty = WarrantyEntity.builder()
            .id(1L)
            .name("test_warranty")
            .ceilings(JsonNodeFactory.instance.objectNode())
            .images(JsonNodeFactory.instance.objectNode())
            .branch(branch)
            .build();

    private final WarrantyEntity newWarranty = WarrantyEntity.builder()
            .id(2L)
            .name("new_test_warranty")
            .ceilings(JsonNodeFactory.instance.objectNode())
            .images(JsonNodeFactory.instance.objectNode())
            .branch(branch)
            .build();

    private final WarrantyEntity newWarrantyNoBranch = WarrantyEntity.builder()
            .id(2L)
            .name(newWarranty.getName())
            .ceilings(JsonNodeFactory.instance.objectNode())
            .images(JsonNodeFactory.instance.objectNode())
            .build();

    final BranchEntity newAddedBranch = BranchEntity.builder()
            .id(2L)
            .name("Corpi di veicoli ferroviari")
            .externalCode("DANNI-4")
            .section("DANNI")
            .build();

    private final WarrantyEntity updatedWarranty = WarrantyEntity.builder()
            .id(2L)
            .name("updated_test_warranty")
            .ceilings(JsonNodeFactory.instance.objectNode())
            .images(JsonNodeFactory.instance.objectNode())
            .branch(newAddedBranch)
            .build();

    @BeforeEach
    void setUp() {
        // necessary for both CREATE and UPDATE
        when(branchRepository.findByIdOptional(any(Long.class)))
                .thenReturn(Optional.of(branch));
    }

    @Test
    public void testGetAll() {
        final PanacheQuery<WarrantyEntity> query = Mockito.mock(PanacheQuery.class);

        when(query.page(any())).thenReturn(query);
        when(query.firstResultOptional()).thenReturn(Optional.of(warranty));

        when(warrantyRepository.findAll(any(Sort.class)))
                .thenReturn(query);
        when(warrantyService.list(any(), any(), any(), any(), any()))
                .thenReturn(List.of(warranty));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.size()", is(1),
                        "data[0].id", is(not(empty())),
                        "data[0].name", is(warranty.getName()),
                        "data[0].branch.id", is(warranty.getBranch().getId().intValue())
                );
    }

    @Test
    public void testGet() {
        when(warrantyService.read(1L))
                .thenReturn(Optional.of(warranty));

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/1")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(warranty.getName()),
                        "data.branch.id", is(warranty.getBranch().getId().intValue())
                );
    }

    @Test
    public void testCreate() {
        when(warrantyService.create(any(WarrantyEntity.class)))
                .thenReturn(newWarranty);

        when(warrantyService.read(2L))
                .thenReturn(Optional.of(newWarranty));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<WarrantyBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(WarrantyMapper.INSTANCE.entityToRequest(newWarranty));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newWarranty.getName()),
                        "data.branch.id", is(newWarranty.getBranch().getId().intValue())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newWarranty.getName()),
                        "data.branch.id", is(newWarranty.getBranch().getId().intValue())
                );
    }

    @Test
    public void testUpdate() {
        when(warrantyService.update(any(Long.class), any(WarrantyEntity.class)))
                .thenReturn(updatedWarranty);
        when(entityManager.merge(any(WarrantyEntity.class)))
                .thenReturn(updatedWarranty);

        when(warrantyService.read(2L))
                .thenReturn(Optional.of(updatedWarranty));

        final BaseRequestBoundary<WarrantyBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(WarrantyMapper.INSTANCE.entityToRequest(updatedWarranty));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .put(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(updatedWarranty.getName()),
                        "data.branch.id", is(updatedWarranty.getBranch().getId().intValue())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(updatedWarranty.getName()),
                        "data.branch.id", is(updatedWarranty.getBranch().getId().intValue())
                );
    }

    @Test
    public void testCreateNoBranch() {
        when(warrantyService.create(any(WarrantyEntity.class)))
                .thenReturn(newWarrantyNoBranch);

        when(warrantyService.read(2L))
                .thenReturn(Optional.of(newWarrantyNoBranch));

        final Query mockQuery = Mockito.mock(Query.class);
        doNothing().when(session).persist(any());
        when(session.createQuery(anyString())).thenReturn(mockQuery);
        when(mockQuery.getSingleResult()).thenReturn(new Random().nextLong(10));

        final BaseRequestBoundary<WarrantyBoundaryRequest> rq = new BaseRequestBoundary<>();
        rq.setData(WarrantyMapper.INSTANCE.entityToRequest(newWarrantyNoBranch));

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .body(rq)
                .post(BASE_PATH)
                .then()
                .statusCode(RestResponse.StatusCode.CREATED)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newWarranty.getName()),
                        "data.branch", is(nullValue())
                );

        given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_PATH + "/2")
                .then()
                .statusCode(RestResponse.StatusCode.OK)
                .body("data", is(notNullValue()),
                        "data", is(not(empty())),
                        "data.id", is(not(empty())),
                        "data.name", is(newWarranty.getName()),
                        "data.branch", is(nullValue())
                );
    }

}
