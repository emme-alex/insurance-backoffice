# API Docs

## Categories

- GET `/api/v1/categories`
- GET `/api/v1/categories/{id}`
- POST `/api/v1/categories`
- PUT `/api/v1/categories/{id}`

`POST` request JSON sample:

```json
{
    "data": {
        "name": "test category",
        "externalCode": "test_home",
        "asset": {}
    }
}
```

`PUT` request JSON sample:

```json
{
    "data": {
        "name": "test category update",
        "externalCode": "updated",
        "asset": {}
    }
}
```

**Note**: a `product` reference is optional and could be added to the request, e.g.:

```json
{
  "data": {
    "name": "test category",
    "externalCode": "test_home",
    "asset": {},
    "products": [
      {
        "id": 1
      }
    ]
  }
}
```

## Catalogs

- GET `/secondary/v1/catalogs`
- GET `/api/v1/catalogs/{id}`

## Warranties

- GET `/api/v1/warranties`
- GET `/api/v1/warranties/{id}`
- POST `/api/v1/warranties`
- PUT `/api/v1/warranties/{id}`

`POST` request JSON sample:

```json
{
  "data": {
    "name": "test warranty",
    "ceilings": {},
    "images": {}
  }
}
```

`PUT` request JSON sample:

```json
{
  "data": {
    "name": "test updated warranty",
    "ceilings": {},
    "internalCode": "",
    "images": {}
  }
}
```

**Note**: a `branchId` reference is optional and could be added to the request, e.g.:

```json
{
  "data": {
    "name": "test warranty",
    "ceilings": {},
    "images": {},
    "internalCode": "",
    "branchId": "1"
  }
}
```

## Products

- GET `/api/v1/products` (list only active products)
- GET `/api/v1/products/all` (list all products)
- GET `/api/v1/products/{id}`
- GET `/api/v1/products/byIds/{ids}` (ids separated by `-`, e.g. `1000-12-33-2`)
- GET `/api/v1/products/byCode/{product_code}` (get product by product code)
- GET `/api/v1/products/recommended` (pagination non available here)
- PUT `/api/v1/products/{id}`

`PUT` request JSON sample:

```json
{
  "data": {
    "productDescription": "updated product description",
    "shortDescription": "updated short description",
    "description": "updated description",
    "price": 12.09,
    "displayPrice": "updated display price",
    "informativeSet": "updated informative set"
  }
}
```

**Note**: only the properties specified in the example above are allowed to be edited during a
PUT request for products!

## Packets

- GET `/api/v1/packets`
- GET `/api/v1/packets/{id}`
- GET `/api/v1/packets/byProductId/{product_id}` (get packets by product id)
- GET `/api/v1/packets/byIds/{ids}` (ids separated by `-`, e.g. `1000-12-33-2`)
- POST `/api/v1/packets`
- PUT `/api/v1/packets/{id}`

`POST` request JSON sample:

```json
{
  "data": {
    "name": "test packet",
    "sku": "test packet sku"
  }
}
```

`PUT` request JSON sample:

```json
{
  "data": {
    "name": "test packet updated",
    "sku": "test packet sku updated"
  }
}
```

**Note**: `warranties`, `productId` and `brokerId` references are 
optional and could be added to the request, e.g.:

```json
{
  "data": {
    "name": "test packet",
    "sku": "test packet sku",
    "brokerId": "1",
    "productId": "1",
    "warranties": [
      {
        "warranty": {
          "id": 16
        }
      },
      {
        "warranty": {
          "id": 17
        }
      }
    ]
  }
}
```

## Orders

- GET `/api/v1/orders`
- GET `/api/v1/orders/{id}`
- GET `/api/v1/orders/byCode/{order_code}` (get order by order code)
- GET `/api/v1/orders/byPolicyId/{policy_id}` (get order by policy id)
- GET `/api/v1/orders/byPolicyCode/{policy_code}` (get orders by policy code)
- ~~PUT `/api/v1/orders/confirm?order_code={code}` (updates the `anag_states` status of an order to "Confirmed")~~
- PUT `/api/v1/orders/cancel?order_id={id}` (updates the `anag_states` status of an order to "Failed")
- GET `/api/v1/orders/byCustomerId/{customer_id}` (get orders by customer id)

## Policies

- GET `/api/v1/policies`
- GET `/api/v1/policies/{id}`
- GET `/api/v1/policies/byPolicyCode/{policy_code}` (get policies by policy code or `master_policy_number`)
- GET `/api/v1/policies/byOrderId/{order_id}` (get policy by order id)
- GET `/api/v1/policies/byCustomerId/{customer_id}` (get policies by customer id)

## Customers

- GET `/api/v1/customers`
- GET `/api/v1/customers/{id}`
- PUT `/api/v1/customers/{id}` (updates a customer: only personal info like city, language etc... and email is updatable)

`PUT` request JSON sample:

```json
{
  "data": {
    "username": "updated username",
    "ndg": "updated ndg",
    "name": "updated name",
    "surname": "updated surname",
    "dateOfBirth": "2000-11-11",
    "birthCity": "updated birth_city",
    "birthCountry": "updated birth_country",
    "birthState": "updated birth_state",
    "taxCode": "updated taxcode",
    "gender": "updated gender",
    "street": "updated street",
    "streetNumber": "updated street_number",
    "city": "updated city",
    "country": "updated country",
    "countryId": 0,
    "zipCode": "updated zip_code",
    "state": "updated state",
    "stateId": 0,
    "primaryMail": "updated primary_mail",
    "secondaryMail": "updated secondary_mail",
    "primaryPhone": "updated primary_phone",
    "secondaryPhone": "updated secondary_phone",
    "language": "updated language",
    "legalForm": "updated legal_form",
    "education": "updated education",
    "salary": "updated salary",
    "profession": "updated profession"
  }
}
```

## Payment methods

- GET `/api/v1/payment_methods`
- GET `/api/v1/payment_methods/{id}`
- GET `/api/v1/payment_methods/byProductId/{product_id}` (get payment methods by product id)
- POST `/api/v1/payment_methods`
- PUT `/api/v1/payment_methods/{id}`

`POST` request JSON sample:

```json
{
  "data": {
    "paymentMethod": "test payment method",
    "paymentMethodType": "test payment method type",
    "type": "type",
    "externalId": 1,
    "active": true
  }
}
```

`PUT` request JSON sample:

```json
{
  "data": {
    "paymentMethod": "updated test payment method",
    "paymentMethodType": "updated test payment method type",
    "type": "updated type",
    "externalId": 2,
    "active": false
  }
}
```

## Brokers

- GET `/api/v1/brokers`
- GET `/api/v1/brokers/{id}`
- POST `/api/v1/brokers`
- PUT `/api/v1/brokers/{id}`

`POST` request JSON sample:

```json
{
  "data": {
    "name": "test broker name",
    "identitycode": "broker code"
  }
}
```

`PUT` request JSON sample:

```json
{
  "data": {
    "name": "test broker name update",
    "identitycode": "broker code update"
  }
}
```

## Companies

- GET `/api/v1/companies`
- GET `/api/v1/companies/{id}`
- POST `/api/v1/companies`
- PUT `/api/v1/companies/{id}`

`POST` request JSON sample:

```json
{
    "data": {
        "name": "test company name"
    }
}
```

`PUT` request JSON sample:

```json
{
    "data": {
        "name": "test company name update"
    }
}
```

## Product configurations

- GET `/api/v1/productConfigurations`
- GET `/api/v1/productConfigurations/{id}`
- POST `/api/v1/productConfigurations`
- PUT `/api/v1/productConfigurations/{id}`

`POST` request JSON sample:

```json
{
  "data": {
    "emission": "test internal",
    "emissionPrefix": "xyz",
    "certificate": "internal",
    "canOpenClaim": false,
    "claimType": "internal",
    "claimProvider": "MotionsCloud",
    "withdrawType": "StandardWithdraw",
    "deactivateType": "StandardDeactivate",
    "productId": 1
  }
}
```

`PUT` request JSON sample:

```json
{
  "data": {
    "emission": "test internal update",
    "emissionPrefix": "emission update",
    "certificate": "certificate update",
    "canOpenClaim": true,
    "claimType": "claimType update",
    "claimProvider": "claimProvider update",
    "withdrawType": "withdrawType update",
    "deactivateType": "deactivateType update"
  }
}
```

**Note**: `productId` in the `PUT` request is optional, while is required during `POST`!

## Configurations

- GET `/api/v1/configurations`
- GET `/api/v1/configurations/{id}`
- GET `/api/v1/configurations/confNode/{nodeName}`

Sample response GET `/api/v1/configurations/confNode/boInfo`:

```json
{
  "data": {
    "boInfo": {
      "title": "",
      "favicon": "",
      "background": "",
      "logo": ""
    }
  }
}
```

## Promotions

- GET `/api/v1/promotions`
- GET `/api/v1/promotions/{id}`
- GET `/api/v1/promotions/byCode/{promotion_code}` (get promotions by code)
- POST `/api/v1/promotions`
- PUT `/api/v1/promotions/{id}`

`POST` request JSON sample:

```json
{
  "data": {
    "ruleId": 21,
    "promotionCode": "test promotion",
    "description": "test desc",
    "startDate": "2023-12-01",
    "endDate": "2023-12-31",
    "active": true,
    "code": "internal"
  }
}
```

`PUT` request JSON sample:

```json
{
  "data": {
    "ruleId": 22,
    "promotionCode": "test updated promotion",
    "description": "test updated desc",
    "startDate": "2024-12-01",
    "endDate": "2024-12-31",
    "active": false
  }
}
```

**Note**: `promotionCode` and `code` fields are mandatory, `code` is also **NOT** updatable!

## Coupons

- GET `/api/v1/coupons`
- GET `/api/v1/coupons/{id}`
- GET `/api/v1/coupons/byCode/{coupon_code}` (get coupons by code)
- GET `/api/v1/coupons/multiple/csv` (generate a CSV file of coupons of type "multiple")
- POST `/api/v1/coupons/single`
- **DEPRECATED**: ~~POST `/api/v1/coupons/multiple/{quantity}`~~
- PUT `/api/v1/coupons/{id}`

`POST` request JSON "single" coupon sample:

```json
{
  "data": {
    "promotionId": 44,
    "couponCode": "TestCouponCode",
    "startDate": "2023-07-15",
    "endDate": "2023-08-31",
    "active": true,
    "maxUsage": 200
  }
}
```

**Notes**: 
- For "single" coupons the `couponCode` field is optional; if not specified a random value
will be generated.
- For `/api/v1/coupons/byCode/{coupon_code}` it is possible to specify the `insensitive=true|false` parameter so 
that the `coupon_code` can be evaluated by following a case sensitive or insensitive strategy, based on the specified value. 
By default the endpoints are case **SENSITIVE**!
E.g. `/api/v1/coupons/byCode/{coupon_code}?insensitive=true`

`PUT` request JSON sample:

```json
{
  "data": {
    "promotionId": 43,
    "startDate": "2023-12-02",
    "endDate": "2023-12-30",
    "active": false,
    "maxUsage": 100
  }
}
```

**Note**: `couponCode` is **NOT** updatable!

## PromotionsOrders

- GET `/api/v1/promotionsOrders`
- GET `/api/v1/promotionsOrders/{id}`
- GET `/api/v1/promotionsOrders/byOrderCode/{order_code}` (get promotion orders by order code)
- GET `/api/v1/promotionsOrders/byPromotionCode/{promotion_code}` (get promotion orders by promotion code)
- GET `/api/v1/promotionsOrders/byCouponCode/{coupon_code}` (get promotion orders by coupon code)
- GET `/api/v1/promotionsOrders/byCustomerCode/{customer_code}` (get promotion orders by customer code)
- GET `/api/v1/promotionsOrders/validate/{coupon_code}` (checks whether the number of `promotionsOrders` 
entities having that `coupon_code` is less than or equal to the `maxUsage` value of the coupon)
- POST `/api/v1/promotionsOrders`
- PUT `/api/v1/promotionsOrders/{id}`

`POST` request JSON sample:

```json
{
    "data": {
        "promotionCode": "HOLIDAY21",
        "orderCode": "Y-70f488b",
        "couponCode": "SUMMER50",
        "customerCode": "STLLGU95A01F839A"
    }
}
```

**Notes**:
- `promotionCode` has to exist in column `promotions.promotions_code`
- `orderCode` has to exist in column `orders.order_code`
- `couponCode` has to exist in column `coupons.coupons_code`
- `customerCode` has to exist in column `customers.tax_code`
- For `/api/v1/promotionsOrders/validate/{coupon_code}` and `/api/v1/promotionsOrders/byCouponCode/{coupon_code}` it
is possible to specify the `insensitive=true|false` parameter so that the `coupon_code` can be evaluated by following
a case sensitive or insensitive strategy, based on the specified value. By default the endpoints are case **SENSITIVE**!
E.g. `/api/v1/promotionsOrders/validate/{coupon_code}?insensitive=true`

`PUT` request JSON sample:

```json
{
    "data": {
        "promotionCode": "SUMMER21",
        "orderCode": "Y-70f488b",
        "couponCode": "SUMMER50",
        "customerCode": "STLLGU95A01F839A"
    }
}
```

## Promotion Rules

- GET `/api/v1/promotionRules`
- GET `/api/v1/promotionRules/{id}`
- POST `/api/v1/promotionRules`
- PUT `/api/v1/promotionRules/{id}`

`POST` request JSON sample:

```json
{
  "data": {
    "name": "test POST API",
    "rule": {
      "actions": {
        "discountType": "percentage",
        "discountAmount": "10%"
      },
      "conditions": {
        "items": {
          "packets": [],
          "products": [
            "product3",
            "product4"
          ]
        }
      }
    }
  }
}
```

**Note**: check the json-schema related to the rule JSON object 
[here](../src/main/resources/schemas/promotionRule_v1.json)

`PUT` request JSON sample:

```json
{
  "data": {
    "name": "test PUT API",
    "rule": {
      "actions": {
        "discountType": "percentage",
        "discountAmount": "20%"
      },
      "conditions": {
        "items": {
          "packets": [
            "packet1"
          ],
          "products": [
            "product1",
            "product2"
          ]
        }
      }
    }
  }
}
```

## Promotions Batches

- GET `/api/v1/promotionBatches`
- GET `/api/v1/promotionBatches/{id}`
- GET `/api/v1/promotionBatches/byPromotionId/{promotion_id}` (get promotion batches by promotion id)
- GET `/api/v1/promotionBatches/byPromotionCode/{promotion_code}` (get promotion batches by promotion code)
- POST `/api/v1/promotionBatches`

`POST` request JSON sample:

```json
{
  "data": {
    "baseCode": "TEST_",
    "promotionId": 44,
    "numberOfCodes": 2,
    "description": "test batch",
    "startDate": "2023-07-15",
    "endDate": "2023-08-31"
  }
}
```

**Notes**:
- `baseCode`, `startDate` and `endDate` fields are optional,
  if not specified the ones of the related promotion will be used;
- The `POST` request creates a batch entity and also as many coupons as the `numberOfCodes` entities
  under the hood.

## Survey Questions

- GET `/api/v1/surveyQuestions`
- GET `/api/v1/surveyQuestions/{id}`

## Related Warranties

- GET `/api/v1/relatedWarranties`
- GET `/api/v1/relatedWarranties/{id}`
- GET `/api/v1/relatedWarranties/byWarrantyAndPacket/{warranty_id}/{packet_id}` 
(get related warranties by warranty ida and packet id)
- GET `/api/v1/relatedWarranties/byPacketId/{packet_id}` (get related warranties by packet id)
- POST `/api/v1/relatedWarranties/validate` (validate related warranties rule constraints based on 
the request body)

**Note**: the `validate` endpoint does not require authentication!

### Example 1 `POST /api/v1/relatedWarranties/validate`

Request example 1:

```json
{
  "data": {
    "packetId": 1,
    "warranties": [
      {
        "id": 1,
        "externalCode": "Responsabilità civile per danni a terzi"
      },
      {
        "id": 2,
        "externalCode": "Rimborso spese veterinarie per intervento"
      },
      {
        "id": 3,
        "externalCode": "Rimborso spese veterinarie senza intervento"
      },
      {
        "id": 17,
        "externalCode": "RC cane"
      },
      {
        "id": 5,
        "externalCode": "Not related to packet 1"
      },
      {
        "id": 11111,
        "externalCode": "Not existing"
      }
    ]
  }
}
```

**Notes**: 
- the `packetId` should always be the packet linked to the relatedWarranties listed in `warranties`
- the `externalCode` field in the request is optional

Response example 1:

```json
{
  "data": {
    "valid": true,
    "rules": [
      {
        "id": 2,
        "externalCode": "Rimborso spese veterinarie per intervento",
        "packetId": 1,
        "dependencies": [
          17
        ],
        "possibleDependencies": [],
        "conflicts": [
          12
        ],
        "hasConstraints": true
      },
      {
        "id": 17,
        "externalCode": "RC cane",
        "packetId": 1,
        "dependencies": [],
        "possibleDependencies": [],
        "conflicts": [],
        "hasConstraints": false
      },
      {
        "id": 3,
        "externalCode": "Rimborso spese veterinarie senza intervento",
        "packetId": 1,
        "dependencies": [],
        "possibleDependencies": [],
        "conflicts": [],
        "hasConstraints": false
      },
      {
        "id": 1,
        "externalCode": "Responsabilità civile per danni a terzi",
        "packetId": 1,
        "dependencies": [
          2
        ],
        "possibleDependencies": [
          [
            2,
            3
          ],
          [
            4
          ]
        ],
        "conflicts": [
          13
        ],
        "hasConstraints": true
      }
    ]
  }
}
```

### Example 2 `POST /api/v1/relatedWarranties/validate`

Request example 2:

```json
{
  "data": {
    "packetId": 1,
    "warranties": [
      {
        "id": 3
      },
      {
        "id": 2
      }
    ]
  }
}
```

Response example 2:

```json
{
  "error": [
    {
      "errorCode": "ERR_ADD_WARRANTY",
      "warranties": [
        {
          "id": 1,
          "internalCode": "DA",
          "name": "Danni abitazione",
          "cause": 2
        }
      ]
    },
    {
      "errorCode": "ERR_ADD_WARRANTY",
      "warranties": [
        {
          "id": 1,
          "internalCode": "DA",
          "name": "Danni abitazione",
          "cause": 3
        }
      ]
    }
  ]
}
```

### Example 3 `POST /api/v1/relatedWarranties/validate`

Request example 3:

```json
{
  "data": {
    "packetId": 1,
    "warranties": [
      {
        "id": 1
      }
    ]
  }
}
```

Response example 3:

```json
{
  "error": [
    {
      "errorCode": "ERR_ADD_WARRANTY",
      "warranties": [
        {
          "id": 2,
          "internalCode": "c",
          "name": "Rimborso spese veterinarie per intervento",
          "cause": 1
        }
      ]
    },
    {
      "errorCode": "ERR_POSS_ADD_WARRANTY",
      "possibleWarranties": [
        [
          {
            "id": 4,
            "internalCode": "g",
            "name": "Tutela legale",
            "cause": 1
          }
        ],
        [
          {
            "id": 2,
            "internalCode": "c",
            "name": "Rimborso spese veterinarie per intervento",
            "cause": 1
          },
          {
            "id": 3,
            "internalCode": "d",
            "name": "Rimborso spese veterinarie senza intervento",
            "cause": 1
          }
        ]
      ]
    }
  ]
}
```

### Example 4 `POST /api/v1/relatedWarranties/validate`

Request example 4:

```json
{
  "data": {
    "packetId": 1,
    "warranties": [
      {
        "id": 1
      },
      {
        "id": 2
      },
      {
        "id": 3
      },
      {
        "id": 17
      },
      {
        "id": 4
      }
    ]
  }
}
```

Response example 4:

```json
{
  "error": [
    {
      "errorCode": "ERR_DEL_WARRANTY",
      "warranties": [
        {
          "id": 4,
          "internalCode": "g",
          "name": "Tutela legale",
          "cause": 2
        }
      ]
    },
    {
      "errorCode": "ERR_DEL_WARRANTY",
      "warranties": [
        {
          "id": 4,
          "internalCode": "g",
          "name": "Tutela legale",
          "cause": 1
        }
      ]
    }
  ]
}
```

## UtmSources

- GET `/api/v1/utmSources`
- GET `/api/v1/utmSources/{id}`
- GET `/api/v1/utmSources/byCode/{code}` (get utmSources by code)
- POST `/api/v1/utmSources`
- PUT `/api/v1/utmSources/{id}`

`POST` request JSON sample:

```json
{
  "data": {
    "code": "carrefour",
    "name": "Carrefour",
    "products": [
      {
        "id": 1
      },
      {
        "id": 2
      }
    ]
  }
}
```

**Note**: `locations` reference is optional and could be added to the request, e.g.:

```json
{
  "data": {
    "code": "carrefour",
    "name": "Carrefour",
    "products": [
      {
        "id": 1
      },
      {
        "id": 2
      }
    ],
    "locations": [
      {
        "id": 11
      },
      {
        "id": 12
      }
    ]
  }
}
```

`PUT` request JSON sample:

```json
{
  "data": {
    "name": "Carrefour updated",
    "products": [
      {
        "id": 3
      }
    ],
    "locations": [
      {
        "id": 13
      }
    ]
  }
}
```

**Note**: `code` is **NOT** updatable!

## UtmSource Locations

- GET `/api/v1/utmSourceLocations`
- GET `/api/v1/utmSourceLocations/{id}`
- GET `/api/v1/utmSourceLocations/byCode/{code}` (get utmSource locations by code)
- POST `/api/v1/utmSourceLocations`
- PUT `/api/v1/utmSourceLocations/{id}`

`POST` request JSON sample:

```json
{
  "data": {
    "code": "carrefour_3",
    "name": "Carrefour, Via Nazionale, 3"
  }
}
```

**Note**: it's also possible to specify the `utmSourceId` to directly link an utmSource to the 
new location:

```json
{
  "data": {
    "code": "carrefour_3",
    "name": "Carrefour, Via Nazionale, 3",
    "utmSourceId": 3
  }
}
```

`PUT` request JSON sample:

```json
{
  "data": {
    "code": "carrefour_4",
    "name": "Carrefour, Via Roma, 4",
    "utmSourceId": 4
  }
}
```

## GiftCards

- GET `/api/v1/giftcards`
- GET `/api/v1/giftcards/{id}`
- POST `/api/v1/giftcards`
- PUT `/api/v1/giftcards/{id}`

`POST` request JSON sample:

```json
{
    "data": {
        "uniqueCardName": "carrefour_5_euro",
        "cardName": "Carrefour",
        "cardValue": 5.00,
        "startDate": "2023-12-01T00:00:00",
        "endDate": "2023-12-31T23:59:59",
        "utmSourceId": 29
    }
}
```

`PUT` request JSON sample:

```json
{
    "data": {
        "uniqueCardName": "carrefour_5_euroo",
        "cardName": "Carrefourr",
        "cardValue": 5.00,
        "startDate": "2023-12-01T00:00:00",
        "endDate": "2024-12-31T23:59:59",
        "utmSourceId": 29
    }
}
```

## Claims

- GET `/api/v1/claims`
- GET `/api/v1/claims/{id}`

## Paging, Sorting and Filtering

For each "get all" endpoint pagination, sorting and filtering is supported.

Here are a couple of examples:
```sh
GET /api/v1/categories?page=1&perPage=2&sort=name&order=desc

GET /secondary/v1/catalogs?page=1&perPage=2&sort=company&order=asc

# filter={"name":"o"} + pagination
GET /api/v1/categories?filter=%7B%22name%22%3A%22o%22%7D&perPage=2&sort=externalCode&order=desc

# filter={"name":"Casa","externalCode":"home"}
GET /api/v1/categories?filter=%7B%22name%22%3A%22Casa%22%2C%22externalCode%22%3A%22home%22%7D

# filter={"product_code":"net","product_description":"Silver"}
GET /api/v1/products?filter=%7B%22product_code%22%3A%22net%22%2C%22product_description%22%3A%22Silver%22%7D

# filter={"product_code":"e","recurring":true}
GET /api/v1/products?filter=%7B%22product_code%22%3A%22e%22%2C%22recurring%22%3Atrue%7D

# filter={"policy_code":null}
GET /api/v1/orders?filter=%7B%22policy_code%22%3Anull%7D

# filter={"policyCode":"R3"}
GET /api/v1/orders?filter=%7B%22policyCode%22%3A%22R3%22%7D

# filter={"id":1}
GET /api/v1/categories?filter=%7B%22id%22%3A2%7D
# filter={"id":"1"}
GET /api/v1/categories?filter=%7B%22id%22%3A%221%22%7D

# {"created_at":"2023-05-24T00:00"}
GET /api/v1/orders?filter=%7B%22created_at%22%3A%222023-05-24T00%3A00%22%7D
# {"created_at":"2023-05-12 12:16:51.961059"}
GET /api/v1/orders?filter=%7B%22created_at%22%3A%222023-05-12%2012%3A16%3A51.961059%22%7D
# {"start_date": "2022-06-12 00:00:00.00000"}
GET /api/v1/products?filter=%7B%22start_date%22%3A%20%222022-06-12%2000%3A00%3A00.00000%22%7D
# {"start_date": "2022-06-12"}
GET /api/v1/products?filter=%7B%22start_date%22%3A%20%222022-06-12%22%7D
```
So the specific **optional** query params are:
- `page` - The current page number (integer). First page is `1`;
- `perPage` - The number of records per page;
- `sort` - DB column name to sort elements
- `order` - Sorting order. `asc` (ascending) or `desc` (descending)
- `filter` - Filter by one or multiple fields using the following structure:
`{"<field>":<value>, "<field>":<value>, ...}`, properly encoded!

**Note 1**:
The `filter` fields are considered as in `AND` at database level and their values are 
case ~~insensitive~~ ~~sensitive~~ **insensitive** in case of `String`, so, for instance, 
when asking for `{"name":"caSA","externalCode":"HOME"}` in `categories` 
it means that we want to retrieve all category entities having `home` that contains `casa` as value 
**and** `externalCode` that contains `home` as value!

`String`, `Boolean`, _Numbers_ and `NULL` values are currently tested and supported.
E.g. to retrieve all orders having `policy_code` set to `NULL` we can use 
`{"policy_code":null}` (remember to encode it first)!

**Note 2**: 
There is default limit in a "get all" request (when `perPage` is not specified) that is configurable
by the `rest.list.default.limit` application property.
